import Cookies from 'universal-cookie';

const baseUrl = `${process.env.REACT_APP_BASE_URL}:${process.env.REACT_APP_CMS_PORT}`;
const cookies = new Cookies();
class BuklUpload {
  static buklUploadApi = data => fetch(`${baseUrl}/BulkImportData`, {
    method: 'Post',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
    body: JSON.stringify(data),
  });
}
export default BuklUpload;
