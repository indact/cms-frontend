import Cookies from 'universal-cookie';

const baseUrl = `${process.env.REACT_APP_BASE_URL}:${process.env.REACT_APP_CMS_PORT}`;

// const baseUrl = 'http://localhost:3006';
const cookies = new Cookies();
const token = cookies.get('token');
const stateId = cookies.get('state').id;

class TaskManagementApi {
  static postAddTaskType = params => fetch(`${baseUrl}/tasktypes`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: token,
    },
    body: JSON.stringify(params),
  });

  static getAllTasksType = (state = stateId) => fetch(`${baseUrl}/tasktypes?page_size=1000&deleted=false&state=${state}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: token,
    },
  });

  static updateTaskTypes = (id, data) => fetch(`${baseUrl}/tasktypes/${id}`, {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: token,
    },
    body: JSON.stringify(data),
  });

  static deleteTaskTypes = data => fetch(`${baseUrl}/tasktypes`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: token,
    },
    body: JSON.stringify(data),
  });

  // status

  static postAddTaskStatus = params => fetch(`${baseUrl}/taskStatus`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
    body: JSON.stringify(params),
  });

  static getAllTaskStatus = (state = stateId) => fetch(`${baseUrl}/taskStatus?page_size=1000&deleted=false&state=${state}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
  });

  static updateTaskStatus = data => fetch(`${baseUrl}/taskstatus`, {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
    body: JSON.stringify(data),
  });

  static deleteTaskStatus = data => fetch(`${baseUrl}/taskStatus`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
    body: JSON.stringify(data),
  });

  static getAllCycle = () => fetch(`${baseUrl}/Cycles?page_size=1000&deleted=false`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: token,
    },
  });

  // message
  static postAddMessage = params => fetch(`${baseUrl}/templates`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: token,
    },
    body: JSON.stringify(params),
  });

  static getAllMessage = (state = stateId) => fetch(`${baseUrl}/templates?page_size=1000&deleted=false&state=${state}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: token,
    },
  });

  static updateMessage = (id, data) => fetch(`${baseUrl}/templates/${id}`, {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: token,
    },
    body: JSON.stringify(data),
  });

  static deleteMessage = data => fetch(`${baseUrl}/templates/`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: token,
    },
    body: JSON.stringify(data),
  });
}

export default TaskManagementApi;
