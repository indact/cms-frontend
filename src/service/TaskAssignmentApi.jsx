import Cookies from 'universal-cookie';

const baseUrlLocal = `${process.env.REACT_APP_BASE_URL}:${process.env.REACT_APP_JS_PORT}`;
const baseUrl = `${process.env.REACT_APP_BASE_URL}:${process.env.REACT_APP_CMS_PORT}`;

const cookies = new Cookies();
const stateId = cookies.get('state').id;
class TaskAssignmentApi {
  static getAllTasksAssigned = (params, state = stateId) => fetch(`${baseUrl}/tasks?stateID=${state}&${params}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
  });

  static PatchBulkAssign = data => fetch(`${baseUrl}/tasks/assign`, {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
    body: JSON.stringify(data),
  });

  static PostAddTasksAssigned = data => fetch(`${baseUrl}/tasks`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
    body: JSON.stringify(data),
  });

  static PatchUpdateTasksAssigned = data => fetch(`${baseUrl}/tasks/`, {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
    body: JSON.stringify(data),
  });

  static DeleteTasksAssigned = id => fetch(`${baseUrlLocal}/alltasks/${id}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
  });

  static GetSingleForm = formName => fetch(`${baseUrl}/forms/${formName}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
  });

  static GetSingleFormSubmission = (formName, id) => fetch(`${baseUrl}/forms/${formName}/submissions/${id}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
  });

  static GetFormByTaskType = id => fetch(`${baseUrl}/tasktypes/${id}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
  });

  static getAllTemplates = () => fetch(`${baseUrl}/templates`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
  });

  static postSendMessage = data => fetch(`${baseUrl}/templates/send`, {
    method: 'Post',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
    body: JSON.stringify(data),
  });
}
export default TaskAssignmentApi;
