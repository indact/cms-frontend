import Cookies from 'universal-cookie';

const baseUrl = `${process.env.REACT_APP_BASE_URL}:${process.env.REACT_APP_CMS_PORT}`;

const cookies = new Cookies();

class UserManagementApi {
  static getUserData = () => fetch(`${baseUrl}/user?page_size=0`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
  });

  static getStateData = () => fetch(`${baseUrl}/states`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
  });

  static getAllPermissionData = () => fetch(`${baseUrl}/all_permissions`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
  });

  static getFeatureData = () => fetch(`${baseUrl}/features`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
  });

  static getSearchStateData = query => fetch(`${baseUrl}/user?states=${cookies.get('state').id}&${query}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
  });

  static postNewPermission = data => fetch(`${baseUrl}/user/AssignPermission`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
    body: JSON.stringify(data),
  });

  static patchUpdatePermission = data => fetch(`${baseUrl}/user/RemovePermission`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
    body: JSON.stringify(data),
  });
}
export default UserManagementApi;
