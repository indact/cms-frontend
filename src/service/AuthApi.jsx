const baseUrl = `${process.env.REACT_APP_BASE_URL}:${process.env.REACT_APP_CMS_PORT}`;
class AllAip {
  static login = data => fetch(`${baseUrl}/user/login`, {
    method: 'POST',

    headers: new Headers({
      Accept: 'application/json',
      'Content-Type': 'application/json',
    }),
    body: JSON.stringify(data),
  });

  static signup = data => fetch(`${baseUrl}/user`, {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json',
    }),
    body: JSON.stringify(data),
  });

  static Forgotpassword = data => fetch(`${baseUrl}/user/forgotpassword/sendotp`, {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
    }),
    body: JSON.stringify(data),
  });

  static ResendOtp = data => fetch(`${baseUrl}/user/forgotpassword/resendotp`, {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
    }),
    body: JSON.stringify(data),
  });

  static VerifyOtp = data => fetch(`${baseUrl}/user/forgotpassword/verifyotp`, {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
    }),
    body: JSON.stringify(data),
  });

  static ResetPassword = data => fetch(`${baseUrl}/user/resetPassword`, {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: data.userKey.token,
    }),
    body: JSON.stringify({ newPassword: data.password }),
  });
}

// static userGetData(key, ip) {
//   return fetch("http://" + ip + "/rest-auth/user/" + key.key + "/");
// }
// static CallerList(ip) {
//   return fetch(
//     " http://" +
//       ip +
//       "/api/v1/users/usertypes/?user_types=SS&user_types=GS&user_types=HL&user_types=VL"
//   );
// }
// static CallerCallStat(userid, ip) {
//   return fetch("http://" + ip + "/api/v1/users/counts/" + userid + "/");
// }
// static CallerRank(userid, ip) {
//   return fetch(" http://" + ip + "/api/v1/users/performance/" + userid + "/");
// }

// static callerCallAssginApi(userid, ip) {
//   return fetch(" http://" + ip + "/api/v1/tasks/assignee/" + userid + "/");
// }
// static DashboardCallData(ip) {
//   return fetch(" http://" + ip + "/api/v1/users/totalcounts/");
// }

// static DashboardCallDataInTime(ip, userdata) {
//   return fetch(
//     "http://" +
//       ip +
//       "/api/v1/users/datewisecounts/?start_date=" +
//       userdata.start_date +
//       "&end_date=" +
//       userdata.end_date
//   );
// }
// static AllTaskTypeApi(ip) {
//   return fetch("http://" + ip + "/api/v1/task_types/");
// }
// static AllTaskStatusApi(ip) {
//   return fetch("http://" + ip + "/api/v1/task_status/");
// }
// static BulkCallAssiginApi(ip, body) {
//   return fetch("http://" + ip + "/api/v1/users/assign_numbers/", {
//     method: "POST",
//     headers: {
//       "Content-Type": "application/json",
//       Accept: "application/json"
//     },
//     body: JSON.stringify(body)
//   });
// }

// static TasksListDataApi(ip) {
//   return fetch("http://" + ip + "/api/v1/paginated_tasks/");
// }

// static TasksListDataNextPageApi(url) {
//   return fetch(url);
// }

// static TaskAssignApi(ip, data) {
//   return fetch("http://" + ip + "/api/v1/tasks/assign/", {
//     method: "POST",
//     headers: {
//       "Content-Type": "application/json",
//       Accept: "application/json"
//     },
//     body: JSON.stringify(data)
//   });
// }

// static CallerUserTypeChangeApi(data, ip) {
//   return fetch("http://" + ip + "/api/v1/users/" + data.id + "/", {
//     method: "PUT",
//     headers: {
//       "Content-Type": "application/json",
//       Accept: "application/json"
//     },
//     body: JSON.stringify(data)
//   });
// }
//   }

export default AllAip;
