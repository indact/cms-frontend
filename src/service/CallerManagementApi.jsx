import Cookies from 'universal-cookie';

const baseUrl = `${process.env.REACT_APP_BASE_URL}:${process.env.REACT_APP_CMS_PORT}`;

const cookies = new Cookies();
const stateId = cookies.get('state').id;
class CallerManagementApi {
  static getCallersData = (token, params, state = stateId) => fetch(`${baseUrl}/user?states=${state}&${params}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
  });

  static getAllCallersData = (token, params) => fetch(`${baseUrl}/user?${params}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
  });

  static postChangeUserState = (token, data) => fetch(`${baseUrl}/user/EnableDisableUser`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
    body: JSON.stringify(data),
  });

  static postGetUserProfile = (token, data) => fetch(`${baseUrl}/user/${data}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: cookies.get('token'),
    },
  });
}

export default CallerManagementApi;
