import Cookies from 'universal-cookie';

const baseUrl = `${process.env.REACT_APP_BASE_URL}:${process.env.REACT_APP_CMS_PORT}`;

const cookies = new Cookies();
const token = cookies.get('token');

export const userData = (id, token_temp) => fetch(`${baseUrl}/user/${id}`, {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json; charset=utf-8',
    Accept: 'application/json, text/plain, */*',
    authorization_token: token_temp,
  },
});
export const updateInfo = (token_temp, data) => fetch(`${baseUrl}/user/changepassword`, {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json; charset=utf-8',
    Accept: 'application/json, text/plain, */*',
    authorization_token: token_temp,
  },
  body: JSON.stringify(data),
});
export const getStateList = defaultToken => fetch(`${baseUrl}/user/states`, {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json; charset=urf-8',
    Accept: 'application/json, text/plain, */*',
    authorization_token: cookies.get('token') || defaultToken,
  },
});
export const getStatePermission = (state, defaultToken) => fetch(`${baseUrl}/user/states/${state}`, {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json; charset=urf-8',
    Accept: 'application/json, text/plain, */*',
    authorization_token: cookies.get('token') || defaultToken,
  },
});
