import Cookies from 'universal-cookie';

const baseUrl = `${process.env.REACT_APP_BASE_URL}:${process.env.REACT_APP_CMS_PORT}`;
const cookies = new Cookies();
const token = cookies.get('token');
const stateName = cookies.get('state') && cookies.get('state').name;

class FormManagement {
  static getFormListApi = (state = stateName) => fetch(`${baseUrl}/forms?state=${state}&limit=1000`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
      authorization_token: token,
    },
  });

  static getPersistentForm = (state = stateName) => fetch(
      `${baseUrl}/forms/${state.toLowerCase().replace(/\s/g, '')}-persistent`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          Accept: 'application/json, text/plain, */*',
          authorization_token: token,
        },
      },
  );

  static getForm = formName => fetch(`${baseUrl}/forms/${formName}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json, text/plain, */*',
    },
  });

  static saveForm = data => fetch(`${baseUrl}/forms`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      authorization_token: token,
    },
    body: JSON.stringify(data),
  });

  // static updateForm = form => fetch(`${baseUrl}/form/${form._id}`, {
  //   method: 'PATCH',
  //   headers: {
  //     'Content-Type': 'application/json',
  //     authorization_token: token,
  //   },
  //   body: JSON.stringify(form),
  // });
  static submitData = (form, { data }) => fetch(`${baseUrl}/forms/${form._id}/submissions`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      authorization_token: token,
    },
    body: JSON.stringify(data),
  });

  static anonymousSubmission = (form, { data }) => fetch(`${baseUrl}/forms/${form._id}/anonymous/submissions`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      authorization_token: token,
    },
    body: JSON.stringify(data),
  });
  // static updateForm = form =>  await fetch(`${baseUrl}/form/${form._id}/submission`, {
  //   method: 'POST',
  //   headers: {
  //     'Content-Type': 'application/json',
  //     authorization_token: token,
  //   },
  //   body: JSON.stringify({ data }),
  // });
  // static updateForm = form =>  await fetch(`${baseUrl}/form/${form._id}/submission`, {
  //   method: 'POST',
  //   headers: {
  //     'Content-Type': 'application/json',
  //     authorization_token: token,
  //   },
  //   body: JSON.stringify({ data }),
  // })
}

export default FormManagement;
