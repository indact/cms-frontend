import React from 'react';
import { storiesOf } from '@storybook/react';

import ButtonGroups from './ButtonGroups';

storiesOf('Buttons', module).add('ButtonGroups', () => <ButtonGroups />);
