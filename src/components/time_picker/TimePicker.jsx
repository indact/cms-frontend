import React, { useState } from 'react';
import { DateTimePicker, KeyboardDateTimePicker } from '@material-ui/pickers';

function DatePicker(props) {
  const [selectedDate, handleDateChange] = useState(
    new Date('2018-01-01T00:00:00.000Z'),
  );

  return (
    <>
      <KeyboardDateTimePicker
        variant="inline"
        ampm={false}
        label="With keyboard"
        // value={selectedDate}
        // onChange={() => console.log()}
        onError={console.log}
        // disableFuture
        format="yyyy/MM/dd HH:mm"
      />
    </>
  );
}

export default DatePicker;
