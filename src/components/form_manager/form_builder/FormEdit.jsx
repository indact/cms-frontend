import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _cloneDeep from 'lodash/cloneDeep';
import Cookies from 'universal-cookie';
import _camelCase from 'lodash/camelCase';
import { connect } from 'react-redux';
// import Button from 'formiojs/components/button/Button';
import {
  Row, Col, Input, FormGroup, Label, Button,
} from 'reactstrap';
import FormBuilder from './FormBuilder';
import {
  getFormList,
  setForm,
  saveForm,
  updateForm,
  resetForm,
} from '../../../store/action/FormioAction';
import Notification from '../../../features/notifications';

const cookies = new Cookies();
const state = cookies.get('state') !== undefined ? cookies.get('state') : null;

class FormEdit extends Component {
  constructor(props) {
    super(props);

    const { addlFormData, form, reset } = props;
    if (!reset) {
      this.state = {
        error: false,
        stateName: state !== null ? state.name : '',
        form: form
          ? { ...form }
          : {
            title: '',
            name: '',
            type: '',
            path: '',
            components: [],
          },
      };
    } else {
      this.state = {
        error: false,
        stateName: state !== null ? state.name : '',
        form: {
          title: '',
          name: '',
          type: '',
          path: '',
          components: [],
        },
      };
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (
      nextProps.form
      && (prevState.form._id !== nextProps.form._id
        || prevState.form.modified !== nextProps.form.modified)
    ) {
      return {
        form: _cloneDeep(nextProps.form),
      };
    }

    return null;
  }

  setForm(form) {
    this.setState({
      form,
    });
  }

  saveForm = async () => {
    const thisProps = this.props;
    const thisState = this.state;
    if (thisProps.saveText === 'Save') {
      await thisProps.updateForm(thisState.form);
      await thisProps.getFormList();
      document.location.reload();
    } else if (thisProps.saveForm && typeof thisProps.saveForm === 'function') {
      if (thisState.form.title !== '' && thisState.form.components.length > 1) {
        thisProps.saveForm(thisState.form);
      } else {
        this.setState({ error: true });
      }
    }
  };

  resetForm = async () => {
    const thisProps = this.props;
    thisProps.resetForm();
    await this.setState({
      form: {
        title: '',
        name: '',
        path: '',
        type: '',
        display: 'form',
        components: [],
      },
    });
  };

  componentWillUnmount = async () => {
    const thisProps = this.props;
    thisProps.resetForm();
    await resetForm();
  };

  handleChange = (prop, event) => {
    const { value } = event.target;
    // console.log({prop, event});

    this.setState({ error: false });
    this.setState((prev) => {
      const { form, stateName } = prev;
      if (prop === 'title') {
        form[prop] = value;
        form.name = prev.form.title
          ? `${stateName.toLowerCase()}-${_camelCase(value)}`
          : '';
        form.path = _camelCase(value).toLowerCase();
        form.path = prev.form.title
          ? `${stateName.toLowerCase()}/${_camelCase(value)}`
          : '';
      } else {
        form[prop] = value;
      }
      return prev;
    });
  };

  render() {
    const thisProps = this.props;
    const thisState = this.state;
    const { form } = this.state;
    const { saveText } = this.props;
    return (
      <div className="animated fadeIn">
        <Notification notifor="createForm" notidata={thisProps.createForm} />
        {/* <Notification
        notifor="allform"
        notidata={thisProps.formList}
        /> */}
        <Row>
          <Col md="2" sm="6">
            <FormGroup>
              <Label htmlFor="title" className="field-required">
                Title
              </Label>
              <Input
                invalid={thisState.error}
                type="text"
                className="form-control"
                id="title"
                placeholder="Enter the form title"
                value={thisState.form.title}
                onChange={event => this.handleChange('title', event)}
              />
            </FormGroup>
          </Col>
          <Col md="2" sm="6">
            <FormGroup>
              <Label htmlFor="State" className="field-required">
                Tags
              </Label>
              <Input
                type="select"
                name="type"
                // disabled
                value={thisState.form.type}
                onChange={event => this.handleChange('type', event)}
              >
                <option>Select Form Tag</option>
                <option>
                  RTE-{state !== null ? state.name : 'Not selected'}
                </option>
                <option>M&E</option>
              </Input>
            </FormGroup>
          </Col>
          <Col md="2" sm="6">
            <FormGroup>
              <Label htmlFor="title" className="field-required">
                Name
              </Label>
              <Input
                disabled
                type="text"
                name="Name"
                value={thisState.form.name}
              />
            </FormGroup>
          </Col>
          <Col md="2" sm="6">
            <FormGroup>
              <Label htmlFor="State" className="field-required">
                Path
              </Label>
              <Input
                disabled
                type="text"
                name="Path"
                value={thisState.form.path}
              />
            </FormGroup>
          </Col>
          <Col md="2" sm="6">
            <FormGroup>
              <Label htmlFor="State" className="field-required">
                State
              </Label>
              <Input
                disabled
                type="text"
                name="state"
                value={state !== null ? state.name : 'Not selected'}
              />
            </FormGroup>
          </Col>
          <Col md="2" sm="2">
            <div id="save-buttons" className="save-buttons pull-right mt-4 ">
              <div className="form-group ">
                <Button color="success" onClick={() => this.saveForm()}>
                  {saveText}
                </Button>
              </div>
            </div>
            {thisProps.reset ? (
              <div
                id="save-buttons"
                className="save-buttons pull-right mt-4 mr-2"
              >
                <div className="form-group pull-right">
                  <Button
                    className="btn btn-warning"
                    onClick={() => this.resetForm()}
                  >
                    Reset Form
                  </Button>
                </div>
              </div>
            ) : null}
          </Col>
        </Row>
        <FormBuilder
          key={form._id}
          form={form}
          options={thisProps.options}
          onChange={data => this.setForm(data)}
          builder={thisProps.builder}
        />
        <div id="save-buttons" className="save-buttons pull-right mt-2">
          <div className="form-group pull-right">
            <Button color="success" onClick={() => this.saveForm()}>
              {saveText}
            </Button>
          </div>
        </div>
        {thisProps.reset ? (
          <div id="save-buttons" className="save-buttons pull-right mt-2 mr-2">
            <div className="form-group pull-right">
              <Button
                className="btn btn-warning"
                onClick={() => this.resetForm()}
              >
                Reset Form
              </Button>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}
const mapStateToProps = states => ({
  form: states.formio.form,
  createForm: states.formio.createForm,
  addlFormData: states.formio.addlFormData,
});
export default connect(
  mapStateToProps,
  {
    getFormList,
    setForm,
    saveForm,
    updateForm,
    resetForm,
  },
)(FormEdit);
