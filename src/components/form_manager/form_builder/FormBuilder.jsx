/* eslint-disable no-prototype-builtins */
import React, { Component } from 'react';
import AllComponents from 'formiojs/components';
import Components from 'formiojs/components/Components';
import FormBuilder from 'formiojs/FormBuilder';
import 'formiojs/dist/formio.full.css';
import './FormBuilder.css';

Components.setComponents(AllComponents);
export default class extends Component {
  static defaultProps = {
    options: {},
  };

  componentDidMount = () => {
    this.initializeBuilder(this.props);
  };

  componentWillUnmount = () => {
    if (this.builder !== undefined) {
      this.builder.instance.destroy(true);
    }
  };

  initializeBuilder = (props) => {
    const { options, form, builder } = props;

    if (this.builder !== undefined) {
      this.builder.instance.destroy(true);
    }

    this.builder = new (builder || FormBuilder)(this.element, form, options);
    this.builderReady = this.builder.setDisplay(form.display);

    this.builderReady.then(() => {
      this.builder.instance.on('saveComponent', this.emit('onSaveComponent'));
      this.builder.instance.on(
        'updateComponent',
        this.emit('onUpdateComponent'),
      );
      this.builder.instance.on(
        'deleteComponent',
        this.emit('onDeleteComponent'),
      );
      this.builder.instance.on(
        'cancelComponent',
        this.emit('onCancelComponent'),
      );
      this.builder.instance.on('editComponent', this.emit('onEditComponent'));
      this.builder.instance.on('saveComponent', this.onChange);
      this.builder.instance.on('updateComponent', this.onChange);
      this.builder.instance.on('deleteComponent', this.onChange);
    });
  };

  componentWillReceiveProps = (nextProps) => {
    const { options, form } = this.props;

    if (form !== nextProps.form) {
      this.initializeBuilder(nextProps);
    }

    if (options !== nextProps.options) {
      this.initializeBuilder(nextProps);
    }
  };

  render = () => <div ref={element => (this.element = element)} />;

  onChange = () => {
    const thisProps = this.props;
    if (
      thisProps.hasOwnProperty('onChange')
      && typeof thisProps.onChange === 'function'
    ) {
      thisProps.onChange(this.builder.instance.schema);
    }
  };

  emit = funcName => (...args) => {
    const thisProps = this.props;
    if (
      thisProps.hasOwnProperty(funcName)
      && typeof thisProps[funcName] === 'function'
    ) {
      thisProps[funcName](...args);
    }
  };
}
