import React, { Component } from 'react';
import { Modal, ModalBody, ModalHeader } from 'reactstrap';
import _ from 'lodash';
import { connect } from 'react-redux';
import MaterialTable from 'material-table';
import FormView from './FormView';

import {
  getFormList,
  setForm,
  setAddlFormData,
  resetMessage,
  resetForm,
  getFilteredFormList,
} from '../../../store/action/FormioAction';
import './index.css';
import Notification from '../../../features/notifications';
import DownlaodData from './DownloadData';

const columns = [
  {
    title: 'Title',
    field: 'title',
  },
  {
    title: 'Owner',
    field: 'owner',
  },
  {
    title: 'Path',
    field: 'path',
  },
  {
    title: 'Tags',
    field: 'tags',
  },

  {
    title: 'Modified',
    field: 'modified',
  },
  {
    title: 'Form',
    field: 'form',
    hidden: true,
  },
  {
    title: 'Formid',
    field: 'formId',
    hidden: true,
  },
];
class FormBrowser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openDownload: false,
      modal: false,
      alldata: [],
      formdata: '',
      filter: {
        title: '',
        owner: '',
        page: '1',
      },
    };
  }

  componentDidMount = async () => {
    const { state, ...actions } = this.props;
    await actions.getFormList(state.name);
  };

  componentDidUpdate = async (prevProps) => {
    const { state, ...actions } = this.props;
    if (prevProps.state !== state) {
      await actions.getFormList(state.name);
    }
  };

  componentWillMount = () => {
    const thisProps = this.props;
    const allRows = [];
    if (
      typeof thisProps.formList.data !== 'undefined'
      && thisProps.formList.data.length > 0
    ) {
      thisProps.formList.data.forEach((val) => {
        const rowdata = {};
        rowdata.title = val.title;
        rowdata.formId = val._id;
        rowdata.owner = val.owner;
        rowdata.path = val.path;
        rowdata.tags = val.tags;
        rowdata.form = val;
        rowdata.modified = val.modified.toString();
        allRows.push(rowdata);
      });
      this.setState({ alldata: allRows });
    } else {
      this.setState({ alldata: [] });
    }
  };

  toggle = (e, form) => {
    const thisProps = this.props;
    const thisState = this.state;
    if (!thisState.modal) {
      thisProps.setForm(form.form);
      thisProps.setAddlFormData(thisProps.addlFormDataList[form.tableData.id]);
    } else {
      thisProps.resetMessage();
    }
    this.setState(prevState => ({
      modal: !prevState.modal,
    }));
  };

  showdownload = (data) => {
    this.setState(prevState => ({
      openDownload: !prevState.openDownload,
      formdata: data,
    }));
  };

  handleChange = async (e) => {
    e.persist();
    const thisProps = this.props;
    const { filter } = this.state;
    await this.setState({
      filter: { ...filter, [e.target.name]: e.target.value },
    });
    thisProps.getFilteredFormList(filter);
  };

  componentWillReceiveProps = (nextProps) => {
    const thisProps = this.props;
    if (!_.isEqual(nextProps.formList, thisProps.formList)) {
      const allRows = [];
      if (
        typeof nextProps.formList.data !== 'undefined'
        && nextProps.formList.data.length > 0
      ) {
        nextProps.formList.data.forEach((val) => {
          const rowdata = {};
          rowdata.title = val.title;
          rowdata.formId = val._id;
          rowdata.owner = val.owner;
          rowdata.path = val.path;
          // eslint-disable-next-line prefer-destructuring
          rowdata.tags = val.tags[0];
          rowdata.form = val;
          rowdata.modified = val.modified.toString();
          allRows.push(rowdata);
        });
        this.setState({ alldata: allRows });
      } else {
        this.setState({ alldata: [] });
      }
    }
  };

  render() {
    const thisProps = this.props;
    const thisState = this.state;

    return (
      <div className="animated fadeIn">
        <DownlaodData
          isOpen={thisState.openDownload}
          toggleFilter={this.showdownload}
          formdata={thisState.formdata}
        />
        <Notification notifor="formList" notidata={thisProps.formList} />
        {/* <Notification
        notifor="allform"
        notidata={thisProps.formList}
        /> */}
        <MaterialTable
          title="List Of Forms"
          columns={columns}
          options={{
            pageSize: 10,
            actionsColumnIndex: -1,
            exportButton: true,
          }}
          actions={[
            {
              icon: 'get_app',
              tooltip: 'download',
              onClick: (event, data) => {
                this.showdownload(data);
                // this.downloadFormData(data.formId);
                // thisProps.openForm(
                //   data.taskId,
                //   data.taskFormId,
                //   data.taskpFormId,
                //   data.taskSubId,
                //   data.taskPSubId,
                //   data.taskStatus,
                // );
              },
            },
          ]}
          data={thisState.alldata}
          // style={{maxheight: '600px',width: '100%'}}
          onRowClick={(e, rowdata) => {
            this.toggle(e, rowdata);
          }}
        />
        <Modal
          isOpen={thisState.modal}
          toggle={this.toggle}
          style={{ maxWidth: '90%' }}
        >
          <ModalHeader toggle={this.toggle}>
            {thisProps.form ? thisProps.form.title : null}
          </ModalHeader>
          <ModalBody>
            <FormView
              close={() => {
                this.toggle();
              }}
            />
          </ModalBody>
        </Modal>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  formList: state.formio.formList.data,
  form: state.formio.form,
  addlFormDataList:
    state.formio.addlFormDataList !== undefined
      ? state.formio.addlFormDataList
      : [],
  addlFormData: state.formio.addlFormData,
  state: state.user.state,
});
export default connect(
  mapStateToProps,
  {
    getFormList,
    setForm,
    resetMessage,
    resetForm,
    setAddlFormData,
    getFilteredFormList,
  },
)(FormBrowser);
