import React, { Component } from 'react';
import { connect } from 'react-redux';
import Cookies from 'universal-cookie';
import {
  Form,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  FormGroup,
  Col,
} from 'reactstrap';
import DateFnsUtils from '@date-io/date-fns';
import TextField from '@material-ui/core/TextField';
import {
  KeyboardDateTimePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import { downloadFormData } from '../../../store/action/FormioAction';
import Notification from '../../../features/notifications';

const cookies = new Cookies();
const userEmailID = cookies.get('userEmail');
class DownloadData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedDate: new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000),
      selectedEndDate: new Date(),
      emailID: userEmailID,
      emailCheck: true,
    };
  }

  DownData = () => {
    const thisProps = this.props;
    // eslint-disable-next-line no-useless-escape
    const emailRex = /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    const { emailID, selectedDate, selectedEndDate } = this.state;
    if (!emailRex.test(emailID)) {
      this.setState({ emailCheck: false });
    } else {
      thisProps.downloadFormData({
        email: emailID,
        start: new Date(selectedDate).getTime(),
        end: new Date(selectedEndDate).getTime(),
        filter: {
          form: thisProps.formdata.formId,
        },
      });
    }
  };

  toggleLarge = () => {
    const thisProps = this.props;
    thisProps.toggleFilter();
  };

  handleChanges = (e) => {
    if (e.target.name === 'email-id') {
      // eslint-disable-next-line no-useless-escape
      const emailRex = /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
      if (emailRex.test(e.target.value)) {
        this.setState({ emailCheck: true });
      } else {
        this.setState({ emailCheck: false });
      }
      this.setState({ emailID: e.target.value });
    }
  };

  handleDateChange = (selectedDate) => {
    this.setState({ selectedDate });
  };

  handleEndDateChange = (selectedEndDate) => {
    this.setState({ selectedEndDate });
  };

  render() {
    const {
      props: { isOpen, datadownStatus },
      state: {
        selectedDate, selectedEndDate, emailID, emailCheck,
      },
    } = this;
    let emailfield = (
      <TextField
        error
        id="email-id"
        name="email-id"
        style={{ width: '100%' }}
        label="Email ID"
        type="email"
        required
        helperText="Please Enter Correct Email"
        value={emailID}
        onChange={this.handleChanges}
        margin="normal"
      />
    );
    if (emailCheck) {
      emailfield = (
        <TextField
          id="email-id"
          style={{ width: '100%' }}
          name="email-id"
          label="Email ID"
          type="email"
          required
          value={emailID}
          onChange={this.handleChanges}
          margin="normal"
        />
      );
    }

    return (
      <>
        <Notification notifor="dataDownload" notidata={datadownStatus} />
        <Modal isOpen={isOpen} toggle={this.toggleLarge} className="modal-info">
          <ModalHeader toggle={this.toggleLarge}>
            Download Form Data
          </ModalHeader>
          <ModalBody>
            <Form
              style={{
                display: 'flex',
                flexFlow: 'column',
              }}
            >
              <FormGroup row>
                <Col sm="6">
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDateTimePicker
                      style={{ width: '100%' }}
                      variant="inline"
                      label="Select Start Date"
                      value={selectedDate}
                      onChange={this.handleDateChange}
                      onError={console.log}
                      format="yyyy/MM/dd HH:mm"
                    />
                  </MuiPickersUtilsProvider>
                </Col>
                <Col sm="6">
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDateTimePicker
                      style={{ width: '100%' }}
                      variant="inline"
                      label="Select End Date"
                      value={selectedEndDate}
                      onChange={this.handleEndDateChange}
                      onError={console.log}
                      format="yyyy/MM/dd HH:mm"
                    />
                  </MuiPickersUtilsProvider>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Col sm="6">{emailfield}</Col>
              </FormGroup>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.DownData}>
              Download
            </Button>
          </ModalFooter>
        </Modal>
      </>
    );
  }
}
function mapStateToProps(state) {
  return {
    datadownStatus: state.formio.datadown,
  };
}

export default connect(
  mapStateToProps,
  {
    downloadFormData,
  },
)(DownloadData);
