/* eslint-disable react/no-array-index-key */
import React, { Component } from 'react';
import { Form } from 'react-formio';
import { connect } from 'react-redux';
import {
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Alert,
  Table,
  Row,
  Col,
  InputGroup,
} from 'reactstrap';
import classNames from 'classnames';
// import PropTypes from 'prop-types';
import { FormEdit, Errors } from '../form_builder';
import {
  resetMessage,
  setForm,
  submitData,
  getSubmissionData,
  updateSubmission,
  setSubmission,
  resetSubmission,
  getFormList,
} from '../../../store/action/FormioAction';

// const propTypes = {
//   children: PropTypes.node,
// };
// const defaultProps = {};
class FormView extends Component {
  constructor(props) {
    super(props);
    // this.toggle = this.toggle.bind(this);
    const list = [];
    const { form } = this.props;
    this.listComponents(form.components, list);
    this.state = {
      componentList: list,
      activeTab: '1',
      submission: {},
      page: '1',
      noSubmission: false,
    };
  }

  componentDidMount = async () => {
    const thisProps = this.props;
    await thisProps.getSubmissionData(thisProps.form);
    this.setState({
      noSubmission: thisProps.submissionList.length === 0,
    });
  };

  toggle = (tab) => {
    const thisState = this.state;
    const thisProps = this.props;
    if (thisState.activeTab !== tab) {
      if (tab === '2') thisProps.resetMessage();
      if (thisState.activeTab === '3') {
        thisProps.resetSubmission();
        this.setState({ submission: {} });
      }
      this.setState({
        activeTab: tab,
      });
    }
  };

  listComponents = (components, list) => {
    components.map((component) => {
      if (component.key === 'columns') {
        component.columns.map(column => this.listComponents(column.components, list));
      } else list.push(component);
      return 0;
    });
  };

  handleChange = async (e) => {
    e.persist();
    const thisState = this.state;
    const thisProps = this.props;
    await this.setState({
      page: e.target.value,
    });
    // console.log(this.state.page);
    thisProps.getSubmissionData(thisProps.form, thisState.page);
  };

  render() {
    const thisState = this.state;
    const thisProps = this.props;
    const { submissionList } = this.props;
    // console.log(thisProps);
    return (
      <React.Fragment>
        <Nav tabs style={{ fontSize: '1em' }}>
          <NavItem>
            <NavLink
              className={classNames({ active: thisState.activeTab === '1' })}
              onClick={() => {
                this.toggle('1');
              }}
            >
              Enter Data
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classNames({ active: thisState.activeTab === '2' })}
              onClick={() => {
                this.toggle('2');
              }}
            >
              Edit Form
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classNames({ active: thisState.activeTab === '3' })}
              onClick={() => {
                thisProps.resetSubmission();
                this.setState({ submission: null });
                this.toggle('3');
              }}
            >
              View Data
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={thisState.activeTab}>
          <TabPane tabId="1" className=" animated fadeIn">
            <Form
              form={thisProps.form}
              submission={thisState.submission}
              onChange={submission => this.setState({ submission })}
              onSubmit={async () => {
                thisProps.submitData(thisProps.form, thisState.submission);
                await thisProps.getSubmissionData(thisProps.form);
                thisProps.resetSubmission();
                this.setState({
                  activeTab: '3',
                  submission: null,
                });
              }}
            />
          </TabPane>
          <TabPane tabId="2" className="p-3 animated fadeIn">
            <FormEdit
              {...this.state}
              redirect={this.redirect}
              saveText="Save"
            />
            <Errors message={thisProps.message} type={thisProps.messageType} />
          </TabPane>
          <TabPane tabId="3" className="p-3 animated fadeIn">
            <div
              style={{ display: thisProps.submission ? 'none' : 'block' }}
              className="animated fadeIn"
            >
              {thisState.noSubmission ? (
                <React.Fragment>
                  <Alert color="primary" className="text-center">
                    To control which fields show up in this table, use the
                    `&quot;`Table View`&quot;` setting on each field under
                    `&quot;`Edit Form`&quot;`.
                  </Alert>
                  <Table
                    size="sm"
                    bordered
                    responsive
                    style={{ overflowX: 'initial' }}
                  >
                    <thead>
                      <tr>
                        {thisState.componentList.map((component, index) => (component.tableView && component.key !== 'submit' ? (
                            <th key={index}>{component.label}</th>
                          ) : null))}
                        <th>Submitted By</th>
                        <th>Submitted On</th>
                      </tr>
                    </thead>
                    <tbody>
                      {submissionList.map(submission => (
                        <tr
                          key={submission._id}
                          onClick={(e) => {
                            thisProps.setSubmission({ ...submission });
                          }}
                          className="form-list mx-0 justify-content-center"
                        >
                          {thisState.componentList.map((component, index) => (component.tableView
                            && component.key !== 'submit' ? (
                              <td key={index}>
                                {submission.data[component.key]}
                              </td>
                            ) : null))}
                          <td>{submission.owner}</td>
                          <td>
                            {new Date(
                              submission.created,
                            ).toLocaleString('en-GB', { hour12: true })}
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                  <Row>
                    <Col md="2">
                      <InputGroup>
                        {/* <InputGroupAddon addonType="prepend">
                          <Button
                            color="primary"
                            onClick={async () => {
                              await this.setState({
                                page: `${parseInt(thisState.page, 10) - 1}`,
                              });
                              thisProps.getSubmissionData(
                                thisProps.form,
                                thisState.page,
                              );
                            }}
                            disabled={parseInt(thisState.page, 10) <= 1}
                          >
                            Prev
                          </Button>
                        </InputGroupAddon>
                        <Input
                          name="page"
                          type="number"
                          className="text-center"
                          value={thisState.page}
                          onChange={(e) => {
                            // setTimeout(() => {
                            //   e.target.blur();
                            // }, 1000);
                            this.handleChange(e);
                          }}
                          onFocus={(e) => {
                            if (e.target.value === '') {
                              e.target.value = 1;
                            }
                            e.target.select();
                          }}
                        /> */}
                        {/* <InputGroupAddon addonType="append">
                          <Button
                            color="primary"
                            onClick={async () => {
                              await this.setState({
                                page: `${parseInt(thisState.page, 10) + 1}`,
                              });
                              thisProps.getSubmissionData(
                                thisProps.form,
                                thisState.page,
                              );
                            }}
                            disabled={
                              submissionList
                                ? submissionList.length > 15
                                : false
                            }
                          >
                            Next
                          </Button>
                        </InputGroupAddon> */}
                      </InputGroup>
                    </Col>
                  </Row>
                </React.Fragment>
              ) : (
                <div className="text-center animated fadeIn">
                  <h1>
                    <strong>No Submissions</strong>
                  </h1>
                </div>
              )}
            </div>
            <div
              id="submission-edit"
              style={{ display: thisProps.submission ? 'block' : 'none' }}
              className="animated fadeIn"
            >
              <Row className="justify-content-center">
                <Col md="6" sm="12">
                  {/* <Button
                    color="primary"
                    className="float-left"
                    style={{ cursor: 'pointer' }}
                    onClick={() => {
                      thisProps.resetSubmission();
                      this.setState({ submission: null });
                    }}
                  >
                    Back
                  </Button> */}
                  {/* <Button
                    className="float-right btn-danger"
                    style={{ cursor: "pointer" }}
                    onClick={async () => {
                      await this.props.deleteSubmission(this.state.submission);
                      this.setState({ submission: null });
                      this.props.getSubmissionData(this.props.form);
                    }}
                  >
                    Delete Submission
                  </Button> */}
                </Col>
              </Row>
              <Row className="justify-content-center">
                <Col md="6" sm="12">
                  <Form
                    form={thisProps.form}
                    submission={thisProps.submission}
                    onChange={submission => this.setState({ submission })}
                    onSubmit={async () => {
                      await thisProps.updateSubmission(thisState.submission);
                      this.setState({ submission: null });
                      thisProps.getSubmissionData(thisProps.form);
                    }}
                  />
                </Col>
              </Row>
            </div>
          </TabPane>
        </TabContent>
      </React.Fragment>
    );
  }
}
// FormView.propTypes = propTypes;
// FormView.defaultProps = defaultProps;
const mapStateToProps = state => ({
  form: state.formio.form,
  message: state.formio.message,
  messageType: state.formio.messageType,
  submissionList: state.formio.submissionList
    ? state.formio.submissionList
    : [],
  submission: state.formio.submission,
});
export default connect(mapStateToProps, {
  resetMessage,
  getFormList,
  setForm,
  submitData,
  getSubmissionData,
  updateSubmission,
  setSubmission,
  resetSubmission,
})(FormView);
