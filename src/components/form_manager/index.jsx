import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect,
} from 'react-router-dom';

import {
  Card, Nav, NavItem, TabContent, TabPane,
} from 'reactstrap';
import { connect } from 'react-redux';
// import PropTypes from 'prop-types';

import FormBrowser from './form_browser';
import { FormEdit, Errors } from './form_builder';
import {
  resetMessage,
  resetForm,
  resetAddlFormData,
} from '../../store/action/FormioAction';

// const propTypes = {
//   children: PropTypes.node,
// };

// const defaultProps = {};
class FormManager extends Component {
  constructor(props) {
    super(props);

    // this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab:
        window.location.pathname !== '/admin/formManager/form-builder'
          ? '1'
          : '2',
    };
  }

  toggle = (tab) => {
    const { activeTab } = this.state;
    const thisResetMessage = this.props;
    if (activeTab !== tab) {
      if (activeTab === '2') thisResetMessage.resetMessage();
      // console.log(window.location.pathname);
      this.setState({
        activeTab: tab,
      });
    }
  };

  render() {
    const { activeTab } = this.state;
    const thisProps = this.props;
    return (
      <Router>
        <div className="animated fadeIn">
          <Nav tabs style={{ fontSize: '1.2em' }}>
            <NavItem>
              <Link
                to="/admin/formManager/form-browser"
                className={`${activeTab === '1' && 'active'} nav-link`}
                onClick={() => {
                  this.toggle('1');
                }}
              >
                <i
                  className={`fas fa-clipboard-list ${activeTab === '1'
                    && 'text-primary'}`}
                />{' '}
                FormBrowser
              </Link>
            </NavItem>
            <NavItem>
              <Link
                to="/admin/formManager/form-builder"
                className={`${activeTab === '2' && 'active'} nav-link`}
                onClick={() => {
                  thisProps.resetForm();
                  thisProps.resetAddlFormData();
                  this.toggle('2');
                }}
              >
                <i
                  className={`fas fa-hammer ${activeTab === '2'
                    && 'text-primary'}`}
                />{' '}
                FormBuilder
              </Link>
            </NavItem>
          </Nav>
          <TabContent activeTab={activeTab}>
            <Switch>
              <Redirect
                exact
                from="/admin/formManager"
                to="/admin/formManager/form-browser"
              />
              <Route
                path="/admin/formManager/form-browser"
                render={() => (
                  <TabPane tabId="1">
                    <FormBrowser />
                  </TabPane>
                )}
              />
              <Route
                path="/admin/formManager/form-builder"
                render={() => (
                  <TabPane tabId="2" className="p-3">
                    <Card className="pr-4 pl-2 py-2">
                      <FormEdit reset saveText="Create" />
                      <Errors
                        message={thisProps.message}
                        type={thisProps.messageType}
                      />
                    </Card>
                  </TabPane>
                )}
              />
            </Switch>
          </TabContent>
        </div>
      </Router>
    );
  }
}

const mapStateToProps = state => ({
  message: state.formio.message,
  messageType: state.formio.messageType,
});
export default connect(
  mapStateToProps,
  { resetMessage, resetForm, resetAddlFormData },
)(FormManager);
