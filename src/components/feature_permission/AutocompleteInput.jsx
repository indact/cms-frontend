import React, { Component } from 'react';

import { FormGroup } from 'reactstrap';
import Autocomplete from './Autocomplete';

const style = {
  margin: '0 0 0 0',
};
export default class AutocompleteInput extends Component {
  changeButtonState = (e) => {
    const { buttonClick } = this.props;
    buttonClick(e);
  };

  render() {
    const {
      changeButtonState,
      props: {
        id, setValue, isModified, suggestions,
      },
    } = this;
    return (
      <FormGroup className="pr-3" style={style}>
        <div>
          <Autocomplete
            placeholder={id}
            setValue={setValue}
            isModified={isModified}
            buttonClick={changeButtonState}
            suggestions={suggestions}
          />
        </div>
      </FormGroup>
    );
  }
}
