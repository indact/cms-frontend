// import React, { Component } from 'react';

// import { FormGroup } from 'reactstrap';
// import AutocompleteMultiple from './AutocompleteMultiple';

// const style = {
//   margin: '0 0 0 0',
// };
// export default class AutocompleteMultipleInput extends Component {
//   changeButtonState = (e) => {
//     const { buttonClick } = this.props;
//     buttonClick(e);
//   };

//   render() {
//     const {
//       changeButtonState,
//       props: {
//         id, setValue, isModified, suggestions,
//       },
//     } = this;
//     return (
//       <FormGroup className="pr-1" style={style}>
//         <div>
//           <AutocompleteMultiple
//             placeholder={id}
//             setValue={setValue}
//             isModified={isModified}
//             buttonClick={changeButtonState}
//             suggestions={suggestions}
//           />
//         </div>
//       </FormGroup>
//     );
//   }
// }

import React, { Component } from 'react';
import MultiSelectReact from 'multi-select-react';
import _ from 'lodash';

// import { FormGroup } from 'reactstrap';
import '../../assets/css/multiple.css';

export default class AutocompleteMultipleInput extends Component {
  constructor() {
    super();
    this.state = {
      multiSelect: [],
      selectedOptions: [],
    };
  }

  componentDidUpdate = (prevProps) => {
    const { suggestions, isModified, isReset } = this.props;
    if (
      (!_.isEqual(prevProps.suggestions, suggestions) && isModified)
      || (isReset !== prevProps.isReset && isReset)
    ) {
      this.setState({ multiSelect: suggestions, selectedOptions: [] });
    }
  };

  changeButtonState = (e) => {
    const { buttonClick } = this.props;
    buttonClick(e);
  };

  optionClicked = (optionsList) => {
    this.setState({ multiSelect: optionsList });
    const obj = [];
    const obj1 = [];
    const { selectedOptions } = this.state;
    optionsList.forEach((value) => {
      if (value.value) {
        obj.push(value.id);
        obj1.push(value.label);
      }
    });
    this.setState({ selectedOptions: obj1 });

    let newState1;
    if (obj.length > selectedOptions.length) {
      const newState2 = _.differenceWith(obj1, selectedOptions, _.isEqual);
      newState1 = { id: newState2[0], do: 'add' };
    } else {
      const newState2 = _.differenceWith(selectedOptions, obj1, _.isEqual);
      newState1 = { id: newState2[0], do: 'remove' };
    }
    this.changeButtonState({
      value: obj,
      name: obj1,
      id: 'user',
      find: newState1,
    });
  };

  // when from dropdown is clicked
  selectedBadgeClicked = (optionsList) => {
    this.setState({ multiSelect: optionsList });
    const obj = [];
    const obj1 = [];
    const { selectedOptions } = this.state;
    optionsList.forEach((value) => {
      if (value.value) {
        obj.push(value.id);
        obj1.push(value.label);
      }
    });

    this.setState({ selectedOptions: obj1 });

    let newState1;
    if (obj1.length > selectedOptions.length) {
      const newState2 = _.differenceWith(obj1, selectedOptions, _.isEqual);
      newState1 = { id: newState2[0], do: 'add' };
    } else {
      const newState2 = _.differenceWith(selectedOptions, obj1, _.isEqual);
      newState1 = { id: newState2[0], do: 'remove' };
    }

    this.changeButtonState({
      value: obj,
      name: obj1,
      id: 'user',
      find: newState1,
    });
    // this.changeButtonState({ value: optionsList, id: 'user' });
  };

  render() {
    const { multiSelect } = this.state;
    const selectedOptionsStyles = {
      color: 'white',
      backgroundColor: '#538ad6',
    };
    const optionsListStyles = {
      backgroundColor: '#538ad6',
      color: 'white',
    };
    const divStyle = {
      flex: '0 0 16.66667%',
      minWidth: '16.66667%',
    };
    return (
      <div className="pr-3" style={divStyle}>
        <MultiSelectReact
          options={multiSelect}
          optionClicked={this.optionClicked}
          selectedBadgeClicked={this.selectedBadgeClicked}
          selectedOptionsStyles={selectedOptionsStyles}
          optionsListStyles={optionsListStyles}
        />
      </div>
    );
  }
}

// import React from 'react'
// import ReactTags from 'react-tag-autocomplete'

// require('../../assets/css/multiple.css');

// export default class AutocompleteMultipleInput extends React.Component {
// // export class App extends React.Component {
//   constructor (props) {
//     super(props)

//     this.state = {
//       tags: [
//         { id: 1, name: "Apples" },
//         { id: 2, name: "Pears" }
//       ],
//       suggestions: [
//         { id: 3, name: "Bananas" },
//         { id: 4, name: "Mangos" },
//         { id: 5, name: "Lemons" },
//         { id: 6, name: "Apricots" }
//       ]
//     }
//   }

//   handleDelete (i) {
//     const tags = this.state.tags.slice(0)
//     tags.splice(i, 1)
//     this.setState({ tags })
//   }

//   handleAddition (tag) {
//     const tags = [].concat(this.state.tags, tag)
//     this.setState({ tags })
//   }

//   render () {
//     return (
//       <ReactTags
//         tags={this.state.tags}
//         suggestions={this.state.suggestions}
//         handleDelete={this.handleDelete.bind(this)}
//         handleAddition={this.handleAddition.bind(this)} />
//     )
//   }
// }
