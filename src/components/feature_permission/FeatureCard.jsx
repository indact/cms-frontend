import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Button,
  Card,
  Collapse,
  CardBody,
  CardHeader,
  Table,
} from 'reactstrap';

class FeatureCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapse: false,
    };
  }

  toggle = () => {
    this.setState(prevState => ({ collapse: !prevState.collapse }));
  };

  findfeature = (featureId, n) => {
    const { search_state } = this.props;
    this.setState({ collapse: true });
    search_state(`page_size=0&features=${featureId}`, n);
  };

  modifyPermission = (featue, permission) => {
    const { modifyPermissionFun } = this.props;
    modifyPermissionFun(featue, permission);
  };

  render() {
    const {
      modifyPermission,
      toggle,
      findfeature,
      state: { collapse },
      props: { feature, featureData, toggleSearch },
    } = this;
    let toggleButton;
    let searchButton;
    if (collapse) {
      toggleButton = <i className="icon-arrow-up" />;
    } else {
      toggleButton = <i className="icon-arrow-down" />;
    }
    if (toggleSearch && !collapse) {
      searchButton = (
        <Button
          className="card-header-action btn btn-minimize"
          data-target="#collapseExample"
          onClick={() => findfeature(feature.id, feature)}
        >
          {toggleButton}
        </Button>
      );
    } else {
      searchButton = (
        <Button
          className="card-header-action btn btn-minimize"
          data-target="#collapseExample"
          onClick={toggle}
        >
          {toggleButton}
        </Button>
      );
    }

    return (
      <Card>
        <CardHeader>
          {feature.name}
          <div className="card-header-actions">{searchButton}</div>
        </CardHeader>
        {featureData && (
          <Collapse isOpen={collapse} id="collapseExample">
            <CardBody>
              <Table bordered responsive size="sm">
                <thead>
                  <tr>
                    <th>Username</th>
                    <th>Feature</th>
                    <th>Permissions</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {featureData.map(currentFeature => currentFeature.permissions.map(permission => (
                    <tr key={`${currentFeature.userName}${permission}`}>
                      <td>{currentFeature.userName}</td>
                      <td>{currentFeature.featureName}</td>
                      <td>{permission}</td>
                      <td>
                        <Button
                          data-test="remove_test"
                          size="sm"
                          color="warning"
                          onClick={() => modifyPermission(currentFeature, permission)
                          }
                        >
                            Remove
                        </Button>
                      </td>
                    </tr>
                  )))}
                </tbody>
              </Table>
            </CardBody>
          </Collapse>
        )}
      </Card>
    );
  }
}

export default FeatureCard;
