/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

export default class AutocompleteMultiple extends Component {
  static defaultProps = {
    suggestions: [],
  };

  constructor(props) {
    super(props);

    this.state = {
      // The active selection's index
      activeSuggestion: 0,
      // The suggestions that match the user's input
      filteredSuggestions: [],
      // Whether or not the suggestion list is shown
      showSuggestions: false,
      // What the user has entered
      userInput: [],
      // Disable when modifying
      disabled: false,
      inputId: '',
    };
  }

  onChange = (e) => {
    const { suggestions } = this.props;
    const userInput = e.currentTarget.value;

    // Filter our suggestions that don't contain the user's input
    const filteredSuggestions = suggestions.filter(
      suggestion => suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1,
    );

    this.setState({
      activeSuggestion: 0,
      filteredSuggestions,
      showSuggestions: true,
      //   userInput: e.currentTarget.value,
    });
    if (e.currentTarget.value === '') {
      const { buttonClick } = this.props;
      buttonClick(e);
    }
  };

  onClick = (e) => {
    const allInput = [];
    let remove = false;
    if (e.currentTarget.checked) {
      allInput.push(e.currentTarget.parentElement.innerText);
    } else {
      remove = e.currentTarget.parentElement.innerText;
    }
    const inputId = e.currentTarget.parentElement.id;

    this.setState((prevState) => {
      let previnputs = prevState.userInput;
      if (remove) {
        previnputs = previnputs.filter(element => element !== remove);
      }
      allInput.push(...previnputs);
      // console.log('state', allInput)
      return {
        // activeSuggestion: 0,
        //   filteredSuggestions: [],
        showSuggestions: false,
        userInput: allInput,
        inputId,
      };
    });
    // const sendthis = {
    //   target: { value: allInput, id: e.currentTarget.parentElement.id },
    // };
    // const { buttonClick } = this.props;
    // buttonClick(sendthis);
  };

  onKeyDown = (e) => {
    const { activeSuggestion, filteredSuggestions } = this.state;

    // User pressed the enter key
    if (e.keyCode === 13) {
      this.setState({
        activeSuggestion: 0,
        showSuggestions: false,
        userInput: filteredSuggestions[activeSuggestion],
        inputId: e.currentTarget.id,
      });
      // const sendthis = {
      //   target: {
      //     value: filteredSuggestions[activeSuggestion],
      //     id: e.currentTarget.id,
      //   },
      // };
      // const { buttonClick } = this.props;
      // buttonClick(sendthis);
    } else if (e.keyCode === 38) {
      if (activeSuggestion === 0) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion - 1 });
    } else if (e.keyCode === 40) {
      if (activeSuggestion - 1 === filteredSuggestions.length) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion + 1 });
    }
  };

  resetall = () => {
    this.setState({
      showSuggestions: false,
    });
  };

  getbackall = () => {
    this.setState({
      showSuggestions: true,
    });
  };

  componentDidUpdate = (prevProps) => {
    const { setValue, placeholder, isModified } = this.props;
    if (setValue !== prevProps.setValue) {
      if (placeholder !== 'permission' && isModified !== false) {
        this.setState({ disabled: true });
      }
      if (isModified === false) {
        this.setState({ disabled: false });
      }
      this.setState({ userInput: setValue, showSuggestions: false });
    } else if (
      isModified !== prevProps.isModified
      && isModified !== false
      && placeholder !== 'permission'
    ) {
      this.setState({ disabled: true });
    } else if (isModified !== prevProps.isModified && isModified === false) {
      this.setState({ disabled: false });
    }
  };

  render() {
    const {
      onChange,
      onClick,
      onKeyDown,
      resetall,
      getbackall,
      state: {
        activeSuggestion,
        filteredSuggestions,
        showSuggestions,
        userInput,
        disabled,
        inputId,
      },
      props: { placeholder, buttonClick },
    } = this;

    let suggestionsListComponent;
    if (showSuggestions && userInput) {
      if (filteredSuggestions.length) {
        suggestionsListComponent = (
          <ul className="suggestions">
            {filteredSuggestions.map((suggestion, index) => {
              let className;

              // Flag the active suggestion with a class
              if (index === activeSuggestion) {
                className = 'suggestion-active';
              }

              return (
                // eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions
                <li
                  className={className}
                  key={suggestion}
                  id={placeholder}
                  // eslint-disable-next-line jsx-a11y/click-events-have-key-events
                >
                  <input type="checkbox" onClick={onClick} />
                  {suggestion}
                </li>
              );
            })}
          </ul>
        );
      } else {
        suggestionsListComponent = <div />;
      }
    }
    const sendthis = {
      target: { value: userInput, id: inputId },
    };
    buttonClick(sendthis);
    return (
      <Fragment>
        <input
          type="text"
          className="form-control"
          autoComplete="off"
          id={placeholder}
          placeholder={placeholder}
          onChange={onChange}
          onKeyDown={onKeyDown}
          value={userInput}
          disabled={disabled}
          // onBlur={resetall}
          // onFocus={getbackall}
        />
        {suggestionsListComponent}
      </Fragment>
    );
  }
}

AutocompleteMultiple.propTypes = {
  suggestions: PropTypes.instanceOf(Array),
};
