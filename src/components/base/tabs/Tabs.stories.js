import React from 'react';
import { storiesOf } from '@storybook/react';

import Tabs from './Tabs';

storiesOf('Base', module).add('Tabs', () => <Tabs />);
