import React from 'react';
import { storiesOf } from '@storybook/react';

import ProgressBar from './ProgressBar';

storiesOf('Base', module).add('ProgressBar', () => <ProgressBar />);
