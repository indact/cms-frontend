import React from 'react';
import { storiesOf } from '@storybook/react';

import Tooltips from './Tooltips';

storiesOf('Base', module).add('Tooltips', () => <Tooltips />);
