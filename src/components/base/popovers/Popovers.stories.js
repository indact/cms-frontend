import React from 'react';
import { storiesOf } from '@storybook/react';

import Popovers from './Popovers';

storiesOf('Base', module).add('Popovers', () => <Popovers />);
