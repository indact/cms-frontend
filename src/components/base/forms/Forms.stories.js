import React from 'react';
import { storiesOf } from '@storybook/react';

import Forms from './Forms';

storiesOf('Base', module).add('Forms', () => <Forms />);
