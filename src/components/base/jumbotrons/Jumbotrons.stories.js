import React from 'react';
import { storiesOf } from '@storybook/react';

import Jumbotrons from './Jumbotrons';

storiesOf('Base', module).add('Jumbotrons', () => <Jumbotrons />);
