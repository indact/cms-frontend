import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';

import Alerts from './Alerts';

storiesOf('Notifications', module).add('Alerts', () => <Alerts />);
