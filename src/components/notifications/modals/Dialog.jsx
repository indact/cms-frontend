import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function AlertDialog(prop) {
  return (
    <div>
      <Dialog
        open={prop.Open}
        onClose={prop.handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{prop.title}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {prop.message}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={prop.handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={prop.Agree} color="primary" autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
