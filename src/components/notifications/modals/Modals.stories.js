import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';

import Modals from './Modals';

storiesOf('Notifications', module).add('Modals', () => <Modals />);
