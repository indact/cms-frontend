import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';

import Badges from './Badges';

storiesOf('Notifications', module).add('Badges', () => <Badges />);
