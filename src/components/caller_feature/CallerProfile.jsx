import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Table,
  Col,
  Row,
} from 'reactstrap';
import DateFnsUtils from '@date-io/date-fns';

import {
  KeyboardDateTimePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';

import { getUserProfileData } from '../../store/action/CallerManagementAction';
import { callerTaskCount } from '../../store/action/Dashboard';
import { createTable, heding } from '../../helper/StatTable';

class CallerProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      from: null,
      to: null,
    };
  }

  componentDidUpdate = (prevProps) => {
    const { token, userId, ...action } = this.props;
    const { from, to } = this.state;
    if (prevProps.userId !== userId) {
      action.getUserProfileData(token, userId);
      action.callerTaskCount({
        assignee: userId,
        from,
        to,
      });
    }
  };

  toggleLarge = () => {
    const { userId, ...action } = this.props;
    this.setState({
      from: null,
      to: null,
    });
    action.callerTaskCount({
      assignee: userId,
    });
    action.toggleProfile();
  };

  handleDateChange = (from) => {
    this.setState({ from });
  };

  handleEndDateChange = (to) => {
    this.setState({ to });
  };

  onDataAccept = () => {
    const { userId, ...action } = this.props;
    const { from, to } = this.state;

    const param = {
      assignee: userId,
      from: from !== null ? new Date(from).toJSON() : '',
      to: to !== null ? new Date(to).toJSON() : '',
    };
    action.callerTaskCount(param);
  };

  render() {
    const {
      isOpen, className, userData, callerTotalCount, id,
    } = this.props;
    const { from, to } = this.state;

    return (
      <Modal
        isOpen={isOpen}
        toggle={this.toggleLarge}
        className={`modal-lg ${className}`}
      >
        <ModalHeader toggle={this.toggleLarge}>
          {id === 1 ? 'Caller' : 'User'}Profile
        </ModalHeader>
        <ModalBody>
          Name:{' '}
          {typeof userData.data.data !== 'undefined' && userData.data.data.name}
          <br />
          Mobile:{' '}
          {typeof userData.data.data !== 'undefined'
            && userData.data.data.mobile}
          <br />
          Email:{' '}
          {typeof userData.data.data !== 'undefined'
            && userData.data.data.email}
          {/* <DatePicker/> */}
          <Row>
            <Col sm="5">
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDateTimePicker
                  disableFuture
                  style={{ width: '100%' }}
                  variant="inline"
                  label="Select Start Date"
                  value={from}
                  onChange={this.handleDateChange}
                  onAccept={this.onDataAccept}
                  format="yyyy/MM/dd HH:mm"
                />
              </MuiPickersUtilsProvider>
            </Col>
            <Col sm="5">
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDateTimePicker
                  disableFuture
                  style={{ width: '100%' }}
                  variant="inline"
                  label="Select End Date"
                  value={to}
                  onChange={this.handleEndDateChange}
                  onAccept={this.onDataAccept}
                  format="yyyy/MM/dd HH:mm"
                />
              </MuiPickersUtilsProvider>
            </Col>
          </Row>
          <Table responsive striped>
            <thead>{heding(callerTotalCount.data.data)}</thead>
            <tbody>{createTable(callerTotalCount.data.data)}</tbody>
          </Table>
        </ModalBody>
        <ModalFooter>
          <Button
            color="secondary"
            data-test="user_cancel"
            onClick={this.toggleLarge}
          >
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return {
    userData: state.callerManagement.callerProfile,
    token: state.auth.token,
    callerTotalCount: state.dashboard.callerTotalCount,
  };
}

export default connect(mapStateToProps, {
  getUserProfileData,
  callerTaskCount,
})(CallerProfile);
