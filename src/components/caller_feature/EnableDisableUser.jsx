import React, { Component } from 'react';
import { AppSwitch } from '@coreui/react';

class EnableDisableUser extends Component {
  enableUser = (e) => {
    const { handleClick } = this.props;
    handleClick(!e.target.checked, e.target.dataset.id);
  };

  render() {
    const { enabled, valueData } = this.props;
    let enablebutton;
    if (enabled) {
      return (
        <AppSwitch
          data-test="disable_user"
          className="mx-1"
          size="sm"
          data-id={valueData.id}
          onClick={this.enableUser}
          color="primary"
          variant="3d"
        />
      );
      // eslint-disable-next-line no-else-return
    } else {
      return (
        <AppSwitch
          data-test="enable_user"
          className="mx-1"
          size="sm"
          data-id={valueData.id}
          onClick={this.enableUser}
          checked
          color="primary"
          variant="3d"
        />
      );
    }
  }
}

export default EnableDisableUser;
