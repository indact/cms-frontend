import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Button, Modal, ModalBody, ModalFooter, ModalHeader,
} from 'reactstrap';

import { getUserProfileData } from '../../store/action/CallerManagementAction';

class AddStatus extends Component {
  componentDidUpdate = (prevProps) => {
    const thisProps = this.props;
    if (prevProps.userId !== thisProps.userId) {
      thisProps.getUserProfileData(thisProps.token, thisProps.userId);
    }
  };

  toggleLarge = () => {
    const thisProps = this.props;
    thisProps.toggleProfile();
  };

  render() {
    const { isOpen, className, userData } = this.props;
    return (
      <Modal
        isOpen={isOpen}
        toggle={this.toggleLarge}
        className={`modal-lg ${className}`}
      >
        <ModalHeader toggle={this.toggleLarge}>User Profile</ModalHeader>
        <ModalBody>
          Name:{' '}
          {typeof userData.data.data !== 'undefined' && userData.data.data.name}
          <br />
          Mobile:{' '}
          {typeof userData.data.data !== 'undefined'
            && userData.data.data.mobile}
          <br />
          Email:{' '}
          {typeof userData.data.data !== 'undefined'
            && userData.data.data.email}
        </ModalBody>
        <ModalFooter>
          {/* <Button color="primary" onClick={this.toggleLarge}>
            Do Something
          </Button>{' '} */}
          <Button color="secondary" onClick={this.toggleLarge}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}
function mapStateToProps(state) {
  return {
    userData: state.callerManagement.callerProfile,
    token: state.auth.token,
  };
}

export default connect(
  mapStateToProps,
  {
    getUserProfileData,
  },
)(AddStatus);
