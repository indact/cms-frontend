import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';

import Flags from './Flags';

storiesOf('icons', module).add('Flags', () => <Flags />);
