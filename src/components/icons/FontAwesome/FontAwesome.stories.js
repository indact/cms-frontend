import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';

import FontAwesome from './FontAwesome';

storiesOf('icons', module).add('FontAwesome', () => <FontAwesome />);
