import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';

import SimpleLineIcons from './SimpleLineIcons';

storiesOf('icons', module).add('SimpleLineIcons', () => <SimpleLineIcons />);
