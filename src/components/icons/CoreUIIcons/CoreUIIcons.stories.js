import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';

import CoreUIIcons from './CoreUIIcons';

storiesOf('icons', module).add('CoreUIIcons', () => <CoreUIIcons />);
