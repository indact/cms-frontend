import React from 'react';
import { storiesOf } from '@storybook/react';

import Charts from './Charts';

storiesOf('Charts', module).add('Charts', () => <Charts />);
