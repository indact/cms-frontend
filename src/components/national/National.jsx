import React, { Component, lazy, Suspense } from 'react';
import Cookies from 'universal-cookie';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import {
  Card,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Table,
  Row,
  Col,
} from 'reactstrap';
import {
  KeyboardDateTimePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { nationalTaskCount } from '../../store/action/Dashboard';
import { getNationalMissedCallCount } from '../../store/action/MissedCallExotel';
import { calculatesumofcalls } from '../../helper/StatTable';
import StatsCard from '../statscard/StatsCard';

const cookies = new Cookies();

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      from: null,
      to: null,
    };
  }

  componentDidMount = async () => {
    const { ...action } = this.props;
    await action.nationalTaskCount();
    await action.getNationalMissedCallCount();
  };

  handleDateChange = (event) => {
    this.setState({ from: event });
    const { ...action } = this.props;
    const { from, to } = this.state;

    const param = {
      from: from !== null ? new Date(from).toJSON() : '',
      to: to !== null ? new Date(to).toJSON() : '',
    };
    action.getNationalMissedCallCount(param);
  };

  handleEndDateChange = (event) => {
    this.setState({ to: event });
    const { ...action } = this.props;
    const { from, to } = this.state;

    const param = {
      from: from !== null ? new Date(from).toJSON() : '',
      to: to !== null ? new Date(to).toJSON() : '',
    };
    action.getNationalMissedCallCount(param);
  };

  render() {
    const {
      nationalTotalCount,
      LocationName,
      exotelNationalCount,
    } = this.props;
    const { from, to } = this.state;
    const singalstate = [];
    singalstate.push({
      name: LocationName,
      data: nationalTotalCount.data.data,
    });

    return (
      <div className="animated fadeIn">
        <Grid container spacing={3}>
          <Grid item xs={4}>
            <StatsCard
              icon="fa fa-database"
              title="TO DO"
              value={
                nationalTotalCount.data.length !== 0
                  ? nationalTotalCount.data.data['To Do']
                  : 0
              }
              color="red"
            />
          </Grid>
          <Grid item xs={4}>
            <StatsCard
              icon="fa fa-cogs"
              title="IN PROGRESS"
              value={
                nationalTotalCount.data.length !== 0
                  ? nationalTotalCount.data.data['In Progress']
                  : 0
              }
              color="rgb(255, 129, 0)"
            />
          </Grid>
          <Grid item xs={4}>
            <StatsCard
              icon="fa fa-check-circle"
              title="COMPLETED"
              value={
                nationalTotalCount.data.length !== 0
                  ? nationalTotalCount.data.data.Done
                  : 0
              }
              color="rgb(45, 167, 105)"
            />
          </Grid>
        </Grid>

        <Grid container item xs={12}>
          <Card
            style={{
              height: '200px',
              width: '100%',
              padding: '25px',
              marginTop: ' 50px',
            }}
          >
            <Row>
              <Col sm="5">
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDateTimePicker
                    disableFuture
                    style={{ width: '100%' }}
                    variant="inline"
                    label="Select Start Date"
                    value={from}
                    onChange={this.handleDateChange}
                    format="yyyy/MM/dd "
                  />
                </MuiPickersUtilsProvider>
              </Col>
              <Col sm="5">
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDateTimePicker
                    disableFuture
                    style={{ width: '100%' }}
                    variant="inline"
                    label="Select End Date"
                    value={to}
                    onChange={this.handleEndDateChange}
                    format="yyyy/MM/dd"
                  />
                </MuiPickersUtilsProvider>
              </Col>
            </Row>
            <div
              style={{
                fontWeight: 'bold',
                display: 'flex',
                justifyContent: 'space-between',
                fontSize: 'x-large',
                marginTop: '32px',
                marginLeft: '27px',
              }}
            >
              Total Missed Calls:{' '}
              {exotelNationalCount.data.length !== 0
                ? exotelNationalCount.data.data.totalmissedcall
                : 0}
              <br></br>
              Total Unique Calls:{' '}
              {exotelNationalCount.data.length !== 0
                ? exotelNationalCount.data.data.uniquecalls
                : 0}
            </div>
          </Card>
        </Grid>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    nationalTotalCount: state.dashboard.nationalTotalCount,
    exotelNationalCount: state.exotel.exotelNationalCount,
  };
}

export default connect(mapStateToProps, {
  nationalTaskCount,
  getNationalMissedCallCount,
})(Dashboard);
