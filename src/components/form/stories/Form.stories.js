import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';

import Email from '../Email';
import Mobile from '../Mobile';
import Name from '../Name';
import Password from '../Password';
import Username from '../Username';

storiesOf('CustomForm', module).add(
  'Form',
  () => (
    <Fragment>
      <Email required="true" />
      <Mobile required="true" />
      <Name required="true" />
      <Password required="true" />
      <Username required="true" />
    </Fragment>
  ),
  {
    notes:
      '5 components are used here <br><br> 1. Email <br> 2. Mobile <br> 3. Name <br> 4. Password <br> 5. Username',
  },
);
