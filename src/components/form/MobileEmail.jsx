import React, { Component } from 'react';
import {
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  FormFeedback,
  FormGroup,
  FormText,
} from 'reactstrap';

class MobileEmail extends Component {
  constructor(props) {
    super(props);
    const { required } = this.props;
    this.state = {
      validate: {},
      // eslint-disable-next-line react/no-unused-state
      required: [required],
      margin: required ? 'mb-3' : 'mb-1',
    };
  }

  onChange = (e) => {
    const { onChange } = this.props;
    this.validate(e);
    onChange(e);
  };

  validate = (e) => {
    const { name } = e.target;

    const Rex = name === 'Mobile'
        ? /^[1-9]\d{9}$/
        : /^([a-zA-Z0-9_\-.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    const { validate } = this.state;
    const { onValidation, required } = this.props;

    if (!e.target.value) {
      if (required) {
        validate[name] = 'empty';
      } else {
        validate[name] = 'success';
      }
    } else if (Rex.test(e.target.value)) {
      validate[name] = 'success';
    } else {
      validate[name] = 'danger';
    }
    const state = validate[name] === 'success';
    onValidation(name, state);
    this.setState({ validate });
  };

  render() {
    const { submit, type, placeholder } = this.props;
    const { validate, margin, required } = this.state;
    const name = type;
    console.log('name');
    console.log(name);

    return (
      <FormGroup>
        <InputGroup className={margin}>
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className="icon-user" />
            </InputGroupText>
          </InputGroupAddon>
          {name === 'Mobile' ? (
            <>
              <Input
                name={name}
                placeholder={placeholder}
                onChange={this.onChange}
                type="text"
                autoComplete="Mobile Or Email"
                maxLength="10"
                minLength="10"
                valid={validate[name] === 'success'}
                invalid={
                  validate[name] === 'danger'
                  || validate[name] === 'empty'
                  || submit
                }
              />
              <FormFeedback invalid="true">
                {validate[name] === 'empty' || submit
                  ? 'Mobile number cannot be empty'
                  : 'Incorrect number entered. Number cannot start with 0.Cannot contain letters.Must contain 10 digits'}
              </FormFeedback>
            </>
          ) : (
            <>
              <Input
                name={name}
                placeholder={placeholder}
                onChange={this.onChange}
                type="text"
                autoComplete="email"
                valid={validate[name] === 'success'}
                invalid={
                  validate[name] === 'danger'
                  || validate[name] === 'empty'
                  || submit
                }
              />
              <FormFeedback invalid="true">
                {validate[name] === 'empty' || submit
                  ? 'Email cannot be empty'
                  : 'Email format is incorrect. Correct format: someone@example.com'}
              </FormFeedback>
            </>
          )}
        </InputGroup>

        {/* {required.map((e) => {
          if (!e) {
            return <FormText key="notRequired">Mobile is optional</FormText>;
          }
          return null;
        })} */}
      </FormGroup>
    );
  }
}

export default MobileEmail;
