import React, { Component } from 'react';
import {
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  FormFeedback,
  FormGroup,
  Row,
  Col,
} from 'reactstrap';

class Name extends Component {
  constructor(props) {
    super(props);
    const { required } = this.props;
    this.state = {
      validate: {},
      // eslint-disable-next-line react/no-unused-state
      required: {
        first_name: required.firstName,
        last_name: required.lastName,
      },
    };
  }

  onChange = (e) => {
    const { onChange } = this.props;
    this.validate(e);
    onChange(e);
  };

  validate = (e) => {
    const { validate, required } = this.state;
    const { onValidation } = this.props;
    const { name } = e.target;
    if (!e.target.value && required[name]) {
      validate[name] = 'empty';
    } else {
      validate[name] = 'success';
    }
    const state = validate[name] === 'success';
    onValidation(name, state);
    this.setState({ validate });
  };

  render() {
    const { submit } = this.props;
    const { validate } = this.state;
    const { first_name, last_name } = validate;
    return (
      <Row>
        <Col xs="12" sm="6">
          <FormGroup>
            <InputGroup className="mb-3">
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <i className="icon-emotsmile" />
                </InputGroupText>
              </InputGroupAddon>
              <Input
                name="first_name"
                onChange={this.onChange}
                type="text"
                placeholder="First Name"
                autoComplete="first_name"
                valid={first_name === 'success'}
                invalid={first_name === 'empty' || submit}
              />
              <FormFeedback invalid="true">First Name is required</FormFeedback>
            </InputGroup>
          </FormGroup>
        </Col>
        <Col xs="12" sm="6">
          <InputGroup className="mb-3">
            <InputGroupAddon addonType="prepend">
              <InputGroupText>
                <i className="icon-emotsmile" />
              </InputGroupText>
            </InputGroupAddon>
            <Input
              name="last_name"
              onChange={this.onChange}
              type="text"
              placeholder="Last Name"
              autoComplete="last_name"
              valid={last_name === 'success'}
              invalid={last_name === 'empty' || submit}
            />
            <FormFeedback invalid="true">Last Name is required</FormFeedback>
          </InputGroup>
        </Col>
      </Row>
    );
  }
}

export default Name;
