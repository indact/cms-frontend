import React, { Component } from 'react';

import {
  Button, Modal, ModalBody, ModalFooter, ModalHeader,
} from 'reactstrap';

class AddState extends Component {
  render() {
    return (
      <div>
        <Modal
          isOpen={this.props.open}
          className={`modal-primary ${this.props.className}`}
        >
          <ModalHeader toggle={this.togglePrimary}>
            Add New State In This Policy
          </ModalHeader>
          <ModalBody>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum.
          </ModalBody>
          <ModalFooter>
            <Button color="primary">Submit</Button>{' '}
            <Button color="secondary" onClick={this.props.onHide}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default AddState;
