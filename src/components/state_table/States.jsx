import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {
  Badge,
  Card,
  CardBody,
  CardHeader,
  Tooltip,
  Table,
  Button,
} from 'reactstrap';
import AddState from './AddState';
// import usersData from "./UsersData";

function UserRow(props) {
  const { user } = props;
  const userLink = `/admin/PolicyState/${user.id}`;

  const getBadge = status => (status === 'Active'
      ? 'success'
      : status === 'Inactive'
      ? 'secondary'
      : status === 'Pending'
      ? 'warning'
      : status === 'Banned'
      ? 'danger'
      : 'primary');

  return (
    <tr key={user.id.toString()}>
      <th scope="row">
        <Link to={userLink}>{user.id}</Link>
      </th>
      <td>
        <Link to={userLink}>{user.name}</Link>
      </td>
      <td>{user.registered}</td>
      <td>{user.role}</td>
      <td>
        <Link to={userLink}>
          <Badge color={getBadge(user.status)}>{user.status}</Badge>
        </Link>
      </td>
    </tr>
  );
}

class Users extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tooltipOpen: [false, false],
      primary: false,
    };
  }

  toggle = (i) => {
    const newArray = this.state.tooltipOpen.map((element, index) => (index === i ? !element : false));
    this.setState({
      tooltipOpen: newArray,
    });
  };

  togglePrimary = () => {
    this.setState({
      primary: !this.state.primary,
    });
  };

  render() {
    const userList = this.props.item.filter(user => user.id < 7);

    const lgClose = () => this.setState({ primary: false });

    return (
      <div className="animated fadeIn">
        <AddState open={this.state.primary} onHide={lgClose} />

        <Card>
          <CardHeader>
            {this.props.header}
            <div className="card-header-actions">
              <Button
                color="success"
                onClick={this.togglePrimary}
                className="mr-1"
                id={`DisabledAutoHide${this.props.tooltipno}`}
              >
                <i className="icon-plus " />
              </Button>

              <Tooltip
                placement="top"
                isOpen={this.state.tooltipOpen[this.props.tooltipno]}
                autohide={false}
                target={`DisabledAutoHide${this.props.tooltipno}`}
                toggle={() => {
                  this.toggle(this.props.tooltipno);
                }}
              >
                {this.props.tooltipcontent}
              </Tooltip>
            </div>
          </CardHeader>
          <CardBody>
            <Table responsive hover>
              <thead>
                <tr>
                  <th scope="col">State id</th>
                  <th scope="col">State</th>
                  <th scope="col">registered</th>
                  <th scope="col">role</th>
                  <th scope="col">status</th>
                </tr>
              </thead>
              <tbody>
                {userList.map((user, index) => (
                  <UserRow key={index} user={user} />
                ))}
              </tbody>
            </Table>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default Users;
