import Colors from './colors/Colors';
import Typography from './typography/Typography';

export { Colors, Typography };
