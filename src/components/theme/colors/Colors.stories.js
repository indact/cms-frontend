import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';

import Colors from './Colors';

storiesOf('theme', module).add('Colors', () => <Colors />);
