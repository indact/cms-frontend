import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';

import Typography from './Typography';

storiesOf('theme', module).add('Typography', () => <Typography />);
