import React, { Component } from 'react';

import { connect } from 'react-redux';
import Cookies from 'universal-cookie';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';

import userActions from '../../store/action/UserAction';

import '../../assets/css/Profile.css';
import { FileSystemNavigator, Password, UserPassword } from '../../components';
import Img from '../../assets/img/brand/blank-profile-picture-973460_640.png';
import Toast from '../../components/notifications/toast/Toast';

const { getUserData, updateUserData } = userActions;

const cookies = new Cookies();
class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: '',
      oldpassword: 'old',
      loader: false,
      Snackbars: true,
      repassword: '',
      error: {
        password: false,
        repassword: false,
      },
      validation: {
        password: false,
        repassword: false,
      },
      features: [],
    };
  }

  componentDidMount = async () => {
    const thisProps = this.props;
    // if (!thisProps.infoStatus || thisProps.infoStatus !== 200)
    thisProps.getUserData(
      (await cookies.get('userBasicDetail').id) || thisProps.user.id,
    );
    // this.setState({
    //   features: thisProps.getAllFeatures(),
    // });
  };

  handleValidation = async (name, state) => {
    const { validation } = this.state;
    validation[name] = state;
    await this.setState({ validation });
  };

  handleChange = (event) => {
    const oldState = { ...this.state };
    const { error } = { ...this.state };
    if (event.target.name) {
      error[event.target.name] = false;
      this.setState({ ...error });
    }
    oldState[event.target.name] = event.target.value.trim();
    this.setState({ ...oldState });
  };

  handelError = (name) => {
    const { error } = { ...this.state };
    error[name] = true;
    this.setState({ ...error });
  };

  submit = (e) => {
    e.preventDefault();
    const {
      password,

      validation,
      oldpassword,
    } = this.state;

    let success = true;
    Object.values(validation).map((value) => {
      success = success && value;
      return success;
    });
    if (success) {
      const ResetPasswordFun = this.props;

      ResetPasswordFun.updateUserData({
        newPassword: password,
        oldPassword: oldpassword,
      });
      this.setState({ Snackbars: true });
    } else if (password === '') {
      this.handelError('password');
    }
  };

  render() {
    const thisProps = this.props;
    const thisState = this.state;
    const {
      name, email, mobile, states,
    } = thisProps.user;
    return (
      <Grid container spacing={3}>
        {thisProps.updateStatus !== 5896 ? (
          <Toast
            open={thisState.Snackbars}
            handleClose={() => this.setState({ Snackbars: false })}
            message={
              thisProps.updateStatus === 200
                ? 'Password successfully Updated.'
                : thisProps.updateResponce.message || ''
            }
            variant={thisProps.updateStatus === 200 ? 'success' : 'error'}
            duration={6000}
          />
        ) : (
          ''
        )}

        <Grid container spacing={1}>
          <AppBar
            position="static"
            color="default"
            style={{ margin: '0 0px -8px 7px', zIndex: '0 !important' }}
          >
            <Toolbar>
              <Typography variant="h6" color="inherit">
                User Profile
              </Typography>
            </Toolbar>
          </AppBar>

          <Grid item xs={12} sm={8}>
            <Box
              boxShadow={3}
              bgcolor="background.paper"
              m={1}
              p={2}
              style={{ width: '100%', minheight: '30rem', maxheight: '30rem' }}
            >
              <Container maxWidth="sm">
                <Grid container spacing={3}>
                  <Grid item xs={12} style={{ margin: '0 40px 0 0' }}>
                    <Avatar
                      alt="Remy Sharp"
                      src={Img}
                      style={{
                        margin: 10,
                        width: 125,
                        height: 125,
                        display: 'block',
                        marginLeft: ' auto',
                        marginRight: ' auto',
                      }}
                    />
                    <div
                      style={{
                        textAlign: ' center',
                        textTransform: 'capitalize',
                        fontFamily: 'serif',
                        fontSize: 'x-large',
                      }}
                    >
                      {' '}
                      {name}
                    </div>
                  </Grid>

                  <Grid
                    item
                    xs={5}
                    style={{
                      textAlign: ' end',
                      fontFamily: 'serif',
                      fontSize: 'x-large',
                    }}
                  >
                    {mobile}
                  </Grid>
                  <Grid
                    item
                    xs={1}
                    style={{
                      textAlign: ' center',
                      fontFamily: 'serif',
                      fontSize: 'x-large',
                    }}
                  >
                    ||
                  </Grid>

                  <Grid
                    item
                    xs={6}
                    style={{
                      textAlign: ' center',
                      fontFamily: 'serif',
                      fontSize: 'x-large',
                    }}
                  >
                    {email}
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    style={{
                      // textAlign: ' center',
                      fontFamily: 'serif',
                      fontSize: 'x-large',
                    }}
                  >
                    <div>Update Password :</div>
                    <div>
                      <UserPassword
                        name="oldpassword"
                        required
                        onChange={this.handleChange}
                        onValidation={this.handleValidation}
                        submit={thisState.error.password}
                        placeholder="Old Password"
                      />
                      <Password
                        placeholder="New Password"
                        onChange={this.handleChange}
                        onValidation={this.handleValidation}
                        submit={thisState.error.password}
                      />
                    </div>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={this.submit}
                    >
                      Update
                    </Button>
                  </Grid>
                </Grid>
              </Container>
            </Box>
          </Grid>
          <Grid item xs={12} sm={4}>
            <Box
              boxShadow={3}
              bgcolor="background.paper"
              m={1}
              p={3}
              style={{ width: '100%', height: '525px', overflow: 'auto' }}
            >
              <h3
                style={{
                  textAlign: 'center',
                  paddingBottom: '25px',
                }}
              >
                States And Permissions{' '}
              </h3>
              <FileSystemNavigator states={states} />
            </Box>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.info,
  updateStatus: state.user.updateStatus,
  updateResponce: state.user.updateResponce,
});

export default connect(mapStateToProps, { getUserData, updateUserData })(
  Profile,
);
