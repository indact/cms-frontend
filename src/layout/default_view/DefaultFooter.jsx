import React from 'react';

const DefaultFooter = () => (
  <React.Fragment>
    <span>Powered By Indus Action</span>
  </React.Fragment>
);

export default DefaultFooter;
