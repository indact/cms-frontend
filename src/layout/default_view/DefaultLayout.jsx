import React, { Component, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Container } from 'reactstrap';
import Cookies from 'universal-cookie';

import {
  AppAside,
  AppBreadcrumb,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav,
} from '@coreui/react';
import { connect } from 'react-redux';
import DefaultAside from '../../features/aside_window/DefaultAside';
import { Spinner } from '../../components';
// import Toast from '../../components/notifications/toast/Toast';
import Notification from './Notifications';
import userActions from '../../store/action/UserAction';

const baseUrl = `${process.env.REACT_APP_BASE_URL}:${process.env.REACT_APP_CMS_PORT}`;

const {
  getStatesData,
  getRoutesData,
  getSidebarData,
  getStateData,
  getStatePermissionsData,
} = userActions;

const cookies = new Cookies();
const DefaultFooter = React.lazy(() => import('./DefaultFooter'));
const DefaultHeader = React.lazy(() => import('./DefaultHeader'));

class DefaultLayout extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      cookieStatus: false,
    };
  }

  componentDidMount = async () => {
    // eslint-disable-next-line react/destructuring-assignment
    const token = this.props.token || (await cookies.get('token'));
    const { sideBar, ...action } = this.props;
    let { stateList } = this.props;
    stateList = cookies.get('stateList') || stateList;
    await fetch(`${baseUrl}/user/states`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json; charset=urf-8',
        Accept: 'application/json, text/plain, */*',
        authorization_token: token || '',
      },
    }).then(async (response) => {
      if (response.status === 200) {
        await action.getStateData();
        await action.getStatesData(token);
      } else {
        await cookies.remove('token');
        await cookies.remove('stateList');
        await cookies.remove('state');
        await cookies.remove('userBasicDetail');
        await cookies.remove('features');
      }
      return 1;
    });
    await action.getStateData();
    await action.getStatesData(stateList, token);
    if (!sideBar.items.length) {
      const features = await cookies.get('features');
      if (features) {
        await action.getSidebarData(features);
        await action.getRoutesData(features);
      }
    }
  };

  componentDidUpdate = async (prev) => {
    const { state, token, ...action } = this.props;
    if (prev.state !== state) {
      await action
        .getStatePermissionsData(state, token || cookies.get('token'))
        .then(async (features = cookies.get('features')) => {
          await action.getSidebarData(features);
          await action.getRoutesData(features);
        });
    }
  };

  loading = () => <Spinner className="animated fadeIn pt-3 text-center" />;

  render() {
    // const thisState = this.state;
    const { sideBar, routes, location } = this.props;
    let { stateList, token } = this.props;
    const { cookieStatus } = this.state;
    token = token || cookies.get('token');
    stateList = stateList || cookies.get('stateList') || [];
    if (!token) {
      // cookies.set('prevPath', location.pathname, { path: '/' });
      return <Redirect true from="*" to="/" />;
    }
    const destination = '/';
    // Check if token is valid
    fetch(`${baseUrl}/user/states`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json; charset=urf-8',
        Accept: 'application/json, text/plain, */*',
        authorization_token: token || '',
      },
    }).then(async (response) => {
      if (response.status !== 200) {
        await this.setState({ cookieStatus: true });
        // await cookies.remove('token');
        // await cookies.remove('stateList');
        // await cookies.remove('state');
        // await cookies.remove('userBasicDetail');
        // await cookies.remove('features');
      }
    });
    if (cookieStatus) {
      return <Redirect true push from="*" to={destination} />;
    }
    const prevPath = cookies.get('prevPath');
    if (
      routes.filter(route => route.path === prevPath).length !== 0
      && location.pathname !== prevPath
    ) {
      return <Redirect true from="*" to={prevPath} />;
    }

    return (
      <div className="app">
        <Notification />
        {/* <Toast
          open={thisState.Snackbars}
          handleClose={() => this.setState({ Snackbars: false })}
          message="This is for testing "
          variant="success"
          duration={6000}
        /> */}
        <AppHeader fixed>
          <Suspense fallback={this.loading()}>
            {/* according to user change defaultheader */}
            <DefaultHeader />
          </Suspense>
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg">
            <AppSidebarHeader />
            <AppSidebarForm />
            <Suspense>
              <AppSidebarNav navConfig={sideBar} {...this.props} />
            </Suspense>
            <AppSidebarFooter />
            <AppSidebarMinimizer />
          </AppSidebar>
          <main className="main">
            <AppBreadcrumb appRoutes={routes} />
            <Container fluid>
              <Suspense fallback={this.loading()}>
                <Switch>
                  {routes.map((route, idx) => (route.component ? (
                      <Route
                        // eslint-disable-next-line react/no-array-index-key
                        key={idx}
                        path={route.path}
                        exact={route.exact}
                        name={route.name}
                        render={props => <route.component {...props} />}
                      />
                    ) : null))}
                  <Redirect true from="/admin" to="/admin/default" />;
                </Switch>
              </Suspense>
            </Container>
          </main>
          <AppAside fixed>
            <Suspense fallback={this.loading()}>
              <DefaultAside />
            </Suspense>
          </AppAside>
        </div>
        <AppFooter>
          <Suspense fallback={this.loading()}>
            <DefaultFooter />
          </Suspense>
        </AppFooter>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  token: state.auth.token,
  userId: state.auth.userKey,
  // userType: state.user.info.user_type,
  sideBar: state.user.sideBar,
  routes: state.user.routes,
  state: state.user.state,
  stateList: state.user.states,
});

export default connect(
  mapStateToProps,
  {
    getStatesData,
    getRoutesData,
    getSidebarData,
    getStateData,
    getStatePermissionsData,
  },
)(DefaultLayout);
