import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { connect } from 'react-redux';
import {
  Card,
  CardBody,
  Row,
  Col,
  ListGroup,
  ListGroupItem,
  Form,
  FormGroup,
  Input,
  Button,
} from 'reactstrap';
import userActions from '../../store/action/UserAction';
import '../../assets/css/Profile.css';

const { getUserData, updateUserData } = userActions;

const cookies = new Cookies();

class Settings extends Component {
  state = {
    firstName: '',
    lastName: '',
    email: '',
    mobile: '',
    user_type: '',
    validation: {},
  };

  componentDidMount = () => {
    const { user, ...action } = this.props;
    // if (!thisProps.infoStatus || thisProps.infoStatus !== 200)
    const id = cookies.get('userBasicDetail').id || user.id;
    action.getUserData(id);
    // this.se
  };

  handleChange = async (e) => {
    e.persist();
    const { name } = e.target;
    await this.setState({
      [name]: e.target.value,
    });
    if (name === 'email' || name === 'mobile') this.validate(name);
  };

  submit = async (e, name) => {
    e.preventDefault();
    const data = {};
    const thisState = this.state;
    const thisProps = this.props;
    if (name === 'email' || name === 'mobile') {
      if (
        thisState.validation[name]
        && thisState[name] !== thisProps.user[name]
      ) {
        data[name] = thisState[name];
        await this.setState({
          [name]: '',
          validation: { ...thisState.validation, [name]: null },
        });
        console.log(thisState);
      }
    } else if (name === 'name') {
      if (
        thisState.firstName !== ''
        && thisState.firstName !== thisProps.user.first_name
      ) {
        data.first_name = thisState.firstName;
        await this.setState({ firstName: '' });
      }
      if (
        thisState.lastName !== ''
        && thisState.lastName !== thisProps.user.last_name
      ) {
        data.last_name = thisState.lastName;
        await this.setState({ lastName: '' });
      }
    }
    if (Object.keys(data).length !== 0) thisProps.updateUserData(data);
  };

  validate = async (name) => {
    const thisState = this.state;
    const { validation } = this.state;
    switch (name) {
      case 'mobile':
        // eslint-disable-next-line
        const mobileRex = /^[1-9]\d{9}$/;
        validation.mobile = mobileRex.test(thisState.mobile);
        break;
      case 'email':
        // eslint-disable-next-line
        const emailRex = /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        validation.email = emailRex.test(thisState.email);
        break;
      default:
        break;
    }
    this.setState({ validation });
  };

  render() {
    const thisState = this.state;
    const { infoStatus, user, ...action } = this.props;
    if (infoStatus !== 200) action.getUserData();
    const {
      id, first_name, last_name, email, mobile, username,
    } = user || {};
    return (
      <Row>
        <Card
          className="border-0 rounded-0"
          style={{
            background: 'rgba(0,0,0,0.05)',
            width: '10%',
          }}
        >
          <CardBody>
            <ListGroup>
              <ListGroupItem>
                {/* image  */}
                {username}
              </ListGroupItem>
              <Link
                to="/admin/profile"
                className="list-group-item"
                style={{
                  textDecoration: 'none',
                  color: 'inherit',
                }}
              >
                <i className="fa fa-user" />
                &nbsp; Profile
              </Link>
              <Link
                to="/admin/settings"
                className="list-group-item"
                style={{
                  textDecoration: 'none',
                  color: 'inherit',
                }}
              >
                <i className="fas fa-tools" />
                &nbsp; Settings
              </Link>
            </ListGroup>
          </CardBody>
        </Card>
        <Card
          className="rounded-0 scroll-sm"
          style={{
            height: '78.5vh',
            overflow: 'auto',
            border: 'none',
            width: '90%',
          }}
        >
          <CardBody>
            #{id} {username}&apos;s Profile
            <div className="pt-3">
              <h2>Basic Info</h2>
            </div>
            <Row className="f11 mb-2 ">
              <Col md={1}>Name</Col>
              <Col md={11}>
                <Form inline>
                  <Row className="w-100">
                    <Col md={4} className="px-0">
                      <FormGroup className="mr-2 mb-0">
                        <Input
                          className="w-100"
                          type="firstName"
                          name="firstName"
                          id="firstName"
                          placeholder={first_name}
                          value={thisState.firstName}
                          onChange={this.handleChange}
                        />
                      </FormGroup>
                    </Col>
                    <Col md={4} className="px-0">
                      <FormGroup className="mr-2 mb-0">
                        <Input
                          className="w-100"
                          type="lastName"
                          name="lastName"
                          id="lastName"
                          placeholder={last_name}
                          value={thisState.lastName}
                          onChange={this.handleChange}
                        />
                      </FormGroup>
                    </Col>
                    <Col md={1}>
                      <Button
                        className="badge badge-pill badge-success"
                        type="submit"
                        onClick={e => this.submit(e, 'name')}
                      >
                        <i className="fas fa-check" />
                      </Button>
                    </Col>
                  </Row>
                </Form>
              </Col>
            </Row>
            <Row className="f11 mb-2  align-middle">
              <Col md={1}>Email</Col>
              <Col md={11}>
                <Form inline>
                  <Row className="w-100">
                    <Col md={8} className="px-0">
                      <FormGroup className="mr-2 mb-0">
                        <Input
                          className="w-100"
                          type="email"
                          name="email"
                          id="email"
                          placeholder={email}
                          value={thisState.email}
                          onChange={this.handleChange}
                          valid={thisState.validation.email === true}
                          invalid={thisState.validation.email === false}
                        />
                      </FormGroup>
                    </Col>
                    <Col md={1}>
                      <Button
                        className="badge badge-pill badge-success"
                        type="submit"
                        onClick={e => this.submit(e, 'email')}
                      >
                        <i className="fas fa-check" />
                      </Button>
                    </Col>
                  </Row>
                </Form>
              </Col>
            </Row>
            <Row className="f11">
              <Col md="1">Mobile</Col>
              <Col md="11">
                <Form inline>
                  <Row className="w-100">
                    <Col md={4} className="px-0">
                      <FormGroup className="mr-2 mb-0">
                        <Input
                          className="w-100"
                          type="mobile"
                          name="mobile"
                          id="mobile"
                          maxLength="10"
                          minLength="10"
                          placeholder={mobile}
                          value={thisState.mobile}
                          onChange={this.handleChange}
                          valid={thisState.validation.mobile === true}
                          invalid={thisState.validation.mobile === false}
                        />
                      </FormGroup>
                    </Col>
                    <Col md={1}>
                      <Button
                        className="badge badge-pill badge-success"
                        type="submit"
                        onClick={e => this.submit(e, 'mobile')}
                      >
                        <i className="fas fa-check" />
                      </Button>
                    </Col>
                  </Row>
                </Form>
              </Col>
            </Row>
          </CardBody>
        </Card>
      </Row>
    );
  }
}
const mapStateToProps = state => ({
  user: state.user.info,
  infoStatus: state.user.infoStatus,
  updateStatus: state.user.updateStatus,
});

export default connect(
  mapStateToProps,
  { getUserData, updateUserData },
)(Settings);
