/* eslint-disable react/prop-types */
import { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withSnackbar } from 'notistack';
import { removeSnackbar } from '../../store/action/NotificationAction';

class Notifier extends Component {
  displayed = [];

  shouldComponentUpdate({ notifications: newSnacks = [] }) {
    const thisProps = this.props;
    if (!newSnacks.length) {
      this.displayed = [];
      return false;
    }

    const { notifications: currentSnacks } = this.props;
    let notExists = false;
    for (let i = 0; i < newSnacks.length; i += 1) {
      const newSnack = newSnacks[i];
      if (newSnack.dismissed) {
        thisProps.closeSnackbar(newSnack.key);
        thisProps.removeSnackbar(newSnack.key);
      }

      // eslint-disable-next-line no-continue
      if (notExists) continue;
      notExists = notExists
        || !currentSnacks.filter(({ key }) => newSnack.key === key).length;
    }
    return notExists;
  }

  componentDidUpdate() {
    const { notifications = [] } = this.props;
    const thisProps = this.props;
    notifications.forEach(({ key, message, options = {} }) => {
      // Do nothing if snackbar is already displayed
      if (this.displayed.includes(key)) return;
      // Display snackbar using notistack
      thisProps.enqueueSnackbar(message, {
        ...options,
        onClose: (event, reason, key1) => {
          if (options.onClose) {
            options.onClose(event, reason, key1);
          }
          // Dispatch action to remove snackbar from redux store
          thisProps.removeSnackbar(key1);
        },
      });
      // Keep track of snackbars that we've displayed
      this.storeDisplayed(key);
    });
  }

  storeDisplayed = (id) => {
    this.displayed = [...this.displayed, id];
  };

  render() {
    return null;
  }
}

const mapStateToProps = store => ({
  notifications: store.notification.notifications,
});

const mapDispatchToProps = dispatch => bindActionCreators({ removeSnackbar }, dispatch);

export default withSnackbar(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Notifier),
);
