import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Dropdown,
  Nav,
  Button,
  NavItem,
} from 'reactstrap';
import Select from '@material-ui/core/Select';

import Cookies from 'universal-cookie';

import { AppNavbarBrand, AppSidebarToggler } from '@coreui/react';

import logo from '../../assets/img/brand/p.svg';
import minlogo from '../../assets/img/brand/minlogo.png';

import userActions from '../../store/action/UserAction';

const { getStatePermissionsData, getSidebarData, getRoutesData } = userActions;

const cookies = new Cookies();
class DefaultHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      logout: false,
      selectedState: '',
      dropdownOpen: false,
      states: [],
    };
  }

  componentDidMount = async () => {
    let { stateList } = this.props;
    stateList = stateList || (await cookies.get('stateList')) || [];
  };

  handleChange = async (state) => {
    cookies.set('state', state, { path: '/', maxAge: 86400 });
    const { token } = this.props;
    const { props } = this;
    await props
      .getStatePermissionsData(state, token || cookies.get('token'))
      .then(async () => {
        const features = cookies.get('features');
        await props.getSidebarData(features);
        await props.getRoutesData(features);
      });
  };

  toggle = () => {
    const oldState = { ...this.state };
    oldState.dropdownOpen = !oldState.dropdownOpen;
    this.setState({ ...oldState });
  };

  logout = () => {
    cookies.remove('token', { path: '/' });
    cookies.remove('state', { path: '/' });
    cookies.remove('stateList', { path: '/' });
    cookies.remove('userBasicDetail', { path: '/' });
    cookies.remove('features', { path: '/' });
    cookies.remove('prevPath', { path: '/' });
    this.setState({ logout: true });
  };

  render() {
    const { state } = this.props;
    const { logout, dropdownOpen, selectedState } = this.state;
    let { stateList } = this.props;
    stateList = cookies.get('stateList') || stateList || [];

    const stateName = state.name
      || (cookies.get('state') && cookies.get('state').name)
      || 'No State Selected';
    if (logout) {
      return <Redirect true from="*" to="/" />;
    }
    const name = cookies.get('userBasicDetail').name || '';
    const mobile = cookies.get('userBasicDetail').mobile || '';

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{
            src: logo,
            width: 150,
            height: 40,
            alt: 'IA Logo',
          }}
          minimized={{
            src: minlogo,
            width: 30,
            height: 30,
            alt: 'IA Logo',
          }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            {stateName === 'No State Selected' ? (
              ''
            ) : (
              <Select
                native
                value={JSON.stringify(cookies.get('state'))}
                onChange={(e) => {
                  const selected_obj = JSON.parse(e.target.value);
                  this.setState({ selectedState: selected_obj.id });
                  this.handleChange(selected_obj);
                }}
              >
                {stateList.map(oneState => (
                  <option value={JSON.stringify(oneState)}>
                    {oneState.name}
                  </option>
                ))}
              </Select>
            )}
          </NavItem>

          {stateName === 'No State Selected' ? (
            ''
          ) : (
            <>
              <NavItem className="px-3">
                <Link to="/admin/dashboard">Dashboard</Link>{' '}
              </NavItem>
              <NavItem className="px-3">
                <Link to="/admin/national">National</Link>{' '}
              </NavItem>
            </>
          )}

          <NavItem className="px-3">
            <div>
              {name}@{mobile}
            </div>
          </NavItem>
        </Nav>

        <Nav className="ml-auto" navbar>
          <NavItem className="px-3">
            {stateList.length === 0 ? (
              ''
            ) : (
              <Link
                to="/admin/profile"
                style={{ color: ' #000000', textDecoration: 'none !important' }}
              >
                Profile
              </Link>
            )}
          </NavItem>
          <NavItem className="d-md-down-none">
            <Button
              data-test="logout_test"
              color="link"
              onClick={() => {
                this.logout();
                // cookies.set('token', '', { path: '/', maxAge: 86400 });
                // cookies.set('state', '', { path: '/', maxAge: 86400 });
                // cookies.set('states', '', { path: '/', maxAge: 86400 });
                document.location.reload();
              }}
              style={{
                fontWeight: '400',
                color: '#000000',
                textDecoration: 'none',
              }}
            >
              Logout
            </Button>
          </NavItem>
        </Nav>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  token: state.auth.token,
  status: state.auth.loginStatus,
  stateList: state.user.states,
  state: state.user.state,
});
export default connect(mapStateToProps, {
  getStatePermissionsData,
  getSidebarData,
  getRoutesData,
})(DefaultHeader);
