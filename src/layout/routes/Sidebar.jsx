import Cookies from 'universal-cookie';

const cookies = new Cookies();

const mandatory = [
  {
    name: 'Dashboard',
    url: '/admin/dashboard',
    icon: 'icon-speedometer',
  },
  {
    name: 'Bulk Upload',
    url: '/admin/bulk_upload',
    icon: 'icon-settings',
  },
];

const sideBar = [
  {
    name: 'Default',
    url: '/admin/default',
    icon: 'icon-speedometer',
  },
  // {
  //   name: 'Policies',
  //   url: '/admin/Policies',
  //   icon: 'icon-briefcase',
  // },
  {
    name: 'Form Management',
    url: '/admin/formManager',
    icon: 'icon-puzzle',
  },
  {
    name: 'States',
    url: '/admin/States',
    icon: 'icon-puzzle',
  },
  {
    name: 'Caller Management',
    url: '/admin/callers',
    icon: 'icon-call-end',
  },
  {
    name: 'User Management',
    url: '/admin/userlist',
    icon: 'icon-user',
  },
  {
    name: 'Task Setting',
    url: '/admin/taskSetting',
    icon: 'icon-settings',
  },
  {
    name: 'Task Assignment',
    url: '/admin/taskAssignment',
    icon: 'icon-settings',
  },
  {
    name: 'Feature Management',
    url: '/admin/userManager/permissionManager',
    icon: 'icon-shield',
  },
  {
    name: 'Report Generator',
    url: '/admin/reportgenerator',
    icon: 'icon-doc',
  },
];

const getSidebar = (features = []) => {
  // const features = cookies.get('features') || [];
  const featureList = [];
  sideBar.forEach((item) => {
    if (features.filter(e => e.name === item.name).length > 0) {
      featureList.push(item);
    }
  });
  if (features.filter(e => e.name === 'Task Management').length > 0) {
    featureList.push(
      ...sideBar.filter(
        e => e.name === 'Task Assignment' || e.name === 'Task Setting',
      ),
    );
  }
  featureList.sort((a, b) => {
    const nameA = a.name.toLowerCase();
    const nameB = b.name.toLowerCase();
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) return 1;
    return 0;
  });

  featureList.splice(0, 0, ...mandatory);
  return { items: cookies.get('state') ? featureList : [] };
};

export default getSidebar;
