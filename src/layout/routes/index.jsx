import routes from './Routes';
import getSidebar from './Sidebar';

export default {
  getSidebar,
  ...routes,
};
