import {
  FEATURE_MANAGEMENT_FEATURES,
  FEATURE_MANAGEMENT_USER_PERMISSIONS,
} from '../action/Types';

export default function (state = initialState.featureManagement, action) {
  switch (action.type) {
    case FEATURE_MANAGEMENT_FEATURES: {
      return {
        ...state,
        features: {
          ...action.payload,
        },
      };
    }
    case FEATURE_MANAGEMENT_USER_PERMISSIONS: {
      return {
        ...state,
        userPermissions: {
          ...action.payload,
        },
      };
    }
    default: {
      return state;
    }
  }
}
