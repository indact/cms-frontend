import {
  USERMANAGEMENT_USERS,
  USERMANAGEMENT_USERS_STATUS,
  USERMANAGEMENT_FEATURES,
  USERMANAGEMENT_FEATURES_STATUS,
  USERMANAGEMENT_PERMISSIONS,
  USERMANAGEMENT_PERMISSIONS_STATUS,
  USERMANAGEMENT_STATES,
  USERMANAGEMENT_STATES_STATUS,
  USERMANAGEMENT_SEARCHSTATE,
  USERMANAGEMENT_SEARCHSTATE_STATUS,
  USERMANAGEMENT_ADDPERMISSION,
  USERMANAGEMENT_ADDPERMISSION_STATUS,
  USERMANAGEMENT_UPDATEPERMISSION,
  USERMANAGEMENT_UPDATEPERMISSION_STATUS,
  USERMANAGEMENT_USER_PERMISSIONS,
} from './Types';
import UserManagementApi from '../../service/UserManagementApi';

export const getAllUsers = state => async (dispatch) => {
  dispatch({ type: USERMANAGEMENT_USERS_STATUS, payload: 'pending' });
  await UserManagementApi.getUserData(state)
    .then((response) => {
      dispatch({ type: USERMANAGEMENT_USERS_STATUS, payload: response.status });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: USERMANAGEMENT_USERS,
        payload,
      });
    });
};
export const getAllStates = () => async (dispatch) => {
  dispatch({ type: USERMANAGEMENT_STATES_STATUS, payload: 'pending' });
  await UserManagementApi.getStateData()
    .then((response) => {
      dispatch({
        type: USERMANAGEMENT_STATES_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: USERMANAGEMENT_STATES,
        payload,
      });
    });
};
export const getAllPermissions = () => async (dispatch) => {
  // dispatch({ type: USERMANAGEMENT_PERMISSIONS_STATUS, payload: 'pending' });
  // await UserManagementApi.getAllPermissionData()
  //   .then((response) => {
  //     dispatch({
  //       type: USERMANAGEMENT_PERMISSIONS_STATUS,
  //       payload: response.status,
  //     });
  //     return response.json();
  //   })
  //   .then((payload) => {
  //     dispatch({
  //       type: USERMANAGEMENT_PERMISSIONS,
  //       payload,
  //     });
  //   });
  dispatch({ type: USERMANAGEMENT_PERMISSIONS_STATUS, payload: 200 });
  dispatch({
    type: USERMANAGEMENT_PERMISSIONS,
    payload: [
      { name: 'Create' },
      { name: 'Update' },
      { name: 'View' },
      { name: 'Delete' },
      { name: 'Download' },
    ],
  });
};
export const getAllFeatures = () => async (dispatch) => {
  dispatch({ type: USERMANAGEMENT_FEATURES_STATUS, payload: 'pending' });
  await UserManagementApi.getFeatureData()
    .then((response) => {
      dispatch({
        type: USERMANAGEMENT_FEATURES_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: USERMANAGEMENT_FEATURES,
        payload,
      });
    });
};
export const searchFromState = query => async (dispatch) => {
  dispatch({ type: USERMANAGEMENT_SEARCHSTATE_STATUS, payload: 'pending' });
  await UserManagementApi.getSearchStateData(query)
    .then((response) => {
      dispatch({
        type: USERMANAGEMENT_SEARCHSTATE_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: USERMANAGEMENT_SEARCHSTATE,
        payload,
      });
      const userPermissions = {};
      if (payload && payload.type !== 'ValidationException') {
        payload.data.map((user) => {
          user.states[0].features.forEach((feature) => {
            const featureName = feature.FeaturesName;
            userPermissions[featureName] = userPermissions[featureName] || [];
            userPermissions[featureName].push({
              userId: user.id,
              userName: user.name,
              featureId: feature.FeaturesID,
              featureName,
              permissions: feature.permissions,
            });
          });
          return 0;
        });
      }
      dispatch({
        type: USERMANAGEMENT_USER_PERMISSIONS,
        payload: userPermissions,
      });
    });
};
export const addNewPermissionFun = data => async (dispatch) => {
  dispatch({ type: USERMANAGEMENT_ADDPERMISSION_STATUS, payload: 'pending' });
  await UserManagementApi.postNewPermission(data)
    .then((response) => {
      dispatch({
        type: USERMANAGEMENT_ADDPERMISSION_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: USERMANAGEMENT_ADDPERMISSION,
        payload,
      });
    });
};
export const updatePermissionFun = data => async (dispatch) => {
  dispatch({
    type: USERMANAGEMENT_UPDATEPERMISSION_STATUS,
    payload: 'pending',
  });
  await UserManagementApi.patchUpdatePermission(data)
    .then((response) => {
      dispatch({
        type: USERMANAGEMENT_UPDATEPERMISSION_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: USERMANAGEMENT_UPDATEPERMISSION,
        payload,
      });
    });
};
