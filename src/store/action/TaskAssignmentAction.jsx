import Cookies from 'universal-cookie';
import {
  TASKASSIGNMENT_ALLTASK,
  TASKASSIGNMENT_ALLTASK_STATUS,
  TASKASSIGNMENT_ADDTASK,
  TASKASSIGNMENT_ADDTASK_STATUS,
  TASKASSIGNMENT_SINGLEFORM,
  TASKASSIGNMENT_SINGLEFORM_STATUS,
  TASKASSIGNMENT_SINGLESUBMISSION,
  TASKASSIGNMENT_SINGLESUBMISSION_STATUS,
  TASKASSIGNMENT_PSINGLESUBMISSION,
  TASKASSIGNMENT_PSINGLESUBMISSION_STATUS,
  TASKASSIGNMENT_FORMDETS,
  TASKASSIGNMENT_FORMDETS_STATUS,
  TASKASSIGNMENT_UPDATETASK,
  TASKASSIGNMENT_UPDATETASK_STATUS,
  TASKASSIGNMENT_BULKASSIGN,
  TASKASSIGNMENT_BULKASSIGN_STATUS,
  TASKASSIGNMENT_TASKSEARCH_STATUS,
  TASKASSIGNMENT_TASKSEARCH,
  TASKASSIGNMENT_ALLTEMPLATE_STATUS,
  TASKASSIGNMENT_ALLTEMPLATE,
  TASKASSIGNMENT_SENDTEMPLATE_STATUS,
  TASKASSIGNMENT_SENDTEMPLATE,
  TASKASSIGNMENT_PERSISTENTFORM_STATUS,
  TASKASSIGNMENT_PERSISTENTFORM,
  TASKASSIGNMENT_STATE,
} from './Types';
import TaskAssignmentApi from '../../service/TaskAssignmentApi';

const cookies = new Cookies();
const stateID = cookies.get('state').id;

export const getAllTasksAssigned = (data, state) => async (dispatch) => {
  dispatch({ type: TASKASSIGNMENT_ALLTASK_STATUS, payload: 'pending' });
  dispatch({ type: TASKASSIGNMENT_ALLTASK, payload: [] });
  dispatch({
    type: TASKASSIGNMENT_STATE,
    payload: state,
  });
  await TaskAssignmentApi.getAllTasksAssigned(data, state)
    .then((response) => {
      dispatch({
        type: TASKASSIGNMENT_ALLTASK_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKASSIGNMENT_ALLTASK,
        payload,
      });
    });
};

export const searchTasksAssigned = data => async (dispatch) => {
  dispatch({ type: TASKASSIGNMENT_TASKSEARCH_STATUS, payload: 'pending' });
  dispatch({ type: TASKASSIGNMENT_TASKSEARCH, payload: [] });
  await TaskAssignmentApi.getAllTasksAssigned(data)
    .then((response) => {
      dispatch({
        type: TASKASSIGNMENT_TASKSEARCH_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKASSIGNMENT_TASKSEARCH,
        payload,
      });
    });
};

export const addNewTask = data => async (dispatch) => {
  // eslint-disable-next-line no-param-reassign
  data.state = stateID;
  dispatch({ type: TASKASSIGNMENT_ADDTASK_STATUS, payload: 'pending' });
  dispatch({ type: TASKASSIGNMENT_ADDTASK, payload: [] });
  await TaskAssignmentApi.PostAddTasksAssigned(data)
    .then((response) => {
      dispatch({
        type: TASKASSIGNMENT_ADDTASK_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKASSIGNMENT_ADDTASK,
        payload,
      });
    });
};

export const BulkAssign = data => async (dispatch) => {
  dispatch({ type: TASKASSIGNMENT_BULKASSIGN_STATUS, payload: 'pending' });
  dispatch({ type: TASKASSIGNMENT_BULKASSIGN, payload: [] });
  await TaskAssignmentApi.PatchBulkAssign(data)
    .then((response) => {
      dispatch({
        type: TASKASSIGNMENT_BULKASSIGN_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKASSIGNMENT_BULKASSIGN,
        payload,
      });
    });
};

export const getSingleForm = data => async (dispatch) => {
  dispatch({ type: TASKASSIGNMENT_SINGLEFORM_STATUS, payload: 'pending' });
  dispatch({ type: TASKASSIGNMENT_SINGLEFORM, payload: [] });
  await TaskAssignmentApi.GetSingleForm(data)
    .then((response) => {
      dispatch({
        type: TASKASSIGNMENT_SINGLEFORM_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKASSIGNMENT_SINGLEFORM,
        payload,
      });
    });
};

export const getPersistentSingleForm = data => async (dispatch) => {
  dispatch({ type: TASKASSIGNMENT_PERSISTENTFORM_STATUS, payload: 'pending' });
  dispatch({ type: TASKASSIGNMENT_PERSISTENTFORM, payload: [] });
  await TaskAssignmentApi.GetSingleForm(data)
    .then((response) => {
      dispatch({
        type: TASKASSIGNMENT_PERSISTENTFORM_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKASSIGNMENT_PERSISTENTFORM,
        payload,
      });
    });
};

export const getSingleFormSubmission = (formname, id) => async (dispatch) => {
  dispatch({
    type: TASKASSIGNMENT_SINGLESUBMISSION_STATUS,
    payload: 'pending',
  });
  dispatch({
    type: TASKASSIGNMENT_SINGLESUBMISSION,
    payload: [],
  });
  await TaskAssignmentApi.GetSingleFormSubmission(formname, id)
    .then((response) => {
      dispatch({
        type: TASKASSIGNMENT_SINGLESUBMISSION_STATUS,
        payload: response.status,
      });
      dispatch({
        type: TASKASSIGNMENT_SINGLESUBMISSION,
        payload: [],
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKASSIGNMENT_SINGLESUBMISSION,
        payload,
      });
    });
};

export const getPersistentSingleFormSubmission = (
  formname,
  id,
) => async (dispatch) => {
  dispatch({
    type: TASKASSIGNMENT_PSINGLESUBMISSION_STATUS,
    payload: 'pending',
  });
  dispatch({
    type: TASKASSIGNMENT_PSINGLESUBMISSION,
    payload: [],
  });
  await TaskAssignmentApi.GetSingleFormSubmission(formname, id)
    .then((response) => {
      dispatch({
        type: TASKASSIGNMENT_PSINGLESUBMISSION_STATUS,
        payload: response.status,
      });
      dispatch({
        type: TASKASSIGNMENT_PSINGLESUBMISSION,
        payload: [],
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKASSIGNMENT_PSINGLESUBMISSION,
        payload,
      });
    });
};

export const GetFormByTaskType = data => async (dispatch) => {
  dispatch({
    type: TASKASSIGNMENT_FORMDETS_STATUS,
    payload: 'pending',
  });
  dispatch({
    type: TASKASSIGNMENT_FORMDETS,
    payload: [],
  });
  await TaskAssignmentApi.GetFormByTaskType(data)
    .then((response) => {
      dispatch({
        type: TASKASSIGNMENT_FORMDETS_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKASSIGNMENT_FORMDETS,
        payload,
      });
    });
};

export const UpdateTask = data => async (dispatch) => {
  dispatch({
    type: TASKASSIGNMENT_UPDATETASK_STATUS,
    payload: 'pending',
  });
  dispatch({
    type: TASKASSIGNMENT_UPDATETASK,
    payload: [],
  });
  await TaskAssignmentApi.PatchUpdateTasksAssigned(data)
    .then((response) => {
      dispatch({
        type: TASKASSIGNMENT_UPDATETASK_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKASSIGNMENT_UPDATETASK,
        payload,
      });
    });
};

export const getAllTemplates = data => async (dispatch) => {
  dispatch({
    type: TASKASSIGNMENT_ALLTEMPLATE_STATUS,
    payload: 'pending',
  });
  dispatch({
    type: TASKASSIGNMENT_ALLTEMPLATE,
    payload: [],
  });
  await TaskAssignmentApi.getAllTemplates(data)
    .then((response) => {
      dispatch({
        type: TASKASSIGNMENT_ALLTEMPLATE_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKASSIGNMENT_ALLTEMPLATE,
        payload,
      });
    });
};

export const sendMessage = data => async (dispatch) => {
  dispatch({
    type: TASKASSIGNMENT_SENDTEMPLATE_STATUS,
    payload: 'pending',
  });
  dispatch({
    type: TASKASSIGNMENT_SENDTEMPLATE,
    payload: [],
  });
  await TaskAssignmentApi.postSendMessage(data)
    .then((response) => {
      dispatch({
        type: TASKASSIGNMENT_SENDTEMPLATE_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKASSIGNMENT_SENDTEMPLATE,
        payload,
      });
    });
};
