import {
  CALLERMANAGEMENT_CALLERS,
  CALLERMANAGEMENT_CALLERS_STATUS,
  CALLERMANAGEMENT_USERSTATE,
  CALLERMANAGEMENT_USERSTATE_STATUS,
  CALLERMANAGEMENT_USERPROFILE,
  CALLERMANAGEMENT_USERPROFILE_STATUS,
} from './Types';
import CallerManagementApi from '../../service/CallerManagementApi';

export const getAllCallers = (token, featureid, state) => async (dispatch) => {
  dispatch({ type: CALLERMANAGEMENT_CALLERS_STATUS, payload: 'pending' });
  await CallerManagementApi.getCallersData(token, featureid, state)
    .then((response) => {
      dispatch({
        type: CALLERMANAGEMENT_CALLERS_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      const data = payload.data ? payload.data : [];
      data.forEach((v, i) => {
        // eslint-disable-next-line no-param-reassign
        delete payload.data[i].features;
      });
      dispatch({
        type: CALLERMANAGEMENT_CALLERS,
        payload,
      });
    });
};
export const getAllCallersData = (token, featureid) => async (dispatch) => {
  dispatch({ type: CALLERMANAGEMENT_CALLERS_STATUS, payload: 'pending' });
  await CallerManagementApi.getAllCallersData(token, featureid)
    .then((response) => {
      dispatch({
        type: CALLERMANAGEMENT_CALLERS_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      const data = payload.data ? payload.data : [];
      data.forEach((v, i) => {
        // eslint-disable-next-line no-param-reassign
        delete payload.data[i].features;
      });
      dispatch({
        type: CALLERMANAGEMENT_CALLERS,
        payload,
      });
    });
};

export const getAllCallersPage = (token, params) => async (dispatch) => {
  dispatch({ type: CALLERMANAGEMENT_CALLERS_STATUS, payload: 'pending' });
  await CallerManagementApi.getCallersData(token, params)
    .then((response) => {
      dispatch({
        type: CALLERMANAGEMENT_CALLERS_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      // eslint-disable-next-line no-param-reassign
      payload.type = 'page';
      payload.data.forEach((v, i) => {
        // eslint-disable-next-line no-param-reassign
        delete payload.data[i].features;
      });
      dispatch({
        type: CALLERMANAGEMENT_CALLERS,
        payload,
      });
    });
};

export const changeUserState = (token, data) => async (dispatch) => {
  dispatch({ type: CALLERMANAGEMENT_USERSTATE_STATUS, payload: 'pending' });
  await CallerManagementApi.postChangeUserState(token, data)
    .then((response) => {
      dispatch({
        type: CALLERMANAGEMENT_USERSTATE_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: CALLERMANAGEMENT_USERSTATE,
        payload,
      });
    });
};
export const getUserProfileData = (token, data) => async (dispatch) => {
  dispatch({ type: CALLERMANAGEMENT_USERPROFILE_STATUS, payload: 'pending' });
  await CallerManagementApi.postGetUserProfile(token, data)
    .then((response) => {
      dispatch({
        type: CALLERMANAGEMENT_USERPROFILE_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: CALLERMANAGEMENT_USERPROFILE,
        payload,
      });
    });
};
