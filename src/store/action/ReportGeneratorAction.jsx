import Cookies from 'universal-cookie';
import { REPORT_GENERATOR_STATUS, REPORT_GENERATOR_MESSAGE } from './Types';

const baseUrl = `${process.env.REACT_APP_BASE_URL}:${process.env.REACT_APP_CMS_PORT}`;

const cookies = new Cookies();

// Report by Task Status
export const prepareTaskReportByStatus = data => async (dispatch) => {
  dispatch({ type: REPORT_GENERATOR_STATUS, payload: 'pending' });
  await fetch(`${baseUrl}/reports/beneficiarydatabystatus`, {
    method: 'POST',
    headers: new Headers({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      authorization_token: cookies.get('token'),
    }),
    body: JSON.stringify(data),
  })
    .then((response) => {
      dispatch({ type: REPORT_GENERATOR_STATUS, payload: response.status });
      return response.json();
    })
    .then((payload) => {
      dispatch({ type: REPORT_GENERATOR_MESSAGE, payload });
      return payload;
    });
};

// Report be DG Category
export const prepareTaskReportByDGCategory = data => async (dispatch) => {
  dispatch({ type: REPORT_GENERATOR_STATUS, payload: 'pending' });
  await fetch(`${baseUrl}/reports/beneficiarydatabycategory`, {
    method: 'POST',
    headers: new Headers({
      Accept: 'application/json',
      'Content-Type': 'application/json',
      authorization_token: cookies.get('token'),
    }),
    body: JSON.stringify(data),
  })
    .then((response) => {
      dispatch({ type: REPORT_GENERATOR_STATUS, payload: response.status });
      return response.json();
    })
    .then((payload) => {
      dispatch({ type: REPORT_GENERATOR_MESSAGE, payload });
      return payload;
    });
};
