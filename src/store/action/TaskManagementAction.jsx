import Cookies from 'universal-cookie';
import {
  TASKMANAGEMENT_ADDTYPE,
  TASKMANAGEMENT_ADDTYPE_STATUS,
  TASKMANAGEMENT_ALLTYPE,
  TASKMANAGEMENT_ALLTYPE_STATUS,
  TASKMANAGEMENT_ADDSTATUS,
  TASKMANAGEMENT_ADDSTATUS_STATUS,
  TASKMANAGEMENT_ALLSTATUS,
  TASKMANAGEMENT_ALLSTATUS_STATUS,
  TASKMANAGEMENT_ALLCYCLE,
  TASKMANAGEMENT_ALLCYCLE_STATUS,
  TASKMANAGEMENT_ADDMESSAGE,
  TASKMANAGEMENT_ADDMESSAGE_STATUS,
  TASKMANAGEMENT_ALLMESSAGE,
  TASKMANAGEMENT_ALLMESSAGE_STATUS,
  TASKMANAGEMENT_ALLCALLERS,
  TASKMANAGEMENT_ALLCALLERS_STATUS,
} from './Types';
import TaskManagementApi from '../../service/TaskManagementApi';
import CallerManagementApi from '../../service/CallerManagementApi';

const cookies = new Cookies();
const stateId = cookies.get('state').id;
export const addNewTaskTypeFun = params => async (dispatch) => {
  // console.log('params');

  // console.log(params);

  dispatch({ type: TASKMANAGEMENT_ADDTYPE_STATUS, payload: 'pending' });
  dispatch({ type: TASKMANAGEMENT_ADDTYPE, payload: [] });
  await TaskManagementApi.postAddTaskType(params)
    .then((response) => {
      dispatch({
        type: TASKMANAGEMENT_ADDTYPE_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKMANAGEMENT_ADDTYPE,
        payload,
      });
    });
};

export const getAllTasksType = state => async (dispatch) => {
  dispatch({ type: TASKMANAGEMENT_ALLTYPE_STATUS, payload: 'pending' });
  dispatch({ type: TASKMANAGEMENT_ALLTYPE, payload: [] });
  await TaskManagementApi.getAllTasksType(state)
    .then((response) => {
      // console.log(response);

      dispatch({
        type: TASKMANAGEMENT_ALLTYPE_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKMANAGEMENT_ALLTYPE,
        payload,
      });
    });
};

export const updateTaskType = (id, data) => async (dispatch) => {
  // console.log('paramsjjj');

  // console.log({id, data});
  dispatch({ type: TASKMANAGEMENT_ADDTYPE_STATUS, payload: 'pending' });
  dispatch({ type: TASKMANAGEMENT_ADDTYPE, payload: [] });
  await TaskManagementApi.updateTaskTypes(id, data)
    .then((response) => {
      dispatch({
        type: TASKMANAGEMENT_ADDTYPE_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKMANAGEMENT_ADDTYPE,
        payload,
      });
    });
};

export const deleteTaskType = id => async (dispatch) => {
  const data = {
    id: [id],
    deleted: 'true',
  };
  // console.log(data);
  dispatch({ type: TASKMANAGEMENT_ADDTYPE_STATUS, payload: 'pending' });
  dispatch({ type: TASKMANAGEMENT_ADDTYPE, payload: [] });
  await TaskManagementApi.deleteTaskTypes(data)
    .then((response) => {
      dispatch({
        type: TASKMANAGEMENT_ADDTYPE_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKMANAGEMENT_ADDTYPE,
        payload,
      });
    });
};

// status

export const addNewTaskStatusFun = params => async (dispatch) => {
  dispatch({ type: TASKMANAGEMENT_ADDSTATUS_STATUS, payload: 'pending' });
  dispatch({ type: TASKMANAGEMENT_ADDSTATUS, payload: [] });
  await TaskManagementApi.postAddTaskStatus(params)
    .then((response) => {
      dispatch({
        type: TASKMANAGEMENT_ADDSTATUS_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKMANAGEMENT_ADDSTATUS,
        payload,
      });
    });
};

export const getAllTaskStatus = state => async (dispatch) => {
  dispatch({ type: TASKMANAGEMENT_ALLSTATUS_STATUS, payload: 'pending' });
  dispatch({ type: TASKMANAGEMENT_ALLSTATUS, payload: [] });
  await TaskManagementApi.getAllTaskStatus(state)
    .then((response) => {
      dispatch({
        type: TASKMANAGEMENT_ALLSTATUS_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKMANAGEMENT_ALLSTATUS,
        payload,
      });
    });
};

export const updateTaskStatus = data => async (dispatch) => {
  // console.log('stst');
  // console.log({id, data});

  dispatch({ type: TASKMANAGEMENT_ADDSTATUS_STATUS, payload: 'pending' });
  dispatch({ type: TASKMANAGEMENT_ADDSTATUS, payload: [] });
  await TaskManagementApi.updateTaskStatus(data)
    .then((response) => {
      console.log(response);
      dispatch({
        type: TASKMANAGEMENT_ADDSTATUS_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKMANAGEMENT_ADDSTATUS,
        payload,
      });
    });
};

export const deleteTaskStatus = id => async (dispatch) => {
  dispatch({ type: TASKMANAGEMENT_ADDSTATUS_STATUS, payload: 'pending' });
  dispatch({ type: TASKMANAGEMENT_ADDSTATUS, payload: [] });
  const data = {
    id: [id],
    deleted: 'true',
  };
  await TaskManagementApi.deleteTaskStatus(data)
    .then((response) => {
      dispatch({
        type: TASKMANAGEMENT_ADDSTATUS_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKMANAGEMENT_ADDSTATUS,
        payload,
      });
    });
};

export const getAllCycle = () => async (dispatch) => {
  dispatch({ type: TASKMANAGEMENT_ALLCYCLE_STATUS, payload: 'pending' });
  dispatch({ type: TASKMANAGEMENT_ALLCYCLE, payload: [] });
  await TaskManagementApi.getAllCycle()
    .then((response) => {
      // console.log(response);

      dispatch({
        type: TASKMANAGEMENT_ALLCYCLE_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKMANAGEMENT_ALLCYCLE,
        payload,
      });
    });
};

// massage

export const createMessage = params => async (dispatch) => {
  dispatch({ type: TASKMANAGEMENT_ADDMESSAGE_STATUS, payload: 'pending' });
  dispatch({ type: TASKMANAGEMENT_ADDMESSAGE, payload: [] });
  await TaskManagementApi.postAddMessage(params)
    .then((response) => {
      dispatch({
        type: TASKMANAGEMENT_ADDMESSAGE_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKMANAGEMENT_ADDMESSAGE,
        payload,
      });
    });
};

export const getAllMessage = state => async (dispatch) => {
  dispatch({ type: TASKMANAGEMENT_ALLMESSAGE_STATUS, payload: 'pending' });
  dispatch({ type: TASKMANAGEMENT_ALLMESSAGE, payload: [] });
  await TaskManagementApi.getAllMessage(state)
    .then((response) => {
      dispatch({
        type: TASKMANAGEMENT_ALLMESSAGE_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKMANAGEMENT_ALLMESSAGE,
        payload,
      });
    });
};

export const updateMessage = (id, data) => async (dispatch) => {
  dispatch({ type: TASKMANAGEMENT_ADDMESSAGE_STATUS, payload: 'pending' });
  dispatch({ type: TASKMANAGEMENT_ADDMESSAGE, payload: [] });
  await TaskManagementApi.updateMessage(id, data)
    .then((response) => {
      dispatch({
        type: TASKMANAGEMENT_ADDMESSAGE_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKMANAGEMENT_ADDMESSAGE,
        payload,
      });
    });
};

export const deleteMessage = id => async (dispatch) => {
  dispatch({ type: TASKMANAGEMENT_ADDMESSAGE_STATUS, payload: 'pending' });
  dispatch({ type: TASKMANAGEMENT_ADDMESSAGE, payload: [] });
  const data = {
    id: [id],
    deleted: 'true',
  };
  await TaskManagementApi.deleteMessage(data)
    .then((response) => {
      dispatch({
        type: TASKMANAGEMENT_ADDMESSAGE_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: TASKMANAGEMENT_ADDMESSAGE,
        payload,
      });
    });
};

export const getAllCallersData = (
  token,
  params,
  state = stateId,
) => async (dispatch) => {
  dispatch({ type: TASKMANAGEMENT_ALLCALLERS_STATUS, payload: 'pending' });
  dispatch({ type: TASKMANAGEMENT_ALLCALLERS, payload: [] });
  await CallerManagementApi.getCallersData(token, params, state)
    .then((response) => {
      dispatch({
        type: TASKMANAGEMENT_ALLCALLERS_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      const data = payload.data ? payload.data : [];
      data.forEach((v, i) => {
        // eslint-disable-next-line no-param-reassign
        delete payload.data[i].features;
      });
      dispatch({
        type: TASKMANAGEMENT_ALLCALLERS,
        payload,
      });
    });
};
