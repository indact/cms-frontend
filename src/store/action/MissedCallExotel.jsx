import Cookies from 'universal-cookie';
import {
  EXOTEL_MISSEDCALL_STATUS,
  EXOTEL_MISSEDCALL,
  EXOTEL_NATIONAL_STATUS,
  EXOTEL_MESSAGE,
} from './Types';
import MissedCallExotel from '../../service/MissedCallExotel';

const cookies = new Cookies();
// const stateID = cookies.get('state').StateID;

export const getMissedCallCount = (param = {}) => async (dispatch) => {
  const data = {
    state_id: cookies.get('state').id,
    from: param.from,
    to: param.to,
  };
  dispatch({ type: EXOTEL_MISSEDCALL_STATUS, payload: 'pending' });
  dispatch({ type: EXOTEL_MISSEDCALL, payload: [] });
  await MissedCallExotel.ExotelMissedCallCountApi(data)
    .then((response) => {
      dispatch({
        type: EXOTEL_MISSEDCALL_STATUS,
        payload: response.status,
      });

      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: EXOTEL_MISSEDCALL,
        payload,
      });
    });
};

export const getNationalMissedCallCount = (param = {}) => async (dispatch) => {
  const data = {
    from: param.from,
    to: param.to,
  };

  dispatch({ type: EXOTEL_NATIONAL_STATUS, payload: 'pending' });
  dispatch({ type: EXOTEL_MESSAGE, payload: [] });
  await MissedCallExotel.ExotelMissedCallCountApi(data)
    .then((response) => {
      dispatch({
        type: EXOTEL_NATIONAL_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((payload) => {
      dispatch({
        type: EXOTEL_MESSAGE,
        payload,
      });
    });
};
