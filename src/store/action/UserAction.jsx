import Cookies from 'universal-cookie';
import {
  USER_INFO_UPDATE,
  USER_INFO_STATUS,
  USER_INFO_STATE_LIST,
  USER_INFO_STATE_FEATURES,
  USER_INFO_STATE_SIDEBAR,
  USER_INFO_UPDATE_STATUS,
  USER_INFO,
  USER_INFO_STATE,
  USER_INFO_STATE_ROUTES,
} from './Types';
import {
  userData,
  updateInfo,
  getStateList,
  getStatePermission,
} from '../../service/UserApi';

import routes from '../../layout/routes/index';

const { getSidebar, otherRoutes, featureRoutes } = routes;

const cookies = new Cookies();
const token = cookies.get('token');

const getUserData = id => async (dispatch) => {
  await userData(id, await cookies.get('token'))
    .then((Response) => {
      dispatch({ type: USER_INFO_STATUS, payload: Response.status });
      return Response.json();
    })
    // eslint-disable-next-line consistent-return
    .then((payload) => {
      if (payload.err) {
        return cookies.remove('token', { path: '/' });
      }
      dispatch({ type: USER_INFO, payload: payload.data });
    });
};
const updateUserData = data => async (dispatch) => {
  await updateInfo(token, data)
    .then((Response) => {
      dispatch({ type: USER_INFO_UPDATE_STATUS, payload: Response.status });
      dispatch({ type: USER_INFO_STATUS, payload: 404 });
      return Response.json();
    })
    .then((payload) => {
      dispatch({ type: USER_INFO_UPDATE, payload });
    });
};
const getStatesData = (defaultToken = token) => async (dispatch) => {
  if (cookies.get('stateList')) {
    dispatch({ type: USER_INFO_STATE_LIST, payload: cookies.get('stateList') });
  } else {
    await getStateList(defaultToken)
      .then(response => response.json())
      .then((data) => {
        const {
          data: { stateList },
        } = data;
        dispatch({ type: USER_INFO_STATE_LIST, payload: stateList });
        cookies.set('stateList', stateList, { path: '/', maxAge: 86400 });
      })
      .catch(e => console.log(e));
  }
};
const getStateData = () => (dispatch) => {
  if (cookies.get('state')) {
    dispatch({ type: USER_INFO_STATE, payload: cookies.get('state') });
  }
};
const getStatePermissionsData = (
  state,
  defaultToken = token,
) => async (dispatch) => {
  dispatch({ type: USER_INFO_STATE, payload: state });
  await getStatePermission(state.id, defaultToken)
    .then(response => response.json())
    .then(({ data: { features } }) => {
      dispatch({ type: USER_INFO_STATE_FEATURES, payload: features });
      cookies.set('features', features, { path: '/', maxAge: 86400 });
      return features;
    });
};
const getSidebarData = features => async (dispatch) => {
  const sideBar = getSidebar(features);
  dispatch({ type: USER_INFO_STATE_SIDEBAR, payload: sideBar });
};
const getRoutesData = features => async (dispatch) => {
  const stateRoutes = [];
  stateRoutes.push(...otherRoutes);

  features.forEach((feature) => {
    for (const key in featureRoutes) {
      if ({}.hasOwnProperty.call(featureRoutes, key)) {
        const value = featureRoutes[key];
        if (feature.name === key) value.map(val => stateRoutes.push(val));
      }
    }
  });

  dispatch({ type: USER_INFO_STATE_ROUTES, payload: stateRoutes });
};
const changeState = state => async (dispatch) => {
  await cookies.set('state', state, { path: '/', maxAge: 86400 });
  dispatch({ type: USER_INFO_STATE, payload: state });
};

export default {
  getUserData,
  updateUserData,
  getStatesData,
  getStateData,
  getStatePermissionsData,
  getSidebarData,
  getRoutesData,
  changeState,
};
