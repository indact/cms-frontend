import {
  FEATURE_MANAGEMENT_FEATURES,
  FEATURE_MANAGEMENT_USER_PERMISSIONS,
} from './Types';

import userManagement from '../../service/UserManagementApi';

const { getFeatureData } = userManagement;

export const getFeatures = () => async (dispatch) => {
  await getFeatureData()
    .then(response => response.json())
    .then((payload) => {
      console.log(payload);
      dispatch({
        type: FEATURE_MANAGEMENT_FEATURES,
        payload,
      });
    });
};

export const getUserPermissions = () => async (dispatch) => {};
