/* eslint-disable no-prototype-builtins */
import Cookies from 'universal-cookie';
import {
  FORMIO_FORM,
  FORMIO_MESSAGE,
  FORMIO_MESSAGE_TYPE,
  // FORMIO_FORM_LIST,
  FORMIO_SUBMISSION_LIST,
  FORMIO_SUBMISSION,
  FORMIO_ADDL_FORM_DATA,
  FORMIO_ADDL_FORM_DATA_LIST,
  FORMIO_NEWSUBMISSION_STATUS,
  FORMIO_NEWSUBMISSION,
  FORMIO_SYNCAPI_STATUS,
  FORMIO_SYNCAPI,
  FORMIO_FORM_CREATE_STSTUS,
  FORMIO_FORM_CREATE,
  FORMIO_FORM_LIST_STATUS,
  FORMIO_FORM_LIST,
  FORMIO_DATADOWN_STATUS,
  FORMIO_DATADOWN,
  FORMIO_PERSISTENCE_EXISTENCE,
} from './Types';
import FormManagement from '../../service/FormManagement';

const cookies = new Cookies();
const token = cookies.get('token');
const baseUrl = `${process.env.REACT_APP_BASE_URL}:${process.env.REACT_APP_CMS_PORT}`;
const stateName = cookies.get('state') && cookies.get('state').name;
// FORM CRUD ACTIONS
export const saveForm = form => async (dispatch) => {
  const data = {
    state: stateName,
    title: form.title,
    type: form.type,
    schema: { components: form.components },
  };
  dispatch({ type: FORMIO_FORM_CREATE_STSTUS, payload: 'pending' });
  await FormManagement.saveForm(data)
    .then((response) => {
      dispatch({ type: FORMIO_FORM_CREATE_STSTUS, payload: response.status });
      return response.json();
    })
    .then((payload) => {
      dispatch({ type: FORMIO_FORM_CREATE, payload });
      return payload;
    });
};

export const getFormList = state => async (dispatch) => {
  dispatch({ type: FORMIO_FORM_LIST_STATUS, payload: 'pending' });
  await FormManagement.getFormListApi(state)
    .then((response) => {
      dispatch({ type: FORMIO_FORM_LIST_STATUS, payload: response.status });
      return response.json();
    })
    .then((payload) => {
      dispatch({ type: FORMIO_FORM_LIST, payload });
      return payload;
    });
};

export const getFilteredFormList = ({
  title,
  owner,
  page,
}) => async (dispatch) => {
  let url = `${baseUrl}/forms?state=${stateName}&limit=1000`;
  let query = '';
  if (title) {
    query += `&title=${title}`;
  }
  if (owner) {
    query += `&owner=${owner}`;
  }
  url += query;

  const list = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      authorization_token: token,
    },
  })
    .then(response => response.json())
    .then((formList) => {
      const payload = formList.data;
      dispatch({ type: FORMIO_ADDL_FORM_DATA_LIST, payload });
      return formList.data;
    });
  return dispatch({ type: FORMIO_FORM_LIST, payload: list });
};

export const updateForm = form => async (dispatch) => {
  const data = {
    state: stateName,
    title: form.title,
    type: form.type,
    schema: { components: form.components },
  };
  dispatch({ type: FORMIO_FORM_CREATE_STSTUS, payload: 'pending' });
  await fetch(`${baseUrl}/forms/${form._id}`, {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json',
      authorization_token: token,
    },
    body: JSON.stringify(data),
  })
    .then((response) => {
      dispatch({ type: FORMIO_FORM_CREATE_STSTUS, payload: response.status });
      return response.json();
    })
    .then((payload) => {
      dispatch({ type: FORMIO_FORM_CREATE, payload });
      return payload;
    });
};

// export const deleteForm = form => async dispatch => {
//   await fetch(`${baseUrl}/form/${form._id}`, {
//     method: "DELETE",
//     headers: {
//       "Content-Type": "application/json",
//       authorization_token: token
//     },
//     body: JSON.stringify(form)
//   });
//   dispatch({ type: FORMIO_FORM, payload: null });
// };

// SUBMISSION CRUD ACTIONS
export const submitData = (form, { data }) => async (dispatch) => {
  await FormManagement.submitData(form, { data })
    .then((response) => {
      dispatch({ type: FORMIO_NEWSUBMISSION_STATUS, payload: response.status });
      return response.json();
    })
    .then((payload) => {
      dispatch({ type: FORMIO_NEWSUBMISSION, payload });
    });
};

export const anonymousSubmission = (form, { data }) => async (dispatch) => {
  await FormManagement.anonymousSubmission(form, { data })
    .then((response) => {
      dispatch({ type: FORMIO_NEWSUBMISSION_STATUS, payload: response.status });
      return response.json();
    })
    .then((payload) => {
      dispatch({ type: FORMIO_NEWSUBMISSION, payload });
    });
};

export const getSubmissionData = (form, page = '1') => async (dispatch) => {
  const url = `${baseUrl}/forms/${form._id}/submissions?limit=1000&skip=
  ${(parseInt(page, 10) - 1) * 15}`;
  await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      authorization_token: token,
    },
  })
    .then(response => response.json())
    .then((data) => {
      // console.log('data');
      // console.log(data);
      dispatch({ type: FORMIO_SUBMISSION_LIST, payload: data.data });
    });
};

export const updateSubmission = ({ _id, form, data }) => async (dispatch) => {
  // console.log('hi');

  // console.log({ _id, form, data });

  await fetch(`${baseUrl}/forms/${form}/submissions/${_id}`, {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json',
      authorization_token: token,
    },
    body: JSON.stringify(data),
  });
  dispatch({ type: FORMIO_SUBMISSION, payload: null });
};

// export const deleteSubmission = ({ _id, form }) => async dispatch => {
//   await fetch(`${baseUrl}/form/${form}/submission/${_id}`, {
//     method: "DELETE",
//     headers: {
//       "Content-Type": "application/json",
//       authorization_token: token
//     }
//   });
//   dispatch({ type: FORMIO_SUBMISSION, payload: null });
// };
// MISC ACTIONS

export const setForm = form => (dispatch) => {
  dispatch({ type: FORMIO_FORM, payload: form });
};

export const resetForm = () => (dispatch) => {
  dispatch({ type: FORMIO_FORM, payload: {} });
  dispatch({ type: FORMIO_MESSAGE, payload: '' });
  dispatch({ type: FORMIO_MESSAGE_TYPE, payload: 'danger' });
};

export const setAddlFormData = addlFormData => (dispatch) => {
  dispatch({ type: FORMIO_ADDL_FORM_DATA, payload: addlFormData });
};

export const resetAddlFormData = () => (dispatch) => {
  dispatch({ type: FORMIO_ADDL_FORM_DATA, payload: {} });
};

export const setSubmission = submission => (dispatch) => {
  dispatch({ type: FORMIO_SUBMISSION, payload: submission });
};

export const resetSubmission = () => (dispatch) => {
  dispatch({ type: FORMIO_SUBMISSION, payload: null });
};

export const resetMessage = () => (dispatch) => {
  dispatch({ type: FORMIO_MESSAGE, payload: '' });
  dispatch({ type: FORMIO_MESSAGE_TYPE, payload: 'danger' });
};

export const formSubmitSync = data => async (dispatch) => {
  dispatch({ type: FORMIO_SYNCAPI_STATUS, payload: 'pending' });
  dispatch({ type: FORMIO_SYNCAPI, payload: [] });

  await fetch(`${baseUrl}/tasks/sync`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      authorization_token: token,
    },
    body: JSON.stringify(data),
  })
    .then((response) => {
      dispatch({ type: FORMIO_SYNCAPI_STATUS, payload: response.status });
      return response.json();
    })
    .then((payload) => {
      dispatch({ type: FORMIO_SYNCAPI, payload });
    });
};
export const downloadFormData = data => async (dispatch) => {
  dispatch({ type: FORMIO_DATADOWN_STATUS, payload: 'pending' });
  dispatch({ type: FORMIO_DATADOWN, payload: [] });

  await fetch(`${baseUrl}/tasks/download/form-report`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      authorization_token: token,
    },
    body: JSON.stringify(data),
  })
    .then((response) => {
      dispatch({ type: FORMIO_DATADOWN_STATUS, payload: response.status });
      return response.json();
    })
    .then((payload) => {
      dispatch({ type: FORMIO_DATADOWN, payload });
    });
};

export const checkPersistenceExistence = (
  state = stateName,
) => async (dispatch) => {
  await FormManagement.getPersistentForm(state)
    .then(response => response.json())
    .then(({ data }) => {
      const payload = !!data;
      dispatch({ type: FORMIO_PERSISTENCE_EXISTENCE, payload });
    });
};

export const getPersistentForm = (state = stateName) => async (dispatch) => {
  await FormManagement.getPersistentForm(state)
    .then(response => response.json())
    .then(({ data: payload }) => {
      dispatch({ type: FORMIO_FORM, payload });
    });
};

export const getForm = formName => async (dispatch) => {
  await FormManagement.getForm(formName)
    .then(response => response.json())
    .then(({ data: payload }) => {
      dispatch({ type: FORMIO_FORM, payload });
    });
};
