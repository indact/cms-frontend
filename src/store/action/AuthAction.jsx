import Cookies from 'universal-cookie';
import {
  AUTH_LOGIN_STATUS,
  AUTH_LOGIN_TOKEN,
  AUTH_LOGIN_ERROR,
  AUTH_SIGNUP_STATUS,
  AUTH_SIGNUP_TOKEN,
  AUTH_SIGNUP_ERROR,
  USER_INFO,
  AUTH_USER_MOBILE,
  AUTH_RESETPASSWORD_KEY,
  AUTH_SEND_OTP_STATUS,
  AUTH_SEND_OTP_ERROR,
  AUTH_RESEND_OTP_STATUS,
  AUTH_RESEND_OTP_ERROR,
  AUTH_VERIFY_OTP_STATUS,
  AUTH_VERIFY_OTP_ERROR,
  AUTH_RESETPASSWORD_STATUS,
  AUTH_RESETPASSWORD_ERROR,
  AUTH_USER_ID,
  USER_STATES,
} from './Types';
import AuthApi from '../../service/AuthApi';

const cookies = new Cookies();

export const login = data => (dispatch) => {
  AuthApi.login(data).then((Response) => {
    dispatch({
      type: AUTH_LOGIN_STATUS,
      payload: Response.status,
    });
    Response.json().then((body) => {
      if (Response.status !== 200) {
        dispatch({ type: AUTH_LOGIN_ERROR, payload: body });
      } else {
        // console.log('action');
        dispatch({ type: AUTH_LOGIN_TOKEN, payload: body.token });
        const userBasicDetail = { ...body.data };
        cookies.set('userBasicDetail', userBasicDetail, {
          path: '/',
          maxAge: 86400,
        });
        dispatch({ type: USER_INFO, payload: userBasicDetail });
        cookies.set('token', body.token, { path: '/', maxAge: 86400 });
        dispatch({ type: AUTH_USER_ID, payload: body.data.id });
        // dispatch({ type: USER_STATES, payload: body.data.states });
      }
    });
  });
};

export const signup = data => (dispatch) => {
  AuthApi.signup(data).then((Response) => {
    dispatch({ type: AUTH_SIGNUP_STATUS, payload: Response.status });
    Response.json().then((body) => {
      if (Response.status !== 200) {
        dispatch({ type: AUTH_SIGNUP_ERROR, payload: body });
      } else {
        dispatch({ type: AUTH_SIGNUP_TOKEN, payload: body.token });
        cookies.set('token', body.token, { path: '/', maxAge: 86400 });
        const {
          id, name, email, mobile,
        } = body.data;
        const userBasicDetail = {
          id,
          name,
          email,
          mobile,
        };
        cookies.set('userBasicDetail', userBasicDetail, {
          path: '/',
          maxAge: 86400,
        });
        dispatch({ type: USER_INFO, payload: userBasicDetail });
        dispatch({ type: AUTH_USER_ID, payload: body.data.id });
      }
    });
  });
};

export const Forgotpassword = data => (dispatch) => {
  AuthApi.Forgotpassword(data).then((Response) => {
    dispatch({ type: AUTH_SEND_OTP_STATUS, payload: Response.status });
    Response.json().then((body) => {
      dispatch({ type: AUTH_USER_MOBILE, payload: data.mobile });
      // eslint-disable-next-line no-unused-expressions
      Response.status !== 200
        ? dispatch({ type: AUTH_SEND_OTP_ERROR, payload: body })
        : '';
    });
  });
};

export const ResendOtp = data => (dispatch) => {
  AuthApi.ResendOtp(data).then((Response) => {
    dispatch({ type: AUTH_RESEND_OTP_STATUS, payload: Response.status });
    Response.json().then((body) => {
      // eslint-disable-next-line no-unused-expressions
      Response.status !== 200
        ? dispatch({ type: AUTH_RESEND_OTP_ERROR, payload: body })
        : '';
    });
  });
};
export const VerifyOtp = data => (dispatch) => {
  AuthApi.VerifyOtp(data).then((Response) => {
    dispatch({ type: AUTH_VERIFY_OTP_STATUS, payload: Response.status });
    Response.json().then((body) => {
      dispatch({ type: AUTH_RESETPASSWORD_KEY, payload: body });
      // eslint-disable-next-line no-unused-expressions
      Response.status !== 200
        ? dispatch({ type: AUTH_VERIFY_OTP_ERROR, payload: body })
        : '';
    });
  });
};

export const ResetPassword = data => (dispatch) => {
  AuthApi.ResetPassword(data).then((Response) => {
    dispatch({ type: AUTH_RESETPASSWORD_STATUS, payload: Response.status });
    Response.json().then((body) => {
      // eslint-disable-next-line no-unused-expressions
      Response.status !== 200
        ? dispatch({ type: AUTH_RESETPASSWORD_ERROR, payload: body })
        : '';
    });
  });
};

// export const ResetPassword = data => (dispatch) => {
//   AuthApi.ResetPassword(data).then((Response) => {
//     dispatch({ type: AUTH_RESETPASSWORD_STATUS, payload: Response.status });
//     Response.json().then((body) => {
//       // dispatch({ type: AUTH_RESETPASSWORD_KEY, payload: body });
//       // eslint-disable-next-line no-unused-expressions
//       Response.status !== 200
//         ? dispatch({ type: AUTH_RESETPASSWORD_ERROR, payload: body })
//         : '';
//     });

//   });
// };

export const Refresh = data => (dispatch) => {
  dispatch({ type: AUTH_SIGNUP_STATUS, payload: 0 });
  dispatch({ type: AUTH_LOGIN_STATUS, payload: 0 });
  dispatch({ type: AUTH_SEND_OTP_STATUS, payload: 0 });
  dispatch({ type: AUTH_VERIFY_OTP_STATUS, payload: 0 });
  dispatch({ type: AUTH_RESETPASSWORD_STATUS, payload: 0 });
  dispatch({ type: AUTH_VERIFY_OTP_STATUS, payload: 0 });
};
