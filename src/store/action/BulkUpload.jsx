import Cookies from 'universal-cookie';
import { BULKUPLOAD_MESSAGE, BULKUPLOAD_STATUS } from './Types';
import BuklUploadApi from '../../service/BulkUpload';

const cookies = new Cookies();
const stateId = cookies.get('state').id;
const bulkUploadAction = param => async (dispatch) => {
  // console.log({ param });

  const data = {
    state: stateId,
    mobile: param.mobile,
    tasks_status: param.status.id,
    task_type: param.task.id,
  };
  // console.log({ data });
  dispatch({ type: BULKUPLOAD_STATUS, payload: 'pending' });
  dispatch({ type: BULKUPLOAD_MESSAGE, payload: [] });
  await BuklUploadApi.buklUploadApi(data)
    .then((response) => {
      // console.log({ response });
      dispatch({
        type: BULKUPLOAD_STATUS,
        payload: response.status,
      });
      return response.json();
    })
    .then((body) => {
      // console.log({ body });
      dispatch({
        type: BULKUPLOAD_MESSAGE,
        payload: body.message,
      });
    });
};

// eslint-disable-next-line import/prefer-default-export
export { bulkUploadAction };
