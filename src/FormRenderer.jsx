import React, { Component } from 'react';
import { Form } from 'react-formio';
import 'formiojs/dist/formio.full.css';
import { connect } from 'react-redux';

import {
  getForm,
  anonymousSubmission,
  resetSubmission,
} from './store/action/FormioAction';

export class FormRenderer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      submission: {},
    };
  }

  componentDidMount = async () => {
    const {
      match: {
        params: { formname },
      },
      ...actions
    } = this.props;
    await actions.getForm(formname);
  };

  render() {
    const { submission } = this.state;
    const { form, ...actions } = this.props;
    if (form) {
      return (
        <div className="container mt-5">
          <div className="card">
            <div className="card-body">
              <Form
                form={form}
                submission={submission}
                onChange={data => this.setState({ submission: data })}
                onSubmit={async () => {
                  actions.anonymousSubmission(form, submission);
                  setTimeout(() => {
                    window.location.reload();
                  }, 2000);
                }}
              />
            </div>
          </div>
        </div>
      );
    }
    return <h1 className="text-center">Waiting for form to load ...</h1>;
  }
}

const mapStateToProps = state => ({
  form: state.formio.form,
});

export default connect(mapStateToProps, {
  getForm,
  anonymousSubmission,
  resetSubmission,
})(FormRenderer);
