import React, { Component } from 'react';
import { connect } from 'react-redux';
// import { AppSwitch } from '@coreui/react';
import Cookies from 'universal-cookie';
import _ from 'lodash';
import {
  Button, Card, Col, CardBody, Form, Row, Label,
} from 'reactstrap';
import AutocompleteInput from '../../../components/feature_permission/AutocompleteInput';
import AutocompleteMultipleInput from '../../../components/feature_permission/AutocompleteMultipleInput';
import FeatureCard from '../../../components/feature_permission/FeatureCard';
import Notification from '../../notifications';

import {
  getAllUsers,
  // getAllStates,
  getAllPermissions,
  getAllFeatures,
  searchFromState,
  addNewPermissionFun,
  updatePermissionFun,
} from '../../../store/action/UserManagementAction';

require('../../../assets/css/feature_permission.css');

const cookies = new Cookies();

class FeaturePermissions extends Component {
  constructor(props) {
    super(props);

    this.state = {
      toggleSearch: true,
      user: { name: [], id: [] },
      selectedFeature: { name: '', id: '' },
      permission: { name: '', id: '' },
      modify: false,
      modifyMultiple: true,
      searchLog: [],
      resetUsers: false,
    };
  }

  componentDidMount = () => {
    const { ...actions } = this.props;
    actions.getAllUsers();
    actions.getAllPermissions();
    actions.getAllFeatures();
  };

  componentDidUpdate = (prevProps) => {
    const { state, ...actions } = this.props;
    if (prevProps.state !== state) {
      actions.getAllUsers(state.id);
      this.findFeature();
    }
  };

  makeSearchQ = (id = null, value = '') => {
    let query = [];
    const {
      findFeature,
      state: { user, selectedFeature, state },
    } = this;
    if (id) {
      // if (id === 'user') {
      //   if (value !== '') query.push(`name=${value}`);
      // } else if (user.name.length > 0) {
      //   query.push(`name=${user.name}`);
      // }

      if (id === 'features') {
        if (value !== '') query.push(`features=${value}`);
      } else if (selectedFeature.id !== '') {
        query.push(`features=${selectedFeature.id}`);
      }

      // if (id === 'state') {
      //   if (value !== '') query.push(`states=${value}`);
      // } else if (state.id !== '') {
      //   query.push(`states=${state.id}`);
      // }
    }
    query = query.filter(item => item);
    query.push('page_size=0');
    if (query.length > 0) findFeature(query.join('&'));
  };

  changeButtonStateMultiple = (data) => {
    const {
      value, name, id, find,
    } = data;
    this.setState(prevState => ({
      searchLog: [...prevState.searchLog, find],
      modifyMultiple: false,
      toggleSearch: false,
      resetUsers: false,
      [id]: {
        name: [...name],
        id: [...value],
      },
    }));
    this.findFeature([`name=${find.id}`].join('&'));
    // this.setState((prevState) => {
    //   return {
    //     modifyMultiple: false,
    //     toggleSearch: false,
    //     [id]: {
    //       name: [...name, ...prevState[id].name],
    //       id: [...value, ...prevState[id].id],
    //     },
    //   };
    // });
  };

  changeButtonState = (e) => {
    const { modify } = this.state;
    const thisProps = this.props;
    const { value, id } = e.target;
    // console.log(value, id);
    let obj = '';
    if (id === 'permission') {
      obj = { name: value, id: value };
      this.setState({ resetUsers: false });
    }
    if (id === 'user') {
      thisProps.users.data.forEach((val) => {
        if (value === val.name) {
          obj = { name: value, id: val.id };
        }
      });
      this.setState({ resetUsers: false });
    }
    if (id === 'features') {
      thisProps.features.data.forEach((val) => {
        if (value === val.name) {
          obj = { name: value, id: val.id };
        }
      });
      // console.log(obj);
      this.setState({
        selectedFeature: obj,
      });
      // this.setState({
      //   user: { name: [], id: [] },
      //   searchLog: [],
      //   resetUsers: true,
      // });
    }
    this.setState({ toggleSearch: false, [id]: obj });
    if (modify === false) {
      if (id === 'features') {
        this.makeSearchQ(id, obj.id);
      }
    }
  };

  findFeature = (query, feature = null) => {
    const thisProps = this.props;
    if (feature !== null) {
      const { name, id } = feature;
      this.setState({ selectedFeature: { name, id } });
    }
    this.setState({ modify: false });
    thisProps.searchFromState(query);
  };

  modifyPermission = (data, per) => {
    const thisProps = this.props;
    const stateId = cookies.get('state').id || '';
    const newPermission = {
      userId: [data.userId],
      stateId,
      featureId: data.featureId,
      permission: per,
    };
    thisProps.updatePermissionFun(newPermission);

    this.makeSearchQ('id', 'value');
    // thisProps.updatePermissionFun(modify, permissionUpdates);

    // Object.keys(data).forEach((v, i) => {
    //   if (v !== 'id') {
    //     this.setState({ [v]: data[v] });
    //   } else {
    //     this.setState({ modify: data[v] });
    //   }
    // });
  };

  addNewPermissionMethod = () => {
    const {
      state: { selectedFeature, user, permission },
      makeSearchQ,
    } = this;
    const { state, ...thisProps } = this.props;
    const stateId = state.id || cookies.get('state').id || '';
    const newPermission = {
      userId: user.id,
      stateId,
      featureId: selectedFeature.id,
      permission: permission.name,
    };
    thisProps.addNewPermissionFun(newPermission);
    this.setState({
      permission: { name: '', id: '' },
      user: { name: [], id: [] },
      searchLog: [],
      resetUsers: true,
    });
    makeSearchQ('id', 'value');
  };

  updatePermissionMethod = () => {
    const {
      state: { modify, permission },
      makeSearchQ,
    } = this;
    const thisProps = this.props;
    const permissionUpdates = {
      permission: [permission],
    };
    thisProps.updatePermissionFun(modify, permissionUpdates);
    // this.setState({modify:false,permission:"",user:"",state:"",selectedFeature:""})
    this.setState({ modify: false });
    makeSearchQ('id', 'value');
  };

  render() {
    const {
      findFeature,
      modifyPermission,
      changeButtonState,
      changeButtonStateMultiple,
      addNewPermissionMethod,
      updatePermissionMethod,
      props: {
        all_permissions,
        users,
        features,
        userData,
        searchresult,
        addData,
        userPermissions,
      },
      state: {
        user,
        permission,
        toggleSearch,
        modify,
        modifyMultiple,
        resetUsers,
        selectedFeature,
      },
    } = this;
    let users1;
    let modifyOrAdd;
    const allpermissions1 = all_permissions.data.map((v, i) => v.name);
    if (typeof users !== 'undefined') {
      users1 = users.map((v, i) => ({
        label: v.name,
        id: v.id,
        value: v.value,
      }));
    }
    const features1 = features.data.map(v => v.name);
    // const states1 = states.data.map((v, i) => v.name);
    const searchRows = features.data.map(feature => (
      <FeatureCard
        key={feature.id}
        feature={feature}
        toggleSearch={toggleSearch}
        search_state={findFeature}
        modifyPermissionFun={modifyPermission}
        featureData={
          typeof userPermissions[feature.name] !== 'undefined'
          && userPermissions[feature.name]
        }
      />
    ));
    if (modify) {
      modifyOrAdd = (
        <Button type="button" onClick={updatePermissionMethod} color="primary">
          Update
        </Button>
      );
    } else {
      modifyOrAdd = (
        <Button type="button" onClick={addNewPermissionMethod} color="primary">
          Save
        </Button>
      );
    }
    return (
      <div>
        <Notification notifor="userdataall" notidata={userData} />
        <Notification notifor="searchdata" notidata={searchresult} />
        <Notification notifor="addpermission" notidata={addData} />
        <Row>
          <Col xs="12" sm="12" md="12">
            <Card>
              <CardBody>
                <Form action="" method="post" inline>
                  <Label className="pr-1">Features</Label>
                  <AutocompleteInput
                    suggestions={features1}
                    buttonClick={changeButtonState}
                    setValue={selectedFeature.name}
                    isModified={modify}
                    id="features"
                  />
                  <Label className="pr-1">Users</Label>
                  <AutocompleteMultipleInput
                    suggestions={users1}
                    buttonClick={changeButtonStateMultiple}
                    setValue={user.id}
                    isModified={modifyMultiple}
                    isReset={resetUsers}
                    id="user"
                  />
                  <Label className="pr-1">Permissions</Label>
                  <AutocompleteInput
                    suggestions={allpermissions1}
                    buttonClick={changeButtonState}
                    setValue={permission.name}
                    isModified={modify}
                    id="permission"
                  />
                  <Col col="2" xl className="mb-3 mb-xl-0">
                    {modifyOrAdd}
                  </Col>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xs="12" sm="12" md="12">
            <Card className="card-height">
              <CardBody data-test="save_test">{searchRows}</CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    users: state.userManagement.users.data,
    addData: state.userManagement.data,
    userData: state.userManagement.users,
    states: state.userManagement.states,
    state: state.user.state,
    all_permissions: state.userManagement.all_permissions,
    features: state.userManagement.features,
    searchresult: state.userManagement.search.data,
    userPermissions: state.userManagement.userPermissions,
  };
}

export default connect(
  mapStateToProps,
  {
    getAllUsers,
    getAllFeatures,
    // getAllStates,
    getAllPermissions,
    searchFromState,
    addNewPermissionFun,
    updatePermissionFun,
  },
)(FeaturePermissions);
