import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import _ from 'lodash';
import {
  enqueueSnackbar,
  closeSnackbar,
} from '../../store/action/NotificationAction';

class Notification extends Component {
  componentWillReceiveProps = (nextprops) => {
    const thisProps = this.props;
    if (!_.isEqual(nextprops.notidata, thisProps.notidata)) {
      if (
        nextprops.notidata.status !== ''
        && nextprops.notidata.status !== 200
        && nextprops.notidata.status !== 'pending'
        && typeof nextprops.notidata.data !== 'undefined'
      ) {
        nextprops.closeSnackbar(nextprops.notifor);
        let name = '';
        if (
          typeof nextprops.notidata.data.error !== 'undefined'
          && nextprops.notidata.data.error !== ''
        ) {
          name = nextprops.notidata.data.error.message;
        }
        if (name !== '') {
          nextprops.enqueueSnackbar({
            message: name,
            options: {
              variant: 'error',
              action: key => (
                <Button onClick={() => nextprops.closeSnackbar(key)}>X</Button>
              ),
            },
          });
        }
      }
      if (
        nextprops.notidata.status === 'pending'
        || nextprops.notidata.status === ''
      ) {
        nextprops.enqueueSnackbar({
          message: 'Processing...',
          options: {
            key: nextprops.notifor,
            variant: 'info',
            persist: true,
            action: key => (
              <Button onClick={() => nextprops.closeSnackbar(key)}>X</Button>
            ),
          },
        });
      } else if (
        nextprops.notidata.status === 200
        && (Object.keys(nextprops.notidata.data).length > 0
          || nextprops.notidata.data.data === 'undefined')
      ) {
        nextprops.closeSnackbar(nextprops.notifor);
        let updatename = '';
        if (
          typeof nextprops.notidata.data.message !== 'undefined'
          && nextprops.notidata.data.message !== ''
        ) {
          updatename = nextprops.notidata.data.message;
        }
        if (
          typeof nextprops.notidata.data.msg !== 'undefined'
          && nextprops.notidata.data.msg !== ''
        ) {
          updatename = nextprops.notidata.data.msg;
        }
        if (updatename !== '') {
          nextprops.enqueueSnackbar({
            message: updatename,
            options: {
              variant: 'success',
              autoHideDuration: 4000,
              action: key => (
                <Button onClick={() => nextprops.closeSnackbar(key)}>X</Button>
              ),
            },
          });
        }
      }
    }
  };

  render() {
    return '';
  }
}

function mapStateToProps() {
  //   return state;
  return {};
}

export default connect(
  mapStateToProps,
  {
    enqueueSnackbar,
    closeSnackbar,
  },
)(Notification);
