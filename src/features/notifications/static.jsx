import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import _ from 'lodash';
import {
  enqueueSnackbar,
  closeSnackbar,
} from '../../store/action/NotificationAction';

class StaticNotification extends Component {
  componentWillMount = () => {
    const thisProps = this.props;
    thisProps.enqueueSnackbar({
      message: thisProps.message,
      options: {
        variant: thisProps.variant,
        autoHideDuration: 1000,
      },
    });
  };

  // componentWillUpdate = (nextProps) => {
  //   const thisProps = this.props;
  //   thisProps.enqueueSnackbar({
  //     message: thisProps.message,
  //     options: {
  //       autoHideDuration: 1000,
  //       variant: 'warning',
  //     },
  //   });
  // }

  render() {
    return '';
  }
}

function mapStateToProps() {
  //   return state;
  return {};
}

export default connect(
  mapStateToProps,
  {
    enqueueSnackbar,
    closeSnackbar,
  },
)(StaticNotification);
