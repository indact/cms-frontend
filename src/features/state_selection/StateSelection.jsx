import React, { Component } from 'react';
import {
  Container,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Row,
} from 'reactstrap';
import { connect } from 'react-redux';
import '../../assets/css/dashboard.css';

import userActions from '../../store/action/UserAction';

const { changeState } = userActions;

class Page404 extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      logout: false,
      dropdownOpen: false,
    };
  }

  handleChange = async (state) => {
    const { ...action } = this.props;
    action.changeState(state);
    // document.location.reload();
  };

  toggle = () => {
    const oldState = { ...this.state };
    oldState.dropdownOpen = !oldState.dropdownOpen;
    this.setState({ ...oldState });
  };

  render() {
    const { stateList } = this.props;
    const { dropdownOpen } = this.state;
    const states = stateList;
    // eslint-disable-next-line
    // stateList && stateList.constructor === Array && stateList.length === 0
    //   ? ''
    //   : cookies.set('states', stateList, { path: '/', maxAge: 86400 });

    // const states = cookies.get('states') ? cookies.get('states') : [];
    // const stateName = cookies.get('state')
    //   ? cookies.get('state').StateName
    //   : 'No State Selected';
    return (
      <div className="app flex-row align-items-center">
        <Container>
          {states.length === 0 ? (
            <Row className="justify-content-center default-msg">
              <div className="default-no-state">
                Please contact CMS admin for assignment of states and
                permissions.
              </div>
            </Row>
          ) : (
            <div>
              <div className="default-state">
                Please select login state to see your features and permissions
                assigned to you.
              </div>
              <Row className="justify-content-center">
                {/* <Col md="6"> */}
                <ButtonDropdown
                  className="mr-1"
                  isOpen={dropdownOpen}
                  toggle={() => {
                    this.toggle();
                  }}
                >
                  <DropdownToggle caret color="danger" data-test="drop_test">
                    Select State
                  </DropdownToggle>
                  <DropdownMenu
                    modifiers={{
                      setMaxHeight: {
                        enabled: true,
                        order: 890,
                        fn: data => ({
                          ...data,
                          styles: {
                            ...data.styles,
                            overflow: 'auto',
                            maxHeight: '300px',
                          },
                        }),
                      },
                    }}
                    data-test="drop_menu"
                  >
                    {states
                    && states.constructor === Array
                    && states.length === 0 ? (
                      <DropdownItem
                        onClick={() => {
                          this.toggle();
                          // this.handleChange(props.id)
                        }}
                      >
                        No State Assgin
                      </DropdownItem>
                    ) : (
                      states.map(state => (
                        <DropdownItem
                          data-test="drop_item"
                          key={state.id}
                          onClick={() => {
                            this.toggle();
                            const { history } = this.props;
                            history.push('/admin/dashboard');
                            this.handleChange(state);
                          }}
                        >
                          {state.name}
                        </DropdownItem>
                      ))
                    )}
                  </DropdownMenu>
                </ButtonDropdown>
              </Row>
            </div>
          )}
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  token: state.auth.token,
  status: state.auth.loginStatus,
  stateList: state.user.states,
  // state: state.user.states,
});

export default connect(mapStateToProps, { changeState })(Page404);
