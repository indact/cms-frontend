import React, { Component } from 'react';
import { connect } from 'react-redux';

import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';

import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Cookies from 'universal-cookie';
import Tooltip from '@material-ui/core/Tooltip';

import {
  Card, CardBody, CardHeader, Button,
} from 'reactstrap';

import '../../assets/css/meterialUi.css';
import SelectSingle from '../../components/select_input/single_select/SingleSelect';
import {
  addNewTaskTypeFun,
  getAllTasksType,
  updateTaskType,
  deleteTaskType,
  getAllCycle,
} from '../../store/action/TaskManagementAction';
import { getFormList } from '../../store/action/FormioAction';
import { ExpansionPanels } from '../../components';
import Notification from '../notifications';
import Dialog from '../../components/notifications/modals/Dialog';

const cookies = new Cookies();

class TaskTypes extends Component {
  style = {
    row: {
      cursor: 'pointer',
    },
    search: {
      float: 'right',
    },
    cardBodyStyle: {
      height: '200px',
      overflow: 'auto',
    },
    AddButton: {
      cursor: 'pointer',
      fontSize: '1.7em',
    },
  };

  constructor(props) {
    super(props);
    this.state = {
      popup: false,
      newTaskName: '',
      isModify: false,
      selectedIndex: '',
      name: '',
      Snackbars: true,
      sequence: '',
      cycle: '',
      cycleId: '',
      sla: '',
      description: '',
      form: '',
      selectedForm: '',
      dOpen: false,
      deleteID: '',

      error: {
        name: false,
        sequence: false,
        cycle: false,
        sla: false,
        description: false,
        form: false,
        selectedForm: false,
      },
    };
  }

  componentWillMount = async () => {
    const { state, ...actions } = this.props;
    actions.getAllTasksType(state.id);
    actions.getAllCycle(state.id);
    await actions.getFormList(state.name);
  };

  componentDidUpdate = async (prev) => {
    const { state, ...actions } = this.props;
    if (prev.state !== state) {
      actions.getAllTasksType(state.id);
      actions.getAllCycle(state.id);
      await actions.getFormList(state.name);
    }
  };

  toggleAddStatus = () => {
    this.setState(prevState => ({ popup: !prevState.popup }));
  };

  handleChange = (event) => {
    const oldState = { ...this.state };
    oldState[event.target.id] = event.target.value;
    this.setState({ ...oldState });
  };

  handelError = (name) => {
    const { error } = { ...this.state };
    error[name] = true;
    this.setState({ ...error });
  };

  addTaskType = async () => {
    const { state, ...thisProps } = this.props;
    const thisState = this.state;

    if (
      thisState.name !== ''
      && thisState.sequence !== ''
      && thisState.cycle !== ''
      && thisState.sla !== ''
      && thisState.description !== ''
      && thisState.selectedForm !== ''
      && cookies.get('state') !== ''
    ) {
      const data = {
        state: state.id || cookies.get('state').id,
        name: thisState.name,
        sequence: thisState.sequence,
        cycle: thisState.cycle.value,
        form: {
          formId: thisState.selectedForm.id,
          formPath: thisState.selectedForm.path,
        },
        sla: thisState.sla,
        description: thisState.description,
      };

      await thisProps.addNewTaskTypeFun(data);
      this.resetTaskName();
    } else {
      if (thisState.name === '') {
        this.handelError('name');
      }
      if (thisState.sequence === '') {
        this.handelError('sequence');
      }
      if (thisState.sla === '') {
        this.handelError('sla');
      }
      if (thisState.description === '') {
        this.handelError('description');
      }
      if (thisState.form === '') {
        this.handelError('form');
      }
      if (thisState.cycle === '') {
        this.handelError('cycle');
      }
      if (thisState.selectedForm === '') {
        this.handelError('selectedForm');
      }
    }
    await thisProps.getAllTasksType();
  };

  selectTaskType = (val) => {
    const oldState = { ...this.state };
    oldState.name = val.name;
    oldState.sequence = val.sequence;
    oldState.sla = val.sla;
    oldState.description = val.description;
    oldState.cycle = {
      label: val.cycle_name,
      id: val.cycle,
      value: val.cycle_name,
    };
    oldState.form = val.form;
    oldState.isModify = val.id;
    oldState.selectedIndex = val.id;
    oldState.selectedForm = val.form === undefined
        ? {}
        : {
          id: val.form.formId,
          path: val.form.formPath,
          label: val.form.formPath,
        };
    this.setState({ ...oldState });
  };

  resetTaskName = () => {
    const oldState = { ...this.state };
    oldState.name = '';
    oldState.sequence = '';
    oldState.sla = '';
    oldState.description = '';
    oldState.cycle = '';
    oldState.form = '';
    oldState.newTaskName = '';
    oldState.isModify = false;
    oldState.selectedIndex = '';
    oldState.selectedForm = '';
    oldState.deleteID = '';

    this.setState({ ...oldState });
  };

  modifyTaskType = async () => {
    const { state, ...thisProps } = this.props;
    const thisState = this.state;
    const data = {
      state: state.id || cookies.get('state').id,
      name: thisState.name,
      sequence: thisState.sequence,
      cycle: thisState.cycle.value,
      form: {
        formId: thisState.selectedForm.id,
        formPath: thisState.selectedForm.path,
      },
      sla: thisState.sla,
      description: thisState.description,
    };

    await thisProps.updateTaskType(thisState.isModify, data);
    thisProps.getAllTasksType();
    this.resetTaskName();
  };

  deleteTaskTypes = (val) => {
    const thisProps = this.props;
    this.setState({ dOpen: true, deleteID: val.id });
  };

  handleChangeSingle = (value) => {
    this.setState({ selectedForm: value });
  };

  render() {
    const {
      newTaskName,
      isModify,
      name,
      sequence,
      cycle,
      sla,
      description,
      error,
      selectedForm,
      dOpen,
      deleteID,
    } = this.state;

    const {
      allTasks, formList, allCycle, addTask, state,
    } = this.props;
    const thisProps = this.props;

    const data = formList.data.data === undefined ? [] : formList.data.data;
    const cycledata = allCycle.data.data === undefined ? [] : allCycle.data.data;

    const allForms = data.map(suggestion => ({
      value: suggestion.title,
      label: suggestion.title,
      components: suggestion.components,
      id: suggestion._id,
      path: suggestion.path,
    }));
    const allCycles = cycledata.map(suggestion => ({
      value: suggestion.name,
      label: suggestion.name,
      id: suggestion.id,
    }));

    let AllRows = '';
    if (
      typeof allTasks.data.data !== 'undefined'
      && allTasks.data.data.length > 0
    ) {
      AllRows = allTasks.data.data.map((val) => {
        let active;
        if (val.id === isModify) {
          active = (
            <ExpansionPanels
              type={1}
              data={val}
              remove={() => this.deleteTaskTypes(val)}
              setting={() => this.selectTaskType(val)}
            />
          );
        } else {
          active = (
            <ExpansionPanels
              type={1}
              data={val}
              remove={() => this.deleteTaskTypes(val)}
              setting={() => this.selectTaskType(val)}
            />
          );
        }
        return active;
      });
    }
    let AddModifyButton = '';
    if (isModify) {
      AddModifyButton = (
        <div>
          <Button color="warning" onClick={this.modifyTaskType}>
            Modify
          </Button>
          {'  '}
          <Button color="danger" onClick={this.resetTaskName}>
            Cancel
          </Button>
        </div>
      );
    } else {
      AddModifyButton = (
        <Button color="primary" onClick={this.addTaskType}>
          Create
        </Button>
      );
    }
    const stateName = state.name
      || (cookies.get('state') && cookies.get('state').name)
      || 'No State Selected';

    return (
      <Card
        style={{
          border: '0px solid #c8ced3',
        }}
      >
        <Dialog
          Open={dOpen}
          handleClose={() => {
            this.setState({ dOpen: false });
          }}
          Agree={() => {
            thisProps.deleteTaskType(deleteID);
            thisProps.getAllTasksType();
            this.setState({ dOpen: false });
          }}
          title="Delete Task Type"
          message=" Do you want to delete this task type?"
        />
        <Notification notifor="allTasks" notidata={allTasks} />
        <Notification notifor="addTask" notidata={addTask} />

        <Grid container>
          <Grid item xs={12} sm={6}>
            <Card
              className="rounded-0 scroll-sm"
              style={{
                height: '78.5vh',
                overflow: 'auto',
                border: 'none',
                width: '100%',
              }}
            >
              <CardHeader
                style={{
                  textAlign: 'center',
                  padding: '0.75rem 1.25rem',
                  marginBottom: 0,
                  backgroundColor: ' #f0f3f5',
                  borderBottom: '1px solid #c8ced3',
                }}
              >
                {isModify && `Modify  ${newTaskName} Task Types`}
                {!isModify && 'Add Task Types'}
              </CardHeader>
              <CardBody style={this.style.cardBodyStyle}>
                <form autoComplete="off">
                  <Grid container spacing={3}>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        error={error.name}
                        label="Name"
                        id="name"
                        type="search"
                        margin="normal"
                        value={name === true ? '' : name}
                        onChange={this.handleChange}
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        disabled
                        defaultValue={stateName}
                        id="State"
                        label="State"
                        type="search"
                        fullWidth
                        value={stateName}
                        onChange={this.handleChange}
                        margin="normal"
                      />
                    </Grid>
                    <Grid item xs={12} sm={12}>
                      <TextField
                        required
                        id="description"
                        label="Description"
                        type="search"
                        error={error.description}
                        fullWidth
                        value={description === true ? '' : description}
                        onChange={this.handleChange}
                        margin="normal"
                      />
                    </Grid>
                    <Grid item xs={12} sm={4}>
                      <TextField
                        required
                        id="sequence"
                        label="Sequence Number"
                        type="search"
                        error={error.sequence}
                        value={sequence === true ? '' : sequence}
                        onChange={this.handleChange}
                        margin="normal"
                      />
                    </Grid>
                    <Grid item xs={6} sm={4}>
                      <SelectSingle
                        error={error.cycle}
                        label="cycle*"
                        placeholder="Select a cycle"
                        suggestions={allCycles}
                        value={cycle === ' ' ? '' : cycle}
                        handleChangeSingle={value => this.setState({ cycle: value })
                        }
                      />
                    </Grid>
                    <Grid item xs={6} sm={4}>
                      <Tooltip title="Number of days in which task is expected to be completed upon creation">
                        <TextField
                          required
                          id="sla"
                          label="Sla"
                          type="search"
                          error={error.sla}
                          value={sla === true ? '' : sla}
                          onChange={this.handleChange}
                          margin="normal"
                        />
                      </Tooltip>
                    </Grid>
                    <Grid item xs={12} sm={12}>
                      <SelectSingle
                        error={error.selectedForm}
                        label="Forms*"
                        placeholder="Select a form"
                        suggestions={allForms}
                        value={selectedForm}
                        handleChangeSingle={this.handleChangeSingle}
                      />
                    </Grid>
                  </Grid>
                </form>
                {AddModifyButton}
                <div style={{ float: 'right' }}>
                  <IconButton
                    edge="end"
                    aria-label="cached"
                    onClick={() => window.location.reload()}
                  >
                    <Icon style={{ color: ' #17a2b8' }}>cached</Icon>
                  </IconButton>
                </div>
              </CardBody>
            </Card>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Card
              className="rounded-0 scroll-sm"
              style={{
                borderLeft: '1px solid #c8ced3',
                borderRight: '0px solid ',
                borderBottom: '0px solid ',
                borderTop: '0px solid ',
                height: '78.5vh',
                overflow: 'auto',
                width: '100%',
              }}
            >
              <CardHeader
                style={{
                  textAlign: 'center',
                  padding: '0.75rem 1.25rem',
                  marginBottom: 0,
                  backgroundColor: ' #f0f3f5',
                  borderBottom: '1px solid #c8ced3',
                }}
              >
                All Task Type Forms
              </CardHeader>
              <CardBody style={this.style.cardBodyStyle}>{AllRows}</CardBody>
            </Card>
          </Grid>
        </Grid>
      </Card>
    );
  }
}

function mapStateToProps(state) {
  return {
    allTasks: state.taskManagement.allTasks,
    addTask: state.taskManagement.addTask,
    allCycle: state.taskManagement.allCycle,
    formList: state.formio.formList,
    state: state.user.state,
  };
}

export default connect(mapStateToProps, {
  addNewTaskTypeFun,
  getAllTasksType,
  updateTaskType,
  deleteTaskType,
  getFormList,
  getAllCycle,
})(TaskTypes);
