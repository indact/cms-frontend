import React, { Component } from 'react';
import { connect } from 'react-redux';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Cookies from 'universal-cookie';

import {
  Card, CardBody, CardHeader, Button,
} from 'reactstrap';
import '../../assets/css/meterialUi.css';
import SelectSingle from '../../components/select_input/single_select/SingleSelect';
import {
  addNewTaskStatusFun,
  getAllTaskStatus,
  updateTaskStatus,
  deleteTaskStatus,
} from '../../store/action/TaskManagementAction';
import Notification from '../notifications';

import { ExpansionPanels } from '../../components';
import Dialog from '../../components/notifications/modals/Dialog';

const cookies = new Cookies();

class TaskTypes extends Component {
  style = {
    row: {
      cursor: 'pointer',
    },
    search: {
      float: 'right',
    },
    cardBodyStyle: {
      height: '200px',
      overflow: 'auto',
    },
    AddButton: {
      cursor: 'pointer',
      fontSize: '1.7em',
    },
  };

  constructor(props) {
    super(props);
    this.state = {
      popup: false,
      newTaskName: '',
      isModify: false,
      selectedIndex: '',
      name: '',

      description: '',

      category: '',
      deleteID: '',
      dOpen: false,
      error: {
        name: false,

        description: false,

        category: false,
      },
    };
  }

  componentWillMount = async () => {
    const { state, ...actions } = this.props;
    await actions.getAllTaskStatus(state.id);
  };

  componentDidUpdate = async (prev) => {
    const { state, ...actions } = this.props;
    if (prev.state !== state) {
      await actions.getAllTaskStatus(state.id);
    }
  };

  toggleAddStatus = () => {
    this.setState(prevState => ({ popup: !prevState.popup }));
  };

  handleChange = (event) => {
    const oldState = { ...this.state };
    oldState[event.target.id] = event.target.value;
    this.setState({ ...oldState });
  };

  handelError = (name) => {
    const { error } = { ...this.state };
    error[name] = true;
    this.setState({ ...error });
  };

  addTaskType = async () => {
    const { state, ...thisProps } = this.props;
    const thisState = this.state;

    if (
      thisState.name !== ''
      && thisState.description !== ''
      && thisState.category !== ''
      && cookies.get('state') !== ''
    ) {
      const data = {
        state: state.id || cookies.get('state').id,
        name: thisState.name,

        category: thisState.category.label,

        description: thisState.description,
      };

      await thisProps.addNewTaskStatusFun(data);
      this.resetTaskName();
    } else {
      if (thisState.name === '') {
        this.handelError('name');
      }

      if (thisState.description === '') {
        this.handelError('description');
      }

      if (thisState.category === '') {
        this.handelError('category');
      }
    }
    await thisProps.getAllTaskStatus();
  };

  selectTaskType = (val) => {
    const oldState = { ...this.state };
    oldState.name = val.name;

    oldState.description = val.description;

    oldState.isModify = val.id;
    oldState.selectedIndex = val.id;
    oldState.category = { value: val.category, label: val.category };
    this.setState({ ...oldState });
  };

  resetTaskName = () => {
    const oldState = { ...this.state };
    oldState.name = '';

    oldState.description = '';

    oldState.newTaskName = '';
    oldState.isModify = false;
    oldState.selectedIndex = '';
    oldState.category = '';
    this.setState({ ...oldState });
  };

  modifyTaskType = async () => {
    const thisProps = this.props;
    const thisState = this.state;

    const data = {
      id: thisState.isModify,
      state: cookies.get('state').id,
      name: thisState.name,
      category: thisState.category.value,
      description: thisState.description,
    };

    await thisProps.updateTaskStatus(data);
    thisProps.getAllTaskStatus();
    this.resetTaskName();
  };

  deleteTaskTypes = (val) => {
    const thisProps = this.props;
    this.setState({ dOpen: true, deleteID: val.id });
  };

  render() {
    const thisProps = this.props;
    const {
      newTaskName,
      isModify,

      name,

      description,

      error,
      category,
      deleteID,
      dOpen,
    } = this.state;

    const { allTaskStatus, addTaskStatus, state } = this.props;

    let AllRows = '';
    if (
      typeof allTaskStatus.data.data !== 'undefined'
      && allTaskStatus.data.data.length > 0
    ) {
      AllRows = allTaskStatus.data.data.map((val) => {
        let active;
        if (val.id === isModify) {
          active = (
            <ExpansionPanels
              type={2}
              data={val}
              remove={() => this.deleteTaskTypes(val)}
              setting={() => this.selectTaskType(val)}
            />
          );
        } else {
          active = (
            <ExpansionPanels
              type={2}
              data={val}
              remove={() => this.deleteTaskTypes(val)}
              setting={() => this.selectTaskType(val)}
            />
          );
        }
        return active;
      });
    }
    let AddModifyButton = '';
    if (isModify) {
      AddModifyButton = (
        <div>
          <Button color="warning" onClick={this.modifyTaskType}>
            Modify
          </Button>
          {'  '}
          <Button color="danger" onClick={this.resetTaskName}>
            Cancel
          </Button>
        </div>
      );
    } else {
      AddModifyButton = (
        <Button color="primary" onClick={this.addTaskType}>
          Create
        </Button>
      );
    }
    const stateName = state.name
      || (cookies.get('state') && cookies.get('state').name)
      || 'No State Selected';

    return (
      <Card
        style={{
          border: '0px solid #c8ced3',
        }}
      >
        <Dialog
          Open={dOpen}
          handleClose={() => {
            this.setState({ dOpen: false });
          }}
          Agree={() => {
            thisProps.deleteTaskStatus(deleteID);
            thisProps.getAllTaskStatus();

            this.setState({ dOpen: false });
          }}
          title="Delete Task Status"
          message=" Do you want to delete this taskstatus?"
        />
        <Notification notifor="allTaskstatus" notidata={allTaskStatus} />
        <Notification notifor="addTaskStatus" notidata={addTaskStatus} />
        <Grid container>
          <Grid item xs={12} sm={6}>
            <Card
              className="rounded-0 scroll-sm"
              style={{
                height: '78.5vh',
                overflow: 'auto',
                border: 'none',
                width: '100%',
              }}
            >
              <CardHeader
                style={{
                  textAlign: 'center',
                  padding: '0.75rem 1.25rem',
                  marginBottom: 0,
                  backgroundColor: ' #f0f3f5',
                  borderBottom: '1px solid #c8ced3',
                }}
              >
                {isModify && `Modify  ${newTaskName} Task Status`}
                {!isModify && 'Add Task Status'}
              </CardHeader>
              <CardBody style={this.style.cardBodyStyle}>
                <form autoComplete="off">
                  <Grid container spacing={3}>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        error={error.name}
                        label="Name"
                        id="name"
                        type="search"
                        margin="normal"
                        value={name === true ? '' : name}
                        onChange={this.handleChange}
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        disabled
                        defaultValue={stateName}
                        id="State"
                        label="State"
                        type="search"
                        fullWidth
                        value={stateName}
                        onChange={this.handleChange}
                        margin="normal"
                      />
                    </Grid>
                    <Grid item xs={12} sm={12}>
                      <TextField
                        required
                        id="description"
                        label="Description"
                        type="search"
                        error={error.description}
                        fullWidth
                        value={description === true ? '' : description}
                        onChange={this.handleChange}
                        margin="normal"
                      />
                    </Grid>

                    <Grid item xs={6} sm={4}>
                      <SelectSingle
                        error={error.category}
                        placeholder="Select a Category"
                        suggestions={[
                          { value: 'To Do', label: 'To Do' },
                          { value: 'In Progress', label: 'In Progress' },
                          { value: 'Done', label: 'Done' },
                        ]}
                        value={category}
                        handleChangeSingle={value => this.setState({ category: value })
                        }
                      />
                    </Grid>
                  </Grid>
                </form>
                {AddModifyButton}
                <div style={{ float: 'right' }}>
                  <IconButton
                    edge="end"
                    aria-label="cached"
                    onClick={() => window.location.reload()}
                  >
                    <Icon style={{ color: ' #17a2b8' }}>cached</Icon>
                  </IconButton>
                </div>
              </CardBody>
            </Card>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Card
              className="rounded-0 scroll-sm"
              style={{
                borderLeft: '1px solid #c8ced3',
                borderRight: '0px solid ',
                borderBottom: '0px solid ',
                borderTop: '0px solid ',
                height: '78.5vh',
                overflow: 'auto',
                width: '100%',
              }}
            >
              <CardHeader
                style={{
                  textAlign: 'center',
                  padding: '0.75rem 1.25rem',
                  marginBottom: 0,
                  backgroundColor: ' #f0f3f5',
                  borderBottom: '1px solid #c8ced3',
                }}
              >
                All Task Status
              </CardHeader>
              <CardBody style={this.style.cardBodyStyle}>{AllRows}</CardBody>
            </Card>
          </Grid>
        </Grid>
      </Card>
    );
  }
}

function mapStateToProps(state) {
  return {
    allTaskStatus: state.taskManagement.allTaskStatus,
    addTaskStatus: state.taskManagement.addTaskStatus,
    state: state.user.state,
  };
}

export default connect(mapStateToProps, {
  addNewTaskStatusFun,
  getAllTaskStatus,
  updateTaskStatus,
  deleteTaskStatus,
})(TaskTypes);
