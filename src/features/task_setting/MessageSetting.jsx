import React, { Component } from 'react';
import { connect } from 'react-redux';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Cookies from 'universal-cookie';

import {
  Card, CardBody, CardHeader, Button, Input, Label,
} from 'reactstrap';

import { ExpansionPanels } from '../../components';
import '../../assets/css/meterialUi.css';
import {
  createMessage,
  getAllMessage,
  updateMessage,
  deleteMessage,
} from '../../store/action/TaskManagementAction';
import Notification from '../notifications';
import Dialog from '../../components/notifications/modals/Dialog';

const cookies = new Cookies();

class MessageSetting extends Component {
  style = {
    row: {
      cursor: 'pointer',
    },
    search: {
      float: 'right',
    },
    cardBodyStyle: {
      height: '200px',
      overflow: 'auto',
    },
    AddButton: {
      cursor: 'pointer',
      fontSize: '1.7em',
    },
  };

  constructor(props) {
    super(props);
    this.state = {
      popup: false,
      newTaskName: '',
      isModify: false,
      selectedIndex: '',
      name: '',

      content: '',
      deleteID: '',
      dOpen: false,

      error: {
        name: false,

        content: false,
      },
    };
  }

  componentWillMount = () => {
    const { state, ...actions } = this.props;
    actions.getAllMessage(state.id);
  };

  componentDidUpdate = (prev) => {
    const { state, ...actions } = this.props;
    if (prev.state !== state) {
      actions.getAllMessage(state.id);
    }
  };

  toggleAddStatus = () => {
    this.setState(prevState => ({ popup: !prevState.popup }));
  };

  handleChange = (event) => {
    const oldState = { ...this.state };
    oldState[event.target.id] = event.target.value;
    this.setState({ ...oldState });
  };

  handelError = (name) => {
    const { error } = { ...this.state };
    error[name] = true;
    this.setState({ ...error });
  };

  addTaskType = async () => {
    const { state, ...thisProps } = this.props;
    const thisState = this.state;

    if (
      thisState.name !== ''
      && thisState.content !== ''
      && cookies.get('state') !== ''
    ) {
      const data = {
        state: state.id || cookies.get('state').id,
        name: thisState.name,
        content: thisState.content,
      };

      await thisProps.createMessage(data);
      this.resetTaskName();
    } else {
      if (thisState.name === '') {
        this.handelError('name');
      }

      if (thisState.content === '') {
        this.handelError('description');
      }
    }
    await thisProps.getAllMessage();
  };

  selectTaskType = (val) => {
    const oldState = { ...this.state };
    oldState.name = val.name;

    oldState.content = val.content;

    oldState.isModify = val.id;
    oldState.selectedIndex = val.id;

    this.setState({ ...oldState });
  };

  resetTaskName = () => {
    const oldState = { ...this.state };
    oldState.name = '';

    oldState.content = '';

    oldState.newTaskName = '';
    oldState.isModify = false;
    oldState.selectedIndex = '';

    this.setState({ ...oldState });
  };

  modifyTaskType = async () => {
    const thisProps = this.props;
    const thisState = this.state;
    const data = {
      state: cookies.get('state').id,
      name: thisState.name,
      content: thisState.content,
    };
    await thisProps.updateMessage(thisState.isModify, data);
    thisProps.getAllMessage();
    this.resetTaskName();
  };

  deleteTaskTypes = (val) => {
    const thisProps = this.props;
    this.setState({ dOpen: true, deleteID: val.id });
  };

  handleChangeSingle = (value) => {
    this.setState({ selectedForm: value });
  };

  render() {
    const { state, ...thisProps } = this.props;
    const {
      newTaskName,
      isModify,

      name,

      content,

      error,
      dOpen,
      deleteID,
    } = this.state;

    const { allMessage, addMessage } = this.props;

    let AllRows = '';
    if (
      typeof allMessage.data.data !== 'undefined'
      && allMessage.data.data.length > 0
    ) {
      AllRows = allMessage.data.data.map((val) => {
        let active;
        if (val.id === isModify) {
          active = (
            <ExpansionPanels
              type={3}
              remove={() => this.deleteTaskTypes(val)}
              setting={() => this.selectTaskType(val)}
              data={val}
            />
          );
        } else {
          active = (
            <ExpansionPanels
              type={3}
              remove={() => this.deleteTaskTypes(val)}
              setting={() => this.selectTaskType(val)}
              data={val}
            />
          );
        }
        return active;
      });
    }
    let AddModifyButton = '';
    if (isModify) {
      AddModifyButton = (
        <div>
          <Button color="warning" onClick={this.modifyTaskType}>
            Modify
          </Button>
          {'  '}
          <Button color="danger" onClick={this.resetTaskName}>
            Cancel
          </Button>
        </div>
      );
    } else {
      AddModifyButton = (
        <Button color="primary" onClick={this.addTaskType}>
          Create
        </Button>
      );
    }
    const stateName = state.name
      || (cookies.get('state') && cookies.get('state').name)
      || 'No State Selected';

    return (
      <Card
        style={{
          border: '0px solid #c8ced3',
        }}
      >
        <Dialog
          Open={dOpen}
          handleClose={() => {
            this.setState({ dOpen: false });
          }}
          Agree={() => {
            thisProps.deleteMessage(deleteID);
            thisProps.getAllMessage();

            this.setState({ dOpen: false });
          }}
          title="Delete Message Template"
          message=" Do you want to delete this message?"
        />
        <Notification notifor="addMessage" notidata={addMessage} />
        <Notification notifor="allMessage" notidata={allMessage} />
        <Grid container>
          <Grid item xs={12} sm={6}>
            <Card
              className="rounded-0 scroll-sm"
              style={{
                height: '78.5vh',
                overflow: 'auto',
                border: 'none',
                width: '100%',
              }}
            >
              <CardHeader
                style={{
                  textAlign: 'center',
                  padding: '0.75rem 1.25rem',
                  marginBottom: 0,
                  backgroundColor: ' #f0f3f5',
                  borderBottom: '1px solid #c8ced3',
                }}
              >
                {isModify && `Modify  ${newTaskName} Message Template`}
                {!isModify && 'Create Message Template'}
              </CardHeader>
              <CardBody style={this.style.cardBodyStyle}>
                <form autoComplete="off">
                  <Grid container spacing={3}>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        error={error.name}
                        label="Message Name"
                        id="name"
                        type="search"
                        margin="normal"
                        value={name === true ? '' : name}
                        onChange={this.handleChange}
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        disabled
                        defaultValue={stateName}
                        id="State"
                        label="State"
                        type="search"
                        fullWidth
                        value={stateName}
                        onChange={this.handleChange}
                        margin="normal"
                      />
                    </Grid>

                    <Grid item xs={12} sm={12}>
                      <Label htmlFor="textarea-input">Message*</Label>
                      <Input
                        id="content"
                        invalid={!(content.length <= 159)}
                        type="textarea"
                        name="textarea-input"
                        onChange={this.handleChange}
                        value={content}
                        rows="9"
                        placeholder=" Enter Message Content here..."
                      />
                      <span> Total character: {content.length}/159</span>
                    </Grid>
                  </Grid>
                </form>
                {AddModifyButton}
                <div style={{ float: 'right' }}>
                  <IconButton
                    edge="end"
                    aria-label="cached"
                    onClick={() => window.location.reload()}
                  >
                    <Icon style={{ color: ' #17a2b8' }}>cached</Icon>
                  </IconButton>
                </div>
              </CardBody>
            </Card>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Card
              className="rounded-0 scroll-sm"
              style={{
                borderLeft: '1px solid #c8ced3',
                borderRight: '0px solid ',
                borderBottom: '0px solid ',
                borderTop: '0px solid ',
                height: '78.5vh',
                overflow: 'auto',
                width: '100%',
              }}
            >
              <CardHeader
                style={{
                  textAlign: 'center',
                  padding: '0.75rem 1.25rem',
                  marginBottom: 0,
                  backgroundColor: ' #f0f3f5',
                  borderBottom: '1px solid #c8ced3',
                }}
              >
                All Message Template
              </CardHeader>
              <CardBody style={this.style.cardBodyStyle}>{AllRows}</CardBody>
            </Card>
          </Grid>
        </Grid>
      </Card>
    );
  }
}

function mapStateToProps(state) {
  return {
    allMessage: state.taskManagement.allMessage,
    addMessage: state.taskManagement.addMessage,
    state: state.user.state,
  };
}

export default connect(mapStateToProps, {
  createMessage,
  getAllMessage,
  updateMessage,
  deleteMessage,
})(MessageSetting);
