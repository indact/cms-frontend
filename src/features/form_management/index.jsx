import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Card, Row, Modal, ModalHeader, ModalBody,
} from 'reactstrap';
import Fab from '@material-ui/core/Fab';

import AddIcon from '@material-ui/icons/Add';

import FormBrowser from '../../components/form_manager/form_browser';
import { FormEdit, Errors } from '../../components/form_manager/form_builder';

class TaskAssignment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
    };
  }

  toggle = () => {
    this.setState(prevState => ({
      modal: !prevState.modal,
    }));
  };

  render() {
    const thisState = this.state;
    return (
      <>
        <Row>
          <Card style={{ width: '100%', maxheight: ' 600px' }}>
            <FormBrowser />
            <Fab
              aria-label="Add"
              style={{
                position: 'fixed',
                bottom: '40px',
                right: ' 40px',
                backgroundColor: '#538ad6',
                color: '#fff',
              }}
              onClick={this.toggle}
            >
              <AddIcon />
            </Fab>
          </Card>
          <Modal
            isOpen={thisState.modal}
            style={{ maxWidth: '90%' }}
            toggle={this.toggle}
          >
            <ModalHeader toggle={this.toggle}> Create Form</ModalHeader>
            <ModalBody>
              <Card style={{ border: ' 0px solid #c8ced3' }}>
                <FormEdit reset saveText="Create" />
                <Errors
                // message={thisProps.message}
                // type={thisProps.messageType}
                />
              </Card>
            </ModalBody>
          </Modal>
        </Row>
      </>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  {},
)(TaskAssignment);
