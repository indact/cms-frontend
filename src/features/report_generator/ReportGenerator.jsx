import React, { Component } from 'react';
import { connect } from 'react-redux';
import Cookies from 'universal-cookie';
import {
  getAllTasksType,
  getAllTaskStatus,
} from '../../store/action/TaskManagementAction';
import { getPersistentForm } from '../../store/action/FormioAction';
import {
  prepareTaskReportByStatus,
  prepareTaskReportByDGCategory,
} from '../../store/action/ReportGeneratorAction';
import {
  enqueueSnackbar,
  closeSnackbar,
} from '../../store/action/NotificationAction';

const cookies = new Cookies();

class ReportGenerator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reportType: 'status',
      stage_till: false,
      complete: false,
    };
  }

  componentDidMount = async () => {
    const { state, ...actions } = this.props;
    const stateId = (state && state.id) || cookies.get('state').id;
    const stateName = (state && state.name) || cookies.get('state').name;
    await actions.getAllTasksType(stateId);
    await actions.getAllTaskStatus(stateId);
    await actions.getPersistentForm(stateName);
    this.setState({
      state: stateName,
    });
  };

  componentDidUpdate() {}

  getSnapshotBeforeUpdate = async (prevProps) => {
    const {
      state, reportResponse, notifications, ...actions
    } = this.props;
    const stateId = (state && state.id) || cookies.get('state').id;
    const stateName = (state && state.name) || cookies.get('state').name;

    if (
      prevProps.reportResponse !== reportResponse
      && reportResponse.data
      && reportResponse.status !== 'pending'
    ) {
      actions.enqueueSnackbar({
        key: 'reportresponse',
        message: reportResponse.data && reportResponse.data.message,
        options: {
          key: 'reportresponse',
          variant: (reportResponse.status === 200 && 'success') || 'error',
          persist: 'false',
        },
      });
    }

    if (notifications.length > 0) {
      setTimeout(() => {
        actions.closeSnackbar();
      }, 3000);
    }

    if (prevProps.state.name !== state.name) {
      await actions.getAllTasksType(stateId);
      await actions.getPersistentForm(stateName);
      await actions.getAllTaskStatus(stateId);
    }
    return null;
  };

  handleChange = (e) => {
    const { name, value } = e.target;
    switch (e.target.type) {
      case 'checkbox':
        this.setState(prevState => ({
          [name]: !prevState[name],
        }));
        break;
      default:
        this.setState({
          [name]: value,
        });
        break;
    }
  };

  onSubmit = async (e) => {
    e.preventDefault();
    const { ...actions } = this.props;
    const {
      reportType,
      state,
      email,
      stage,
      stage_till,
      status,
      complete,
      category,
    } = this.state;
    const data = { state };
    if (email) data.email = email;
    switch (reportType) {
      case 'category':
        if (stage) data.stage = stage;
        if (complete) data.complete = complete;
        if (category) data.category = category;
        // call action to generate report
        await actions.prepareTaskReportByDGCategory(data);
        break;
      case 'status':
        if (stage && stage_till) {
          data.stage_till = stage;
        } else if (stage) {
          data.stage = stage;
        }
        if (status) data.status = status;
        // call action to generate report
        await actions.prepareTaskReportByStatus(data);
        break;
      default:
        break;
    }
  };

  render() {
    // To Do :: Put up validations
    const { reportType, ...selected } = this.state;
    const {
      state, stages, allStatus, persistentForm,
    } = this.props;
    let options;

    const conditions = !persistentForm
      || !(stages.data && stages.data.length)
      || !(allStatus.data && allStatus.data.length);

    const categories = persistentForm
      && persistentForm.components.filter(
        component => component.key === 'category',
      ).length
      && persistentForm.components.filter(
        component => component.key === 'category',
      )[0].data.values;
    switch (reportType) {
      case 'category':
        options = (
          <div className="form-group">
            <label>Category</label>
            <select
              name="category"
              className="form-control"
              onChange={this.handleChange}
              value={selected.category}
            >
              <option value="">All</option>
              {categories
                && categories.map(category => (
                  <option value={category.value} key={category.value}>
                    {category.label}
                  </option>
                ))}
            </select>
          </div>
        );
        break;
      case 'status':
        options = (
          <div className="form-group">
            <label>Status</label>
            <select
              name="status"
              className="form-control"
              onChange={this.handleChange}
              value={selected.status}
            >
              <option value="">All</option>
              {allStatus.data
                && allStatus.data.map(status => (
                  <option key={status.id}>{status.name}</option>
                ))}
            </select>
          </div>
        );
        break;
      default:
        break;
    }
    return (
      <div className="container">
        <div className="card">
          <div className="card-body">
            <form onSubmit={this.onSubmit}>
              <div className="row">
                <div className="col col-md-6 col-sm-12">
                  <div className="form-group">
                    <label htmlFor="reportType">Report Type</label>
                    <select
                      className="form-control"
                      id="reportType"
                      name="reportType"
                      onChange={this.handleChange}
                      defaultValue="status"
                      disabled
                    >
                      <option value="category">By Beneficiary Category</option>
                      <option value="status">By Task Status</option>
                    </select>
                  </div>
                </div>
                <div className="col col-md-6 col-sm-12">
                  <div className="form-group">
                    <label htmlFor="state">State</label>
                    <input
                      type="text"
                      className="form-control"
                      value={state.name}
                      disabled
                    />
                  </div>
                </div>
                <div className="col col-md-12">
                  <div className="form-group">
                    <label>Email to</label>
                    <input
                      type="email"
                      className="form-control"
                      name="email"
                      placeholder="name@example.com"
                      onChange={this.handleChange}
                    />
                  </div>
                </div>
                <div className="col col-md-6">
                  <div className="form-group">
                    <label>Stage</label>
                    <select
                      name="stage"
                      className="form-control"
                      onChange={this.handleChange}
                    >
                      <option value="">All</option>
                      {stages.data
                        && stages.data.map(stage => (
                          <option key={stage.id}>{stage.name}</option>
                        ))}
                    </select>
                  </div>
                </div>

                {reportType === 'status' ? (
                  <div className="col col-md-6 d-flex align-items-center">
                    <div className="form-check">
                      <input
                        id="stage_till"
                        name="stage_till"
                        type="checkbox"
                        className="form-check-input"
                        onChange={this.handleChange}
                        checked={selected.stage_till}
                        disabled={!selected.stage}
                      />
                      <label className="form-check-label" htmlFor="stage_till">
                        Check this box if data of the previous stages are also
                        required.
                      </label>
                    </div>
                  </div>
                ) : (
                  <div className="col col-md-6 d-flex align-items-center">
                    <div className="form-check">
                      <input
                        id="stage_till"
                        name="complete"
                        type="checkbox"
                        className="form-check-input"
                        onChange={this.handleChange}
                        checked={selected.complete}
                        disabled={!selected.stage}
                      />
                      <label className="form-check-label" htmlFor="complete">
                        Include only beneficiaries that have reached:
                      </label>
                    </div>
                  </div>
                )}
              </div>
              {options}
              <button
                id="reportGeneratorSubmit"
                type="submit"
                className="btn btn-primary"
                disabled={conditions}
              >
                Submit
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  state: state.user.state,
  stages: state.taskManagement.allTasks.data,
  allStatus: state.taskManagement.allTaskStatus.data,
  persistentForm: state.formio.form,
  reportResponse: state.reportGenerator.reportResponse,
  notifications: state.notification.notifications,
});

export default connect(mapStateToProps, {
  getAllTasksType,
  getAllTaskStatus,
  getPersistentForm,
  prepareTaskReportByStatus,
  prepareTaskReportByDGCategory,
  enqueueSnackbar,
  closeSnackbar,
})(ReportGenerator);
