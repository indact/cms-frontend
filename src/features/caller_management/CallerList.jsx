/* eslint-disable react/no-did-update-set-state */
import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Card,
  CardBody,
  CardHeader,
  Table,
  Button,
  Input,
  Col,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  ButtonDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
} from 'reactstrap';
import IsEqual from 'lodash.isequal';

import CallerProfile from '../../components/caller_feature/CallerProfile';
import EnableDisableUser from '../../components/caller_feature/EnableDisableUser';

import {
  getAllCallers,
  getAllCallersPage,
  changeUserState,
} from '../../store/action/CallerManagementAction';
import { getAllFeatures } from '../../store/action/UserManagementAction';
import { callerTaskCount } from '../../store/action/Dashboard';
import Notification from '../notifications';

// eslint-disable-next-line react/prefer-stateless-function
class CallerList extends Component {
  style = {
    row: {
      cursor: 'pointer',
    },
    search: {
      float: 'right',
    },
    cardBodyStyle: {
      height: '500px',
      overflow: 'auto',
      padding: '0',
    },
  };

  constructor(props) {
    super(props);
    this.state = {
      popup: false,
      userid: '',
      selectAll: false,
      showButtons: false,
      callersData: [],
      showDropdown: false,
      searchBy: 'Name',
      searchByText: '',
      callerFeature: '',
    };
  }

  componentDidMount = () => {
    const thisprops = this.props;
    // const parameter = [];
    thisprops.getAllFeatures();
    // setTimeout(
    // thisprops.features.data.forEach((value, index) => {
    //   if (value.name === 'Caller Management') {
    //     this.setState({ callerFeature: value.id });
    //     parameter.push(`features=${value.id}`);
    //   }
    // });
    // , 9000,
    // );
    // setTimeout(
    // thisprops.getAllCallers(thisprops.token, parameter.join('&'));
    // , 10000,
    // );
  };

  loadMoreItems = () => {
    const thisProps = this.props;
    if (thisProps.callers.data.links.next !== null) {
      const params = thisProps.callers.data.links.next.split('?')[1].split('&');
      params.forEach((value, index) => {
        if (value.split('=')[0] === 'states') {
          delete params[index];
        }
      });
      thisProps.getAllCallersPage('', params.join('&'));
    }
  };

  componentDidUpdate = (prevProps) => {
    const { callers, features, state } = this.props;
    const thisProps = this.props;
    if (prevProps.state !== state) {
      const parameter = [];
      features.data.forEach((value) => {
        if (value.name === 'Caller Management') {
          this.setState({ callerFeature: value.id });
          parameter.push(`features=${value.id}`);
        }
      });
      if (parameter.length > 0) thisProps.getAllCallers('', parameter.join('&'), state.id);
      else thisProps.getAllCallers('', '', state.id);
    }
    if (!IsEqual(prevProps.features, features)) {
      const parameter = [];
      features.data.forEach((value) => {
        if (value.name === 'Caller Management') {
          this.setState({ callerFeature: value.id });
          parameter.push(`features=${value.id}`);
        }
      });
      if (parameter.length > 0) thisProps.getAllCallers('', parameter.join('&'), state.id);
    }

    if (prevProps.callers.data !== callers.data) {
      if (callers.status === 200 && typeof callers.data.data !== 'undefined') {
        callers.data.data.forEach((v, i) => {
          callers.data.data[i].isChecked = false;
        });
        if (typeof callers.data.type !== 'undefined') {
          this.setState((prevState) => {
            const allData = prevState.callersData;
            allData.push(...callers.data.data);
            return { callersData: allData };
          });
        } else {
          this.setState({ callersData: callers.data.data });
        }
      }
    }
  };

  enableUser = (status, id) => {
    const thisprops = this.props;
    const data = {
      id: [id],
      deleted: status,
    };
    thisprops.changeUserState(thisprops.token, data);
    this.setState((prevState) => {
      const callerData = [];
      prevState.callersData.forEach((item) => {
        if (item.id === id) {
          callerData.push({ ...item, deleted: status });
        } else {
          callerData.push(item);
        }
      });
      return { callersData: callerData };
    });
  };

  showCallerProfile = (data) => {
    this.setState(prevState => ({ popup: !prevState.popup, userid: data }));
  };

  toggleCallerProfile = () => {
    this.setState(prevState => ({ popup: !prevState.popup }));
  };

  selectAll = () => {
    this.setState(prevState => ({ selectAll: !prevState.selectAll }));
  };

  enableAll = (status) => {
    const thisProps = this.props;
    const { callersData } = this.state;
    const dataToChange = [];
    callersData.forEach((item) => {
      if (item.isChecked) {
        dataToChange.push(item.id);
      }
    });
    if (dataToChange.length > 0) {
      const data = {
        id: dataToChange,
        deleted: status,
      };
      thisProps.changeUserState(thisProps.changeUserState, data);
      this.setState((prevState) => {
        const callerData = [];
        prevState.callersData.forEach((item) => {
          if (item.isChecked) {
            callerData.push({ ...item, deleted: status });
          } else {
            callerData.push(item);
          }
        });
        return { callersData: callerData };
      });
    }
  };

  handleChange = (e) => {
    const { name, checked } = e.target;
    const itemName = name;
    const checked1 = checked;
    let showButtons = false;
    this.setState((prevState) => {
      let { callersData, selectAll } = prevState;
      if (itemName === 'checkAll') {
        selectAll = checked1;
        callersData = callersData.map(item => ({
          ...item,
          isChecked: checked1,
        }));
      } else {
        callersData = callersData.map(item => (item.name === itemName
            ? {
              ...item,
              isChecked: checked1,
            }
            : item));
        selectAll = callersData.every(item => item.isChecked);
      }
      const data = callersData.filter(item => item.isChecked);
      if (data.length > 0) {
        showButtons = true;
      } else {
        showButtons = false;
      }
      return { callersData, selectAll, showButtons };
    });
  };

  searchUsers = (e) => {
    const { state } = this.props;
    this.setState({ searchByText: e.target.value });
    // if (e.keyCode === 13) {
    const parameter = [];
    const { searchBy, callerFeature } = this.state;
    if (callerFeature !== '') parameter.push(`features=${callerFeature}`);
    const thisprops = this.props;
    if (e.target.value !== '') {
      if (searchBy === 'Name') parameter.push(`name=${e.target.value}`);
      if (searchBy === 'Phone') parameter.push(`mobile=${e.target.value}`);
      if (searchBy === 'Email') parameter.push(`email=${e.target.value}`);
    }
    if (searchBy !== 'Search By') thisprops.getAllCallers('', parameter.join('&'), state.id);
    // }
  };

  searchUsersBy = (by) => {
    const { state } = this.props;
    this.setState({ searchBy: by });
    const parameter = [];
    const thisprops = this.props;
    const { searchByText, callerFeature } = this.state;
    if (callerFeature !== '') parameter.push(`features=${callerFeature}`);
    if (searchByText !== '') {
      if (by === 'Name') parameter.push(`name=${searchByText}`);
      if (by === 'Phone') parameter.push(`mobile=${searchByText}`);
      if (by === 'Email') parameter.push(`email=${searchByText}`);
    }
    if (by !== 'Search By') thisprops.getAllCallers('', parameter.join('&'), state.id);
  };

  searchUsersByED = (by) => {
    const { state } = this.props;
    this.setState({ searchByText: '' });
    this.setState({ searchBy: 'Search By' });
    const { callerFeature } = this.state;
    const thisprops = this.props;
    const parameter = [];
    if (callerFeature !== '') parameter.push(`features=${callerFeature}`);
    if (by === 'Disabled') parameter.push('deleted=true');
    if (by === 'Enabled') parameter.push('deleted=false');
    thisprops.getAllCallers('', parameter.join('&'), state.id);
  };

  handleScroll = (e) => {
    if (
      e.target.scrollHeight - Math.floor(e.target.scrollTop)
      === e.target.clientHeight
    ) {
      this.loadMoreItems();
    }
  };

  render() {
    const {
      popup,
      userid,
      callersData,
      selectAll,
      showButtons,
      showDropdown,
      searchBy,
      searchByText,
    } = this.state;
    const thisProps = this.props;
    const allUsers = callersData.map((value, index) => (
      <tr key={value.id}>
        {/* <tr key={value.id} style={this.style.row} onClick={
          () => this.showCallerProfile(value.id)
          }> */}
        <td>
          <input
            type="checkbox"
            name={value.name}
            checked={value.isChecked}
            onChange={this.handleChange}
            className="chkBx"
            id={`chk${value.id}`}
          />
        </td>
        <td>{value.name}</td>
        <td>{value.email}</td>
        <td>{value.mobile}</td>
        <td>
          <Button
            data-test="caller_profile"
            color="primary"
            size="sm"
            block
            onClick={() => this.showCallerProfile(value.id)}
          >
            Caller Profile
          </Button>
        </td>
        <td>
          <EnableDisableUser
            valueData={value}
            enabled={value.deleted}
            handleClick={this.enableUser}
          />
        </td>
      </tr>
    ));
    return (
      <>
        <Notification notifor="callersdata" notidata={thisProps.callers} />
        <Card>
          <CardHeader>
            Callers List
            <Col
              sm={{ offset: 6 }}
              className="form-inline"
              style={this.style.search}
            >
              <FormGroup row>
                <Col md="12">
                  <InputGroup>
                    <Input
                      data-test="Searching"
                      type="text"
                      onChange={this.searchUsers}
                      placeholder="Search By"
                      value={searchByText}
                    />
                    <InputGroupAddon addonType="append">
                      <ButtonDropdown
                        isOpen={showDropdown}
                        toggle={() => {
                          this.setState({ showDropdown: !showDropdown });
                        }}
                      >
                        <DropdownToggle
                          caret
                          color="primary"
                          data-test="Search_test"
                        >
                          {searchBy}
                        </DropdownToggle>
                        <DropdownMenu
                          data-test="srchmenu_test"
                          className={showDropdown ? 'show' : ''}
                        >
                          <DropdownItem
                            onClick={() => this.searchUsersBy('Name')}
                          >
                            Name
                          </DropdownItem>
                          <DropdownItem
                            onClick={() => this.searchUsersBy('Phone')}
                          >
                            Phone
                          </DropdownItem>
                          <DropdownItem
                            onClick={() => this.searchUsersBy('Email')}
                          >
                            Email
                          </DropdownItem>
                          <DropdownItem divider />
                          <DropdownItem
                            onClick={() => this.searchUsersByED('Disabled')}
                          >
                            Disabled Users
                          </DropdownItem>
                          <DropdownItem
                            onClick={() => this.searchUsersByED('Enabled')}
                          >
                            Enabled Users
                          </DropdownItem>
                        </DropdownMenu>
                      </ButtonDropdown>
                    </InputGroupAddon>
                  </InputGroup>
                </Col>
              </FormGroup>
              {/* <FormGroup className="pr-1">
              <Input placeholder="Search By Name" onKeyUp={this.searchUsers} />
            </FormGroup> */}
              {showButtons && (
                <div>
                  <Button
                    data-test="enable_test"
                    color="primary"
                    size="sm"
                    onClick={() => this.enableAll(false)}
                  >
                    Enable
                  </Button>
                  <Button
                    data-test="disable_test"
                    color="danger"
                    size="sm"
                    onClick={() => this.enableAll(true)}
                  >
                    Disable
                  </Button>
                </div>
              )}
            </Col>
          </CardHeader>
          <CardBody
            style={this.style.cardBodyStyle}
            onScroll={this.handleScroll}
          >
            <CallerProfile
              toggleProfile={this.toggleCallerProfile}
              isOpen={popup}
              userId={userid}
              id={1}
            />
            <Table responsive data-test="table_test">
              <thead>
                <tr>
                  <th>
                    <input
                      type="checkbox"
                      onChange={this.handleChange}
                      checked={selectAll}
                      name="checkAll"
                    />
                  </th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone number</th>
                  <th>Caller Profile</th>
                  <th>Enable/Disable User</th>
                </tr>
              </thead>
              <tbody>{allUsers}</tbody>
            </Table>
          </CardBody>
        </Card>
      </>
    );
  }
}

function mapStateToProps(state) {
  return {
    callers: state.callerManagement.callers,
    token: state.auth.token,
    features: state.userManagement.features,
    state: state.user.state,
  };
}

export default connect(mapStateToProps, {
  getAllCallers,
  getAllFeatures,
  changeUserState,
  getAllCallersPage,
  callerTaskCount,
})(CallerList);
