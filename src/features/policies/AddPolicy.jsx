import React from 'react';
import { Link } from 'react-router-dom';
import Forms from '../../components/base/forms/Forms';

const AddPolicy = () => (
  <div>
    <h4>
      <i className="icon-arrow-left policyaddicon" />
      <Link to="/admin/Policies">{'  '}Back</Link>
    </h4>
    <Forms />
  </div>
);

export default AddPolicy;
