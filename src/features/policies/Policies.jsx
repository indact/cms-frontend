import React from 'react';
import { Link } from 'react-router-dom';
import { Col, Row } from 'reactstrap';
import PolicyStates from '../../components/state_table/States';
import usersData from '../../components/state_table/UsersData';

const Policies = () => (
  <div>
    <Row>
      <Col xs="12" md="12" xl="12">
        <h4>
          <i className="icon-plus policyaddicon" />
          <Link to="/admin/AddPolicy">{'  '}Add New Policy</Link>
        </h4>
      </Col>
    </Row>
    <Row>
      <Col xs="12" md="6" xl="6">
        {' '}
        <PolicyStates
          header="Right To Education"
          tooltipno={0}
          tooltipcontent="Add New State"
          item={usersData}
        />
      </Col>
      <Col xs="12" md="6" xl="6">
        {' '}
        <PolicyStates
          header="RTX"
          tooltipno={1}
          tooltipcontent="Add New State"
          item={usersData}
        />
      </Col>
    </Row>
  </div>
);

export default Policies;
