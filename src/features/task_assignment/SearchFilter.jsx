import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Form,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';
import { makeStyles } from '@material-ui/core/styles';

import FormControl from '@material-ui/core/FormControl';
import MInput from '@material-ui/core/Input';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
// import Chip from '@material-ui/core/Chip';
import TextField from '@material-ui/core/TextField';
import SingleSelect from '../../components/select_input/single_select/SingleSelect';

class SearchFilter extends Component {
  toggleLarge = () => {
    const thisProps = this.props;
    thisProps.toggleFilter();
  };

  handleChanges = (event) => {
    const thisProps = this.props;
    let data;
    if (event.target.name === 'tasktype') {
      thisProps.allTasksType.forEach((val) => {
        if (val.id === event.target.value) data = val.name;
      });
    }
    if (event.target.name === 'taskstatus') {
      thisProps.allTaskStatus.forEach((val) => {
        if (val.id === event.target.value) data = val.name;
      });
    }
    thisProps.selectChanges(event.target.value, event.target.name, data);
  };

  handleChangeSingle = (value) => {
    const thisProps = this.props;
    thisProps.selectChanges(value.value, 'assignee', value.label);
  };

  render() {
    const classes = makeStyles(theme => ({
      root: {
        display: 'flex',
        flexWrap: 'wrap',
      },
      formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        maxWidth: 300,
        width: '100%',
      },
      chips: {
        display: 'flex',
        flexWrap: 'wrap',
      },
      chip: {
        margin: 2,
      },
      noLabel: {
        marginTop: theme.spacing(3),
      },
      textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
      },
    }));
    const {
      isOpen,
      allTaskStatus,
      allTasksType,
      allAssignee,
      selectedValues,
      selectChanges,
    } = this.props;
    const allTypes = [
      <MenuItem key="1" value="">
        <em>None</em>
      </MenuItem>,
    ];
    if (typeof allTasksType !== 'undefined') {
      allTasksType.forEach((val) => {
        allTypes.push(
          <MenuItem key={val.id} value={val.id}>
            {val.name}
          </MenuItem>,
        );
      });
    }
    const allStatus = [
      <MenuItem key="1" value="">
        <em>None</em>
      </MenuItem>,
    ];
    if (typeof allTaskStatus !== 'undefined') {
      allTaskStatus.forEach((val) => {
        allStatus.push(
          <MenuItem key={val.id} value={val.id}>
            {val.name}
          </MenuItem>,
        );
      });
    }
    const allCallers = [
      { label: 'None', value: '' },
      { label: 'Unassigned', value: 'null' },
    ];
    allAssignee.forEach((val) => {
      allCallers.push({ label: val.name, value: val.id });
    });
    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
      PaperProps: {
        style: {
          maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
          width: 250,
        },
      },
    };
    return (
      <Modal
        isOpen={isOpen}
        toggle={this.toggleLarge}
        className="modal-info modal-lg"
      >
        <ModalHeader toggle={this.toggleLarge}>Apply Filter</ModalHeader>
        <ModalBody>
          <Form
            style={{
              display: 'flex',
              flexFlow: 'column',
            }}
          >
            <FormControl style={{ margin: '10px' }}>
              <TextField
                id="standard-with-placeholder"
                label="Beneficiary Mobile Number"
                placeholder="Mobile Number"
                name="beneficiary"
                onChange={this.handleChanges}
                value={selectedValues.beneficiary}
                className={classes.textField}
              />
            </FormControl>
            <FormControl style={{ margin: '10px' }}>
              <SingleSelect
                label="Assignee"
                placeholder="Assignee"
                suggestions={allCallers}
                single={{
                  label: selectedValues.assignee,
                  value: selectedValues.assigneeID,
                }}
                handleChangeSingle={this.handleChangeSingle}
              />
              {/* <TextField
                id="standard-with-placeholder"
                label="Assignee"
                placeholder="Enter your Assignee"
                className={classes.textField}
              /> */}
            </FormControl>
            <FormControl style={{ margin: '10px' }}>
              <InputLabel htmlFor="tasktype-select">Task Type</InputLabel>
              <Select
                value={selectedValues.taskTypeID}
                onChange={this.handleChanges}
                input={<MInput name="tasktype" id="tasktype-select" />}
                MenuProps={MenuProps}
              >
                {allTypes}
              </Select>
            </FormControl>
            <FormControl style={{ margin: '10px' }}>
              <InputLabel htmlFor="taskstatus-select">Task Status</InputLabel>
              <Select
                value={selectedValues.taskStatusID}
                onChange={this.handleChanges}
                input={<MInput name="taskstatus" id="taskstatus-select" />}
                MenuProps={MenuProps}
              >
                {allStatus}
              </Select>
              {/* <Select
                multiple
                value={selectedValues.taskStatus}
                onChange={this.handleChanges}
                input={<MInput id="select-multiple-chip" name="taskstatus" />}
                renderValue={selected => (
                  <div className={classes.chips}>
                    {selected.map(value => (
                      <Chip
                        key={value}
                        label={value}
                        className={classes.chip}
                      />
                    ))}
                  </div>
                )}
                MenuProps={MenuProps}
              >
                {allStatus}
              </Select> */}
            </FormControl>
            {/* <FormControl style={{ margin: '10px' }}>
              <TextField
                id="standard-with-placeholder"
                label="Category"
                placeholder="Category"
                className={classes.textField}
              />
            </FormControl> */}
          </Form>
        </ModalBody>
        <ModalFooter>
          {/* <Button color="primary" onClick={this.toggleLarge}>
            Do Something
          </Button>{' '} */}
          <Button
            color="primary"
            data-test="user_cancel"
            onClick={this.toggleLarge}
          >
            Done
          </Button>
          <Button
            color="primary"
            data-test="user_cancel"
            onClick={() => selectChanges('', 'Reset', '')}
          >
            Reset
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}
function mapStateToProps() {
  return {
    // allTasksType: state.taskManagement.allTasks,
    // allTaskStatus: state.taskManagement.allTaskStatus,
  };
}

export default connect(mapStateToProps, {})(SearchFilter);
