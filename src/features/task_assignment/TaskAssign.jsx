import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Button, Modal, ModalBody, ModalFooter, ModalHeader,
} from 'reactstrap';
import FormControl from '@material-ui/core/FormControl';
import _ from 'lodash';
import TextField from '@material-ui/core/TextField';
import {
  getAllTasksAssigned,
  BulkAssign,
} from '../../store/action/TaskAssignmentAction';
import DefaultTable from './DefaultTable';
import MultipleSelect from '../../components/select_input/single_select/MultipleSelect';
import Notification from '../notifications';

class TaskAssign extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allData: [],
      allSelectedData: [],
      selectedAssignee: [],
      select: 0,
    };
  }

  componentWillReceiveProps = (nextProps) => {
    const thisProps = this.props;
    if (!_.isEqual(nextProps.allTasks, thisProps.allTasks)) {
      const allRows = [];
      if (
        typeof nextProps.allTasks !== 'undefined'
        && nextProps.allTasks.length > 0
      ) {
        nextProps.allTasks.forEach((val) => {
          if (val.task_status.category !== 'Done') {
            const rowdata = {};
            rowdata.taskId = val.id;
            rowdata.beneficiary_name = `${val.beneficiary.mobile} | ${val.task_type.name} | ${val.task_status.name}`;
            rowdata.assignee = val.assignee.id;
            rowdata.taskTypeId = val.task_type.id;
            rowdata.startDate = val.startDate;
            rowdata.dueData = val.dueData;
            rowdata.taskType = val.task_type.name;
            rowdata.taskStatus = val.task_status.name;

            if (typeof val.task_type.form.formId !== 'undefined') {
              rowdata.taskFormId = val.task_type.form.formId;
            } else {
              rowdata.taskFormId = null;
            }
            if (typeof val.submission.id !== 'undefined') {
              rowdata.taskSubId = val.submission.id;
            } else {
              rowdata.taskSubId = null;
            }
            allRows.push(rowdata);
          }
        });

        this.setState({ allData: [], allSelectedData: allRows });
      } else {
        this.setState({ allData: [], allSelectedData: [] });
      }
    }
    if (!_.isEqual(nextProps.selectedValues, thisProps.selectedValues)) {
      const q = [];
      if (nextProps.selectedValues.taskTypeID) q.push(`taskTypeID=${nextProps.selectedValues.taskTypeID}`);
      if (nextProps.selectedValues.taskStatusID) {
        q.push(`taskStatusID=${nextProps.selectedValues.taskStatusID}`);
      }
      if (nextProps.selectedValues.beneficiary) q.push(`beneficiaryMobile=${nextProps.selectedValues.beneficiary}`);
      if (nextProps.selectedValues.assigneeID) q.push(`assigneeID=${nextProps.selectedValues.assigneeID}`);
      q.push('page_size=0');
      thisProps.getAllTasksAssigned(q.join('&'));
    }
  };

  AssignTasks = () => {
    const thisProps = this.props;
    const thisState = this.state;
    const send = {
      assignee: [],
      Task: [],
    };
    thisState.allSelectedData.forEach((val) => {
      send.Task.push(val.taskId);
    });
    if (
      thisState.selectedAssignee.filter(name => name.value === 'all').length
      === 1
    ) {
      thisProps.allAssignee.forEach((val) => {
        send.assignee.push(val.id);
      });
    } else {
      thisState.selectedAssignee.forEach((val) => {
        send.assignee.push(val.value);
      });
    }
    thisProps.BulkAssign(send);
    this.setState({ selectedAssignee: [] });
    thisProps.AfterAssign();
  };

  toggleLarge = () => {
    const thisProps = this.props;
    thisProps.toggleFilter();
  };

  handleChangeMulti = (data) => {
    this.setState({ selectedAssignee: data });
  };

  selectAssignBene = (data) => {
    this.setState((prevState) => {
      const oldData = prevState.allData;
      const oldsData = prevState.allSelectedData;
      prevState.allData.forEach((val, ind) => {
        if (val.taskId === data.rowData.taskId) {
          oldsData.unshift(val);
          oldData.splice(ind, 1);
        }
      });
      return { allData: oldData, allSelectedData: oldsData };
    });
  };

  unselectAssignBene = (data) => {
    this.setState((prevState) => {
      const oldData = prevState.allData;
      const oldsData = prevState.allSelectedData;
      prevState.allSelectedData.forEach((val, ind) => {
        if (val.taskId === data.rowData.taskId) {
          oldData.unshift(val);
          oldsData.splice(ind, 1);
        }
      });

      return { allData: oldData, allSelectedData: oldsData };
    });
  };

  handleChange = (e) => {
    if (e.target.name === 'toSelect') {
      this.setState({
        select: e.target.value,
      });
    }
  };

  handleFocus = (e) => {
    e.target.select();
  };

  beneFilter = () => {
    const { allData, allSelectedData, select } = this.state;
    const tempAllData = [...allData];
    const tempSelectedData = [...allSelectedData];
    if (select > 0) {
      tempSelectedData.push(...tempAllData.splice(0, select));
    }
    this.setState({
      allData: tempAllData,
      allSelectedData: tempSelectedData,
    });
  };

  selectAll = () => {
    const { allData, allSelectedData } = this.state;
    const tempAllData = [...allData];
    const tempSelectedData = [...allSelectedData];
    tempSelectedData.push(...tempAllData.splice(0));
    this.setState({
      allData: tempAllData,
      allSelectedData: tempSelectedData,
    });
  };

  deselectAll = () => {
    const { allData, allSelectedData } = this.state;
    const tempAllData = [...allData];
    const tempSelectedData = [...allSelectedData];
    tempAllData.push(...tempSelectedData.splice(0));
    this.setState({
      allData: tempAllData,
      allSelectedData: tempSelectedData,
    });
  };

  render() {
    const { isOpen, allAssignee, bulkassignInfo } = this.props;
    const {
      allData, allSelectedData, selectedAssignee, select,
    } = this.state;
    const allCallers = [{ label: 'Assign To All', value: 'all' }];
    allAssignee.forEach((val) => {
      allCallers.push({ label: val.name, value: val.id });
    });
    return (
      <>
        <Notification notifor="assigntask" notidata={bulkassignInfo} />
        <Modal
          isOpen={isOpen}
          toggle={this.toggleLarge}
          className="modal-info modal-lg"
        >
          <ModalHeader toggle={this.toggleLarge}>Assign Task</ModalHeader>
          <ModalBody>
            <FormControl style={{ margin: '10px' }}>
              <MultipleSelect
                label="Assignee"
                placeholder="Assignee"
                suggestions={allCallers}
                multi={selectedAssignee}
                handleChangeMulti={this.handleChangeMulti}
              />
              <div className="d-flex align-self-center justify-content-between mb-3">
                <TextField
                  style={{ width: '30%' }}
                  className="flex-fill"
                  id="standard-number"
                  label="Select"
                  name="toSelect"
                  placeholder="Task Count"
                  type="number"
                  value={select}
                  onChange={this.handleChange}
                  onFocus={this.handleFocus}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                <Button
                  color="primary"
                  className="btn-sm align-self-end"
                  style={{ height: '30px', width: '60px', marginTop: '5px' }}
                  onClick={this.beneFilter}
                >
                  Select
                </Button>
              </div>
            </FormControl>
            <div className="row">
              <div style={{ width: '45%', marginLeft: '1%' }}>
                <div
                  style={{
                    fontWeight: 'bold',
                    display: 'flex',
                    justifyContent: 'space-between',
                  }}
                >
                  <p className="text-right text-primary">
                    Count: {allData.length}
                  </p>
                  <button
                    type="button"
                    className="btn btn-primary btn-sm"
                    style={{ height: '30px' }}
                    onClick={this.selectAll}
                  >
                    <p>Select All</p>
                  </button>
                </div>
                <DefaultTable
                  css={{ height: 250 }}
                  rows={allData}
                  onRowClick={this.selectAssignBene}
                  label="Beneficiary"
                />
              </div>
              <div style={{ width: '45%', marginLeft: '8%' }}>
                <div
                  style={{
                    fontWeight: 'bold',
                    display: 'flex',
                    justifyContent: 'space-between',
                  }}
                >
                  <button
                    type="button"
                    className="btn btn-primary btn-sm"
                    style={{ height: '30px' }}
                    onClick={this.deselectAll}
                  >
                    <p>Deselect All</p>
                  </button>
                  <p className="text-right text-primary">
                    Count: {allSelectedData.length}
                  </p>
                </div>
                <DefaultTable
                  css={{ height: 250 }}
                  rows={allSelectedData}
                  onRowClick={this.unselectAssignBene}
                  label="Seleted Beneficiary"
                />
              </div>
            </div>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.AssignTasks}>
              Assign
            </Button>
          </ModalFooter>
        </Modal>
      </>
    );
  }
}
function mapStateToProps(state) {
  return {
    allTasks: state.taskAssignment.allTasks.data.data,
    allTasksType: state.taskManagement.allTasks.data.data,
    bulkassignInfo: state.taskAssignment.bulkassign,
  };
}

export default connect(mapStateToProps, {
  BulkAssign,
  getAllTasksAssigned,
})(TaskAssign);
