import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import MaterialTable from 'material-table';
import {
  getAllTasksAssigned,
  UpdateTask,
} from '../../store/action/TaskAssignmentAction';
import Notification from '../notifications';
import StaticNotification from '../notifications/static';

require('../../assets/css/tasklist.css');

class TaskList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [],
      alldata: [],
      showNotiMsg: false,
      msgId: '',
      variant: 'info',
    };
  }

  componentDidMount = () => {
    const {
      state, allTasks, taskAssignmentState, ...actions
    } = this.props;
    if (allTasks.status === 200 && taskAssignmentState === state.id) {
      const allRows = [];
      if (
        typeof allTasks.data.data !== 'undefined'
        && allTasks.data.data.length > 0
      ) {
        allTasks.data.data.forEach((val) => {
          const rowdata = {};
          rowdata.beneficiary_name = val.beneficiary.mobile;
          rowdata.taskId = val.id;
          rowdata.assignee = val.assignee.id;
          rowdata.taskTypeId = val.task_type.id;
          rowdata.taskType = val.task_type.id;
          rowdata.taskStatus = val.task_status.id;
          const created_at = new Date(val.created_at);
          const updated_at = new Date(val.updated_at);
          rowdata.created_at = created_at.toLocaleString() || '';
          rowdata.updated_at = updated_at.toLocaleString() || '';

          if (typeof val.task_type.form.formId !== 'undefined') {
            rowdata.taskFormId = val.task_type.form.formId;
          } else {
            rowdata.taskFormId = null;
          }
          if (typeof val.beneficiary.form.pFormID !== 'undefined') {
            rowdata.taskpFormId = val.beneficiary.form.pFormID;
          } else {
            rowdata.taskpFormId = null;
          }
          if (typeof val.submission.id !== 'undefined') {
            rowdata.taskSubId = val.submission.id;
          } else {
            rowdata.taskSubId = null;
          }
          if (typeof val.beneficiary.form.pSubmissionId !== 'undefined') {
            rowdata.taskPSubId = val.beneficiary.form.pSubmissionId;
          } else {
            rowdata.taskPSubId = null;
          }
          allRows.push(rowdata);
        });
        this.setState({ alldata: allRows });
      } else {
        this.setState({ alldata: [] });
      }
    } else {
      actions.getAllTasksAssigned('page_size=0', state.id);
    }
  };

  componentDidUpdate = (prevProps) => {
    const {
      state,
      allTasksType,
      allTaskStatus,
      allAssignee,
      selectedValues,
      ...actions
    } = this.props;
    if (
      !_.isEqual(prevProps.allTasksType, allTasksType)
      || !_.isEqual(prevProps.allTaskStatus, allTaskStatus)
      || !_.isEqual(prevProps.allAssignee, allAssignee)
    ) {
      const taskstatus = {};
      const tasktype = {};
      const assignee = {};
      if (typeof allTasksType !== 'undefined') {
        allTasksType.forEach((val) => {
          tasktype[val.id] = val.name;
        });
      }
      if (typeof allTaskStatus !== 'undefined') {
        allTaskStatus.forEach((val) => {
          taskstatus[val.id] = val.name;
        });
      }
      if (typeof allAssignee !== 'undefined') {
        allAssignee.forEach((val) => {
          assignee[val.id] = val.name;
        });
      }
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        columns: [
          {
            title: 'Beneficiary',
            field: 'beneficiary_name',
            editable: 'onAdd',
          },
          {
            title: 'Assignee',
            field: 'assignee',
            lookup: assignee,
          },
          {
            title: 'Task Type',
            field: 'taskType',
            lookup: tasktype,
          },
          {
            title: 'Task Status',
            field: 'taskStatus',
            lookup: taskstatus,
          },
          {
            title: 'Created_At ',
            field: 'created_at',
          },
          {
            title: 'Updated_At ',
            field: 'updated_at',
          },
          {
            title: 'Task type id',
            field: 'taskTypeId',
            hidden: true,
          },
          {
            title: 'Task form id',
            field: 'taskFormId',
            hidden: true,
          },
          {
            title: 'Task pform id',
            field: 'taskPFormId',
            hidden: true,
          },
          {
            title: 'Task submission id',
            field: 'taskSubId',
            hidden: true,
          },
          {
            title: 'Task Psubmission id',
            field: 'taskPSubId',
            hidden: true,
          },
          {
            title: 'Task id',
            field: 'taskId',
            hidden: true,
          },
        ],
      });
    }
    if (
      !_.isEqual(prevProps.selectedValues, selectedValues)
      || !_.isEqual(prevProps.state, state)
    ) {
      const q = [];
      if (selectedValues.taskTypeID) q.push(`taskTypeID=${selectedValues.taskTypeID}`);
      if (selectedValues.taskStatusID) {
        q.push(`taskStatusID=${selectedValues.taskStatusID}`);
      }
      if (selectedValues.beneficiary) q.push(`beneficiaryMobile=${selectedValues.beneficiary}`);
      if (selectedValues.assigneeID) q.push(`assigneeID=${selectedValues.assigneeID}`);
      q.push('page_size=0');
      actions.getAllTasksAssigned(q.join('&'), state.id);
    }
  };

  componentWillReceiveProps = (nextProps) => {
    const { allTasks } = this.props;
    if (!_.isEqual(nextProps.allTasks, allTasks)) {
      const allRows = [];
      if (
        typeof nextProps.allTasks.data.data !== 'undefined'
        && nextProps.allTasks.data.data.length > 0
      ) {
        nextProps.allTasks.data.data.forEach((val) => {
          const rowdata = {};
          rowdata.beneficiary_name = val.beneficiary.mobile;
          rowdata.taskId = val.id;
          rowdata.assignee = val.assignee.id;
          rowdata.taskTypeId = val.task_type.id;
          rowdata.taskType = val.task_type.id;
          rowdata.taskStatus = val.task_status.id;
          const created_at = new Date(val.created_at);
          const updated_at = new Date(val.updated_at);
          rowdata.created_at = created_at.toLocaleString() || '';
          rowdata.updated_at = updated_at.toLocaleString() || '';
          if (typeof val.task_type.form.formId !== 'undefined') {
            rowdata.taskFormId = val.task_type.form.formId;
          } else {
            rowdata.taskFormId = null;
          }
          if (typeof val.beneficiary.form.pFormID !== 'undefined') {
            rowdata.taskpFormId = val.beneficiary.form.pFormID;
          } else {
            rowdata.taskpFormId = null;
          }
          if (typeof val.submission.id !== 'undefined') {
            rowdata.taskSubId = val.submission.id;
          } else {
            rowdata.taskSubId = null;
          }
          if (typeof val.beneficiary.form.pSubmissionId !== 'undefined') {
            rowdata.taskPSubId = val.beneficiary.form.pSubmissionId;
          } else {
            rowdata.taskPSubId = null;
          }
          allRows.push(rowdata);
        });
        this.setState({ alldata: allRows });
      } else {
        this.setState({ alldata: [] });
      }
    }
  };

  openForm = (formname, subid) => {
    const thisProps = this.props;
    thisProps.openForm(formname, subid);
  };

  render() {
    const {
      columns, alldata, showNotiMsg, msgId, variant,
    } = this.state;
    const thisProps = this.props;
    let snoti = '';
    if (showNotiMsg !== false) {
      snoti = (
        <StaticNotification
          id={msgId}
          notidata={thisProps.notiData}
          notifor="taskliststatic"
          variant={variant}
          message={showNotiMsg}
        />
      );
      this.setState({ showNotiMsg: false });
    }
    return (
      <div>
        <Notification
          notidata={thisProps.notiData}
          notifor={thisProps.tasklistnoti}
        />
        <Notification
          notidata={thisProps.updatetaskInfo}
          notifor="taskupdate"
        />
        {snoti}
        <MaterialTable
          title="List of Tasks"
          columns={columns}
          data={alldata}
          options={{
            pageSize: 15,
            pageSizeOptions: [0],
            actionsColumnIndex: -1,
            padding: 'dense',
            exportButton: true,
          }}
          onRowClick={(event, data) => {
            if (data.taskFormId) {
              thisProps.openForm(
                data.taskId,
                data.taskFormId,
                data.taskpFormId,
                data.taskSubId,
                data.taskPSubId,
                data.taskStatus,
              );
            } else {
              this.setState({
                showNotiMsg: 'No Form Found',
                msgId: data.taskId,
                variant: 'warning',
              });
            }
          }}
          actions={[
            {
              icon: 'comment',
              tooltip: 'Send SMS',
              onClick: (event, data) => {
                thisProps.openSms(data);
              },
            },
          ]}
          editable={{
            onRowUpdate: (newData, oldData) => new Promise((resolve) => {
              const data = [...alldata];
              data[data.indexOf(oldData)] = newData;
              this.setState({ alldata: data });
              const send = {};
              send.id = newData.taskId;
              if (oldData.assignee !== newData.assignee) {
                send.assignee = newData.assignee;
              }
              if (oldData.taskStatus !== newData.taskStatus) {
                send.status = newData.taskStatus;
              }
              if (oldData.taskType !== newData.taskType) {
                send.type = newData.taskType;
              }
              thisProps.UpdateTask(send);
              setTimeout(() => {
                resolve();
              }, 600);
            }),
          }}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    allTasks: state.taskAssignment.allTasks,
    allTasksType: state.taskManagement.allTasks.data.data,
    notiData: state.taskManagement.allTasks,
    updatetaskInfo: state.taskAssignment.updatetask,
    state: state.user.state,
    taskAssignmentState: state.taskAssignment.taskAssignmentState,
  };
}

export default connect(mapStateToProps, {
  getAllTasksAssigned,
  UpdateTask,
})(TaskList);
