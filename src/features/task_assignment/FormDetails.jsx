import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal, ModalBody, ModalHeader } from 'reactstrap';
import { Form } from 'react-formio';
import 'formiojs/dist/formio.full.css';
import {
  resetMessage,
  setForm,
  submitData,
  getSubmissionData,
  formSubmitSync,
  updateSubmission,
  setSubmission,
  resetSubmission,
} from '../../store/action/FormioAction';
import {
  getSingleForm,
  getPersistentSingleForm,
  getPersistentSingleFormSubmission,
  getSingleFormSubmission,
  UpdateTask,
} from '../../store/action/TaskAssignmentAction';
import Notifications from '../notifications';

class FormDetails extends Component {
  constructor(props) {
    super(props);
    this.state = { submission: {}, Psubmission: {} };
  }

  componentDidMount = () => {
    const thisProps = this.props;
    thisProps.getSingleForm(thisProps.formName);
    thisProps.getPersistentSingleForm(thisProps.persistentForm);
    if (thisProps.submissionId !== null && thisProps.submissionId !== '') {
      thisProps.getSingleFormSubmission(
        thisProps.formName,
        thisProps.submissionId,
      );
    }
    if (thisProps.PSubmissionId !== null && thisProps.PSubmissionId !== '') {
      thisProps.getPersistentSingleFormSubmission(
        thisProps.persistentForm,
        thisProps.PSubmissionId,
      );
    }
  };

  // componentWillReceiveProps = (nextProps) => {
  //   const thisProps = this.props;
  //   const send = {};
  //   if (!_.isEqual(thisProps.newSubmission, nextProps.newSubmission)) {
  //     send.id = nextProps.taskId;
  //     send.submission_id = nextProps.newSubmission._id;
  //     thisProps.UpdateTask(send);
  //   }
  // };

  toggleLarge = () => {
    const thisProps = this.props;
    thisProps.toggleFilter();
  };

  render() {
    const { isOpen } = this.props;
    const thisProps = this.props;
    const thisState = this.state;
    const send = {};
    return (
      <>
        <Notifications notidata={thisProps.formNotiData} notifor="openform" />
        <Notifications notidata={thisProps.syncapiNoti} notifor="formupdate" />
        <Modal isOpen={isOpen} toggle={this.toggleLarge} className=" modal-lg">
          <ModalHeader toggle={this.toggleLarge}>Form</ModalHeader>
          <ModalBody>
            {thisProps.Pform && (
              <>
                <h4>Persistent Form</h4>
                <hr />
                <Form
                  form={thisProps.Pform}
                  submission={thisProps.Psubmission}
                  onChange={submission => this.setState({ Psubmission: submission })
                  }
                  onSubmit={async () => {
                    send.tasks = [
                      {
                        id: thisProps.taskId,
                        status: thisProps.taskStatusId,
                        pSubmission: { data: thisState.Psubmission.data },
                      },
                    ];
                    await thisProps.formSubmitSync(send);
                    thisProps.afterForm();
                  }}
                />
              </>
            )}
            {thisProps.form && (
              <>
                <h4>Task Form</h4>
                <hr />
                <Form
                  form={thisProps.form}
                  submission={thisProps.submission}
                  onChange={submission => this.setState({ submission })}
                  onSubmit={async () => {
                    // if (thisProps.submissionId) {
                    // await thisProps.updateSubmission(thisState.submission);
                    // } else {
                    send.tasks = [
                      {
                        id: thisProps.taskId,
                        status: thisProps.taskStatusId,
                        submission: { data: thisState.submission.data },
                      },
                    ];
                    await thisProps.formSubmitSync(send);
                    thisProps.afterForm();
                    // await thisProps.submitData(
                    //   thisProps.form,
                    //   thisState.submission,
                    // );
                    // }
                    // this.setState({ submission: null });
                    // thisProps.getSubmissionData(thisProps.form);
                  }}
                />
              </>
            )}
          </ModalBody>
        </Modal>
      </>
    );
  }
}
function mapStateToProps(state) {
  let sub = state.taskAssignment.singleFormSub.data.data;
  let pSub = state.taskAssignment.singlePFormSub.data.data;
  if (typeof state.taskAssignment.singleFormSub.data.data === 'undefined') {
    sub = {};
  }
  if (typeof state.taskAssignment.singlePFormSub.data.data === 'undefined') {
    pSub = {};
  }
  return {
    form: state.taskAssignment.singleForm.data.data,
    Pform: state.taskAssignment.singlePForm.data.data,
    syncapiNoti: state.formio.syncapi,
    formNotiData: state.taskAssignment.singleForm,
    submission: sub,
    Psubmission: pSub,
    // newSubmission: state.formio.newSubmission.data.data,
    allTasksType: state.taskManagement.allTasks,
    allTaskStatus: state.taskManagement.allTaskStatus,
  };
}

export default connect(
  mapStateToProps,
  {
    getSingleForm,
    getPersistentSingleForm,
    UpdateTask,
    getSingleFormSubmission,
    getPersistentSingleFormSubmission,
    resetMessage,
    setForm,
    submitData,
    getSubmissionData,
    updateSubmission,
    setSubmission,
    resetSubmission,
    formSubmitSync,
  },
)(FormDetails);
