import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, Row, CardBody } from 'reactstrap';
import Tooltip from '@material-ui/core/Tooltip';
import _ from 'lodash';
import SearchFilter from './SearchFilter';
import FormDetails from './FormDetails';
import TaskList from './TaskList';
import TaskAssign from './TaskAssign';
import SendSms from './SendSms';
import AddTask from './AddTask';
import {
  getAllTaskStatus,
  getAllTasksType,
  getAllCallersData,
} from '../../store/action/TaskManagementAction';
import { checkPersistenceExistence } from '../../store/action/FormioAction';
import {
  enqueueSnackbar,
  closeSnackbar,
} from '../../store/action/NotificationAction';

// import { getAllCallersData } from '../../store/action/CallerManagementAction';

require('../../assets/css/TaskSettings.css');

class TaskAssignment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allTasksType: [],
      allTaskStatus: [],
      allAssignee: [],
      selectedValues: {
        update: false,
        taskType: '',
        assignee: '',
        assigneeID: '',
        taskTypeID: '',
        taskStatus: '',
        taskStatusID: '',
        beneficiary: '',
      },
      showFilter: false,
      showForm: false,
      showAssign: false,
      showSms: false,
      showAddtask: false,
      formName: '',
      persistentForm: '',
      selectedTaskId: '',
      selectedTaskStatus: '',
      PSubmissionId: null,
      submissionId: null,
      smsData: null,
      tasklistnoti: 'tasklist',
    };
  }

  componentDidMount = () => {
    const { state, ...actions } = this.props;
    actions.getAllCallersData(
      '',
      'page_size=0&deleted=false&features=Caller Management',
      state.id,
    );
    actions.getAllTaskStatus(state.id);
    actions.getAllTasksType(state.id);
    actions.checkPersistenceExistence(state.name);
  };

  componentWillReceiveProps = (nextProps) => {
    const { ...thisProps } = this.props;
    if (nextProps.notifications.length > 0) {
      setTimeout(() => {
        nextProps.closeSnackbar();
      }, 3000);
    }
    if (nextProps.state.id !== thisProps.state.id) {
      nextProps.getAllCallersData(
        '',
        'page_size=0&deleted=false&features=Caller Management',
        nextProps.state.id,
      );
      nextProps.getAllTaskStatus(nextProps.state.id);
      nextProps.getAllTasksType(nextProps.state.id);
      nextProps.checkPersistenceExistence(nextProps.state.name);
    }
    if (!_.isEqual(nextProps.allTasksType, thisProps.allTasksType)) {
      this.setState({
        allTasksType: nextProps.allTasksType,
      });
    }
    if (!_.isEqual(nextProps.allTaskStatus, thisProps.allTaskStatus)) {
      this.setState({
        allTaskStatus: nextProps.allTaskStatus,
      });
    }
    if (!_.isEqual(nextProps.callers, thisProps.callers)) {
      const allcall = [];
      if (typeof nextProps.callers !== 'undefined') {
        nextProps.callers.forEach((val) => {
          allcall.push({
            id: val.id,
            name: val.name,
            email: val.email,
            mobile: val.mobile,
          });
        });
        this.setState({
          allAssignee: allcall,
        });
      }
    }
  };

  handleChanges = (value, type, name) => {
    if (type === 'tasktype') {
      this.setState(prevState => ({
        selectedValues: {
          ...prevState.selectedValues,
          taskType: name,
          taskTypeID: value,
        },
      }));
    }
    if (type === 'taskstatus') {
      this.setState(prevState => ({
        selectedValues: {
          ...prevState.selectedValues,
          taskStatus: name,
          taskStatusID: value,
        },
      }));
    }
    if (type === 'beneficiary') {
      this.setState(prevState => ({
        selectedValues: { ...prevState.selectedValues, beneficiary: value },
      }));
    }
    if (type === 'assignee') {
      this.setState(prevState => ({
        selectedValues: {
          ...prevState.selectedValues,
          assigneeID: value,
          assignee: name,
        },
      }));
    }
    if (type === 'Reset') {
      this.setState({
        selectedValues: {
          beneficiary: '',
          assigneeID: '',
          assignee: '',
          taskStatus: '',
          taskStatusID: '',
          taskType: '',
          taskTypeID: '',
        },
      });
    }
  };

  openFilter = () => {
    this.setState(lastState => ({ showFilter: !lastState.showFilter }));
  };

  openAddtask = () => {
    // To Do: Add Notification
    // Persistent Form is absent for this State. Cannot add task without a Persistent Form
    const { persistenceExistence, ...actions } = this.props;
    if (persistenceExistence) {
      this.setState(lastState => ({ showAddtask: !lastState.showAddtask }));
    } else {
      actions.enqueueSnackbar({
        key: 'persistentabsent',
        message: `Persistent form is not found. Persistent form is required to add task.`,
        options: {
          key: 'persistentabsent',
          variant: 'error',
          persist: 'false',
        },
      });
    }
  };

  openAssign = () => {
    this.setState(lastState => ({
      showAssign: !lastState.showAssign,
    }));
  };

  AfterAssign = () => {
    this.setState(lastState => ({
      tasklistnoti: 'assignnotilist',
      showAssign: !lastState.showAssign,
      selectedValues: {
        ...lastState.selectedValues,
        update: !lastState.selectedValues.update,
      },
    }));
  };

  AfterAdd = () => {
    this.setState(lastState => ({
      showAddtask: !lastState.showAddtask,
      tasklistnoti: 'addnotilist',
      selectedValues: {
        ...lastState.selectedValues,
        update: !lastState.selectedValues.update,
      },
    }));
  };

  openSms = () => {
    this.setState(lastState => ({ showSms: !lastState.showSms }));
  };

  openSmsPopup = (data) => {
    this.setState(lastState => ({
      smsData: data,
      showSms: !lastState.showSms,
    }));
  };

  openForm = (taskid, formname, pformname, subid, Psubid, taskstatus) => {
    this.setState(lastState => ({
      formName: formname,
      persistentForm: pformname,
      submissionId: subid,
      selectedTaskId: taskid,
      PSubmissionId: Psubid,
      selectedTaskStatus: taskstatus,
      showForm: !lastState.showForm,
    }));
  };

  openForm1 = () => {
    this.setState(lastState => ({ showForm: !lastState.showForm }));
  };

  afterForm = () => {
    this.setState(lastState => ({
      tasklistnoti: 'formnotilist',
      selectedValues: {
        ...lastState.selectedValues,
        update: !lastState.selectedValues.update,
      },
    }));
  };

  aftersms = () => {
    this.setState(lastState => ({
      showSms: !lastState.showSms,
    }));
  };

  render() {
    const {
      allTaskStatus,
      allTasksType,
      allAssignee,
      selectedValues,
      showFilter,
      showForm,
      formName,
      showAssign,
      showSms,
      submissionId,
      selectedTaskId,
      showAddtask,
      smsData,
      selectedTaskStatus,
      persistentForm,
      PSubmissionId,
      tasklistnoti,
    } = this.state;
    return (
      <Row>
        {/* <Card style={{ width: '100%' }}>
          <CardBody onClick={this.openAssign}>Assign Task</CardBody>
        </Card> */}
        <Card style={{ width: '100%', height: '705px' }}>
          {/* <CardHeader>
            Tasks{' '} */}

          {/* <IconButton aria-label="setting" onClick={setting}>
                    <Icon style={{ color: ' #17a2b8' }}>settings</Icon>
                  </IconButton> */}

          <div style={{ padding: ' 8px 18px 0 0' }}>
            {/* <IconButton edge="end" aria-label="delete" onClick={this.openAddtask}>
                    <DeleteIcon color="#17a2b8" />
                  </IconButton> */}
            <Tooltip title="Filter" placement="top">
              <button
                type="button"
                className="fa fa-2x fa-filter"
                onClick={this.openFilter}
                style={{
                  backgroundColor: 'white',
                  color: 'rgb(40, 156, 213)',
                  float: 'right',
                  cursor: 'pointer',
                  border: 'none',
                }}
              />
            </Tooltip>
            <Tooltip title="Task Assign" placement="top">
              <button
                type="button"
                className="fa fa-2x fa-user-plus"
                onClick={this.openAssign}
                style={{
                  backgroundColor: 'white',
                  color: 'rgb(40, 156, 213)',
                  float: 'right',
                  cursor: 'pointer',
                  border: 'none',
                }}
              />
            </Tooltip>
            <Tooltip title="Add Task" placement="top">
              <button
                type="button"
                className="fa fa-2x fa-plus-circle"
                onClick={this.openAddtask}
                style={{
                  backgroundColor: 'white',
                  color: 'rgb(40, 156, 213)',
                  float: 'right',
                  cursor: 'pointer',
                  border: 'none',
                }}
              />
            </Tooltip>
          </div>
          {/* </CardHeader> */}
          <CardBody style={{ padding: '0', overflow: 'auto', height: '418px' }}>
            {showAddtask && (
              <AddTask
                isOpen={showAddtask}
                toggleFilter={this.openAddtask}
                selectedValues={selectedValues}
                allAssignee={allAssignee}
                allTaskStatus={allTaskStatus}
                allTasksType={allTasksType}
                AfterAdd={this.AfterAdd}
              />
            )}
            <TaskAssign
              isOpen={showAssign}
              toggleFilter={this.openAssign}
              selectedValues={selectedValues}
              allAssignee={allAssignee}
              AfterAssign={this.AfterAssign}
            />
            {showSms && (
              <SendSms
                isOpen={showSms}
                toggleFilter={this.openSms}
                smsData={smsData}
                aftersms={this.aftersms}
              />
            )}
            {showFilter && (
              <SearchFilter
                isOpen={showFilter}
                allAssignee={allAssignee}
                allTaskStatus={allTaskStatus}
                allTasksType={allTasksType}
                toggleFilter={this.openFilter}
                selectChanges={this.handleChanges}
                selectedValues={selectedValues}
              />
            )}
            {showForm && (
              <FormDetails
                isOpen={showForm}
                toggleFilter={this.openForm1}
                formName={formName}
                persistentForm={persistentForm}
                submissionId={submissionId}
                PSubmissionId={PSubmissionId}
                taskId={selectedTaskId}
                taskStatusId={selectedTaskStatus}
                afterForm={this.afterForm}
              />
            )}
            <TaskList
              allAssignee={allAssignee}
              allTaskStatus={allTaskStatus}
              allTasksType={allTasksType}
              openForm={this.openForm}
              openSms={this.openSmsPopup}
              tasklistnoti={tasklistnoti}
              selectedValues={selectedValues}
            />
          </CardBody>
        </Card>
      </Row>
    );
  }
}

function mapStateToProps(state) {
  return {
    allTasksType: state.taskManagement.allTasks.data.data,
    allTaskStatus: state.taskManagement.allTaskStatus.data.data,
    callers: state.taskManagement.allCallers.data.data,
    state: state.user.state,
    persistenceExistence: state.formio.persistenceExistence,
    notifications: state.notification.notifications,
  };
}

export default connect(mapStateToProps, {
  getAllTaskStatus,
  getAllTasksType,
  getAllCallersData,
  checkPersistenceExistence,
  enqueueSnackbar,
  closeSnackbar,
})(TaskAssignment);
