import React, { Component } from 'react';
import { connect } from 'react-redux';
import Cookies from 'universal-cookie';
import {
  Form,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Col,
} from 'reactstrap';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MInput from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';
import {
  searchTasksAssigned,
  addNewTask,
} from '../../store/action/TaskAssignmentAction';
import { formSubmitSync } from '../../store/action/FormioAction';
import SingleSelect from '../../components/select_input/single_select/SingleSelect';
import Notification from '../notifications';

const cookies = new Cookies();
const stateID = cookies.get('state').id;

class AddTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: '',
      selectedBene: '',
      selectedAssignee: {},
      taskStatusID: '',
      taskTypeID: '',
      beneError: false,
    };
  }

  taskAddFun = async () => {
    const thisState = this.state;
    const { state, ...thisProps } = this.props;
    const send = {};
    const mobileRex = /^[1-9]\d{9}$/;
    if (thisState.taskTypeID !== '') send.type = thisState.taskTypeID;
    if (thisState.taskStatusID !== '') send.status = thisState.taskStatusID;
    if (
      thisState.selectedBene !== ''
      && mobileRex.test(thisState.selectedBene)
    ) {
      send.mobile = thisState.selectedBene;
    } else {
      this.setState({ beneError: true });
      return '';
    }
    if (typeof thisState.selectedAssignee.value !== 'undefined') send.assignee = thisState.selectedAssignee.value;
    send.state = state.id || stateID;
    // await thisProps.addNewTask(send);
    await thisProps.formSubmitSync({ tasks: [send] });
    thisProps.AfterAdd();
    return '';
  };

  toggleLarge = () => {
    const thisProps = this.props;
    thisProps.toggleFilter();
  };

  handleInputChange = (newValue) => {
    // const inputValue = newValue.replace(/\W/g, '');
    const inputValue = newValue;
    this.setState({ inputValue });
  };

  filterBene = () => {
    const thisProps = this.props;
    const ret = [];
    if (typeof thisProps.taskSearch !== 'undefined') {
      thisProps.taskSearch.forEach((val) => {
        const obj = {};
        obj.label = val.beneficiary.mobile;
        obj.value = val.beneficiary.id;
        ret.push(obj);
      });
    }
    return ret;
  };

  loadOptions = async (inputValue, callback) => {
    const thisProps = this.props;
    if (inputValue !== '') {
      await thisProps.searchTasksAssigned(
        `page_size=0&beneficiaryMobile=${inputValue}`,
      );
    }
    callback(this.filterBene());
  };

  handleChangeSingle = (val) => {
    this.setState({ selectedAssignee: val });
  };

  handleChanges = (event) => {
    if (event.target.name === 'tasktype') {
      this.setState({ taskTypeID: event.target.value });
    }
    if (event.target.name === 'taskstatus') {
      this.setState({ taskStatusID: event.target.value });
    }
  };

  benechange = (e) => {
    const mobileRex = /^[1-9]\d{9}$/;
    if (!mobileRex.test(e.target.value)) {
      this.setState({ beneError: true });
    } else {
      this.setState({ beneError: false });
    }
    // this.setState({ selectedBene: e.value });
    this.setState({ selectedBene: e.target.value });
  };

  render() {
    const {
      isOpen,
      allAssignee,
      allTasksType,
      allTaskStatus,
      syncapi,
    } = this.props;
    const {
      inputValue,
      selectedBene,
      selectedAssignee,
      taskStatusID,
      taskTypeID,
      beneError,
    } = this.state;

    const allCallers = [];
    allAssignee.forEach((val) => {
      allCallers.push({ label: val.name, value: val.id });
    });

    const allTypes = allTasksType.length === 0
        ? [
          <MenuItem key="1" value="">
            <em>None</em>
          </MenuItem>,
        ]
        : [];

    if (typeof allTasksType !== 'undefined') {
      allTasksType.forEach((val) => {
        allTypes.push(
          <MenuItem key={val.id} value={val.id}>
            {val.name}
          </MenuItem>,
        );
      });
    }

    const allStatus = allTaskStatus.length === 0
        ? [
          <MenuItem key="1" value="">
            <em>None</em>
          </MenuItem>,
        ]
        : [];
    if (typeof allTaskStatus !== 'undefined') {
      allTaskStatus.forEach((val) => {
        allStatus.push(
          <MenuItem key={val.id} value={val.id}>
            {val.name}
          </MenuItem>,
        );
      });
    }
    return (
      <>
        <Notification notidata={syncapi} notifor="addtask" />
        <Modal
          isOpen={isOpen}
          toggle={this.toggleLarge}
          className="modal-lg modal-info"
        >
          <ModalHeader toggle={this.toggleLarge}>Add Task</ModalHeader>
          <ModalBody>
            <Form
              style={{
                display: 'flex',
                flexFlow: 'column',
              }}
            >
              <Col>
                <FormControl style={{ margin: '10px' }}>
                  <TextField
                    error={beneError}
                    type="number"
                    helperText={beneError && 'Enter 10 digit phone number'}
                    id="beneficiary"
                    label="Beneficiary Phone"
                    value={selectedBene}
                    onChange={this.benechange}
                  />
                  {/* <AsyncSelect
                    lavel="Beneficiary"
                    cacheOptions
                    loadOptions={this.loadOptions}
                    defaultOptions
                    placeholder="Enter beneficiary phone"
                    onInputChange={this.handleInputChange}
                    onChange={this.benechange}
                  /> */}
                </FormControl>
              </Col>
              <Col>
                <FormControl style={{ margin: '10px' }}>
                  <SingleSelect
                    label="Assignee"
                    placeholder="Assignee"
                    suggestions={allCallers}
                    single={selectedAssignee}
                    handleChangeSingle={this.handleChangeSingle}
                  />
                </FormControl>
              </Col>
              <Col>
                <FormControl style={{ margin: '10px' }}>
                  <InputLabel htmlFor="tasktype-select">Task Type</InputLabel>
                  <Select
                    value={taskTypeID}
                    onChange={this.handleChanges}
                    input={<MInput name="tasktype" id="tasktype-select" />}
                  >
                    {allTypes}
                  </Select>
                </FormControl>
              </Col>
              <Col>
                <FormControl style={{ margin: '10px' }}>
                  <InputLabel htmlFor="taskstatus-select">
                    Task Status
                  </InputLabel>
                  <Select
                    value={taskStatusID}
                    onChange={this.handleChanges}
                    input={<MInput name="taskstatus" id="taskstatus-select" />}
                  >
                    {allStatus}
                  </Select>
                </FormControl>
              </Col>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.taskAddFun}>
              Add Task
            </Button>
          </ModalFooter>
        </Modal>
      </>
    );
  }
}
function mapStateToProps(state) {
  return {
    // allTasksType: state.taskManagement.allTasks,
    // allTaskStatus: state.taskManagement.allTaskStatus,
    taskSearch: state.taskAssignment.taskSearch.data.data,
    taskSearchStatus: state.taskAssignment.taskSearch.status,
    addTasksInfo: state.taskAssignment.addTasks,
    syncapi: state.formio.syncapi,
    state: state.user.state,
  };
}

export default connect(mapStateToProps, {
  searchTasksAssigned,
  addNewTask,
  formSubmitSync,
})(AddTask);
