import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Form,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MInput from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';

import {
  getAllTemplates,
  sendMessage,
} from '../../store/action/TaskAssignmentAction';
import Notification from '../notifications';

class SendSms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      templateId: '',
      templateData: null,
    };
  }

  componentDidMount() {
    const thisProps = this.props;
    // thisProps.sendMessage({
    //   template: 'd1eb1375-1877-4f88-b1d5-c2ea62eb98a7',
    //   mobile: '9911114530',
    // });
    thisProps.getAllTemplates();
  }

  toggleLarge = () => {
    const thisProps = this.props;
    thisProps.toggleFilter();
  };

  sendSmsData = async () => {
    const { templateId } = this.state;
    const thisProps = this.props;
    if (
      typeof thisProps.smsData.beneficiary_name !== 'undefined'
      && thisProps.smsData.beneficiary_name !== ''
    ) {
      await thisProps.sendMessage({
        template: templateId,
        mobile: thisProps.smsData.beneficiary_name,
      });
      thisProps.aftersms();
    } else {
      // eslint-disable-next-line no-alert
      alert('no beneficiary selected');
    }
  };

  handleChanges = (event) => {
    const { allTemplate } = this.props;
    this.setState({ templateId: event.target.value });
    allTemplate.forEach((val) => {
      if (val.id === event.target.value) {
        this.setState({ templateData: val.content });
      }
    });
  };

  render() {
    const { isOpen, allTemplate, sendsms } = this.props;
    const { templateId, templateData } = this.state;
    const allTemp = [
      <MenuItem key="1" value="">
        <em>None</em>
      </MenuItem>,
    ];
    if (typeof allTemplate !== 'undefined') {
      allTemplate.forEach((val) => {
        allTemp.push(
          <MenuItem key={val.id} value={val.id}>
            {val.name}
          </MenuItem>,
        );
      });
    }
    return (
      <>
        <Notification notifor="sendsms" notidata={sendsms} />
        <Modal
          isOpen={isOpen}
          toggle={this.toggleLarge}
          className="modal-info modal-lg"
        >
          <ModalHeader toggle={this.toggleLarge}>Send SMS</ModalHeader>
          <ModalBody>
            <Form
              style={{
                display: 'flex',
                flexFlow: 'column',
              }}
            >
              <FormControl style={{ margin: '10px' }}>
                <InputLabel htmlFor="template-select">Template</InputLabel>
                <Select
                  value={templateId}
                  onChange={this.handleChanges}
                  input={<MInput name="template" id="template-select" />}
                >
                  {allTemp}
                </Select>
              </FormControl>
              <FormControl style={{ margin: '10px' }}>
                {templateData}
              </FormControl>
            </Form>
          </ModalBody>
          <ModalFooter>
            {/* <Button color="primary" onClick={this.toggleLarge}>
            Do Something
          </Button>{' '} */}
            <Button
              color="secondary"
              data-test="user_cancel"
              onClick={this.sendSmsData}
            >
              Send
            </Button>
          </ModalFooter>
        </Modal>
      </>
    );
  }
}
function mapStateToProps(state) {
  return {
    allTemplate: state.taskAssignment.template.data.data,
    sendsms: state.taskAssignment.sendsms,
    // allTasksType: state.taskManagement.allTasks,
    // allTaskStatus: state.taskManagement.allTaskStatus,
  };
}

export default connect(
  mapStateToProps,
  {
    getAllTemplates,
    sendMessage,
  },
)(SendSms);
