import React, { Component } from 'react';

import { OutTable, ExcelRenderer } from 'react-excel-renderer';
import Grid from '@material-ui/core/Grid';
import {
  Jumbotron,
  Col,
  Input,
  InputGroup,
  InputGroupAddon,
  FormGroup,
  Label,
  Button,
  Fade,
  FormFeedback,
  Container,
  Card,
} from 'reactstrap';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      dataLoaded: false,
      isFormInvalid: false,
      rows: null,
      cols: null,
    };
    // this.fileHandler = this.fileHandler.bind(this);
    // this.toggle = this.toggle.bind(this);
    // this.openFileBrowser = this.openFileBrowser.bind(this);
    // this.renderFile = this.renderFile.bind(this);
    // this.openNewPage = this.openNewPage.bind(this);
    this.fileInput = React.createRef();
  }

  renderFile = (fileObj) => {
    // just pass the fileObj as parameter
    const thisProps = this.props;
    ExcelRenderer(fileObj, (err, resp) => {
      if (err) {
        console.log(err);
      } else {
        this.setState({
          dataLoaded: true,
          cols: resp.cols,
          rows: resp.rows,
        });
        thisProps.ExcelRender(resp);
      }
    });
  };

  fileHandler = (event) => {
    if (event.target.files.length) {
      const fileObj = event.target.files[0];
      const fileName = fileObj.name;

      // check for file extension and pass only if it is .xlsx and display error message otherwise
      const fileExtension = fileName.slice(fileName.lastIndexOf('.') + 1);
      if (
        fileExtension === 'xlsx'
        || fileExtension === 'csv'
        || fileExtension === 'xls'
      ) {
        this.setState({
          uploadedFileName: fileName,
          isFormInvalid: false,
        });
        this.renderFile(fileObj);
      } else {
        this.setState({
          isFormInvalid: true,
          uploadedFileName: '',
        });
      }
    }
  };

  openFileBrowser = () => {
    this.fileInput.current.click();
  };

  toggle = () => {
    this.setState(prevState => ({
      isOpen: !prevState.isOpen,
    }));
  };

  render() {
    // console.log(this.state);
    const {
      uploadedFileName,
      isFormInvalid,
      cols,
      rows,
      dataLoaded,
    } = this.state;

    return (
      <div>
        <form>
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <Button
                color="info"
                style={{ color: 'white', zIndex: 0 }}
                onClick={this.openFileBrowser}
              >
                <i className="cui-file"></i> Browse&hellip;
              </Button>
              <input
                type="file"
                hidden
                onChange={this.fileHandler}
                ref={this.fileInput}
                accept=".xls,.xlsx,.csv"
                onClick={(event) => {
                  // eslint-disable-next-line no-param-reassign
                  event.target.value = null;
                }}
                style={{ padding: '10px' }}
              />
            </InputGroupAddon>
            <Input
              type="text"
              className="form-control"
              value={uploadedFileName}
              readOnly
              invalid={isFormInvalid}
            />
            <FormFeedback>
              <Fade in={isFormInvalid} tag="h6" style={{ fontStyle: 'italic' }}>
                Selected file is invalid!
              </Fade>
            </FormFeedback>
          </InputGroup>
        </form>

        {dataLoaded && (
          <div>
            <Card body outline color="secondary" className="restrict-card">
              <OutTable
                data={rows}
                columns={cols}
                tableClassName="ExcelTable2007"
                tableHeaderRowClass="heading"
              />
            </Card>
          </div>
        )}
        <div>
          <a href="/Bulk number upload sample.xlsx" download>
            Click to Download Sample
          </a>
        </div>
      </div>
    );
  }
}

export default App;
