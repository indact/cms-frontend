import React, { Component } from 'react';
import { connect } from 'react-redux';

import Container from '@material-ui/core/Container';

import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { Button } from '@material-ui/core';
import Cookies from 'universal-cookie';
import SingleSelect from '../../components/select_input/single_select/SingleSelect';
import {
  getAllTaskStatus,
  getAllTasksType,
  // getAllCallersData,
} from '../../store/action/TaskManagementAction';
import { bulkUploadAction } from '../../store/action/BulkUpload';
import Upload from './Upload';

const style = {
  Paper: {
    padding: ' 33px',
  },
  select: {
    paddingBottom: '20px',
  },
  Notes: {
    paddingBottom: '20px',
    paddingTop: '20px',
  },
};

const cookies = new Cookies();

class SearchFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      task_type: '',
      task_status: '',
      rows: [],
      mobile: [],
      error: {
        taskStatus: false,
        taskType: false,
        mobile: false,
        mobileList: [],
      },
      // cols: null,
    };
  }

  componentDidMount = async () => {
    const { state, ...actions } = this.props;
    const stateId = state.id || cookies.get('state').id;
    await actions.getAllTasksType(stateId);
    await actions.getAllTaskStatus(stateId);
    // await actions.getFormList(state.name);
  };

  getSnapshotBeforeUpdate = async (prevProps) => {
    const { state, ...actions } = this.props;
    const stateId = state.id || cookies.get('state').id;
    if (prevProps.state.name !== state.name) {
      await actions.getAllTasksType(stateId);
      await actions.getAllTaskStatus(stateId);
      return null;
    }
    return null;
  };

  ExcelRender = (resp) => {
    this.setState({
      // cols: resp.cols,
      rows: resp.rows,
    });
  };

  validate = (mobile, { value: taskType }, { value: taskStatus }) => {
    let validated = true;
    const { error } = this.state;
    if (!taskType) {
      validated = false;
      error.taskType = true;
    } else error.taskType = false;
    if (!taskStatus) {
      validated = false;
      error.taskStatus = true;
    } else error.taskStatus = false;
    if (mobile.length < 1) {
      validated = false;
      error.mobile = true;
    } else error.mobile = false;

    this.setState({ error });

    return validated;
  };

  submit = () => {
    const {
      validate,
      state: {
        rows, task_type, task_status, error, mobile,
      },
      props,
    } = this;
    // const mobile = [];
    const mobileRex = /^[1-9]\d{9}$/;
    error.mobileList = [];
    mobile.splice(0, mobile.length);
    rows.forEach((row, index) => {
      row.forEach((e) => {
        const number = e.toString().trim();
        if (mobileRex.test(number)) mobile.push(number);
        else error.mobileList.push({ row: index, number });
      });
    });

    const valid = validate(mobile, task_type, task_status);

    if (valid) {
      props.bulkUploadAction({
        mobile,
        task: task_type,
        status: task_status,
      });
    }
  };

  render() {
    const { allTasks, allTaskStatus } = this.props;
    const { task_type, task_status, error } = this.state;

    // this data making for select task type
    const tasksData = allTasks.data === undefined ? [] : allTasks.data;
    const allTaskType = tasksData.map(suggestion => ({
      value: suggestion.name,
      label: suggestion.name,
      id: suggestion.id,
    }));

    const taskStatusData = allTaskStatus.data === undefined ? [] : allTaskStatus.data;
    const TaskStatusList = taskStatusData.map(suggestion => ({
      value: suggestion.name,
      label: suggestion.name,
      id: suggestion.id,
    }));

    return (
      <div>
        <Container maxWidth="md">
          <Paper elevation={3} style={style.Paper}>
            <Grid container spacing={10}>
              <Grid item xs={6} style={style.select}>
                <SingleSelect
                  //   error={error.cycle}
                  label="Task Type"
                  placeholder="Select Task Type"
                  suggestions={allTaskType}
                  value={task_type === ' ' ? '' : task_type}
                  handleChangeSingle={value => this.setState({ task_type: value })
                  }
                />
                {error.taskType && (
                  <div className="alert alert-danger">
                    <strong>Required!</strong>
                    &nbsp;The operation cannot proceed without a Task Type
                    Selected.
                  </div>
                )}
              </Grid>
              <Grid item xs={6} style={style.select}>
                <SingleSelect
                  label="Task Status"
                  placeholder="Select Task Status"
                  suggestions={TaskStatusList}
                  value={task_status === ' ' ? '' : task_status}
                  handleChangeSingle={value => this.setState({ task_status: value })
                  }
                />
                {error.taskStatus && (
                  <div className="alert alert-danger">
                    <strong>Required!</strong>
                    &nbsp;The operation cannot proceed without a Task Status
                    Selected.
                  </div>
                )}
              </Grid>
              <Grid item xs={12} style={style.Notes}>
                <div>
                  <p style={{ color: 'red' }}>
                    <strong>NOTE:</strong>
                    <ul>
                      Do not put the comma, special character while entering the
                      number in the file.
                      <br></br>
                      Do not write the serial number, Row name and column name
                      in the file.
                      <br></br>
                      Do not exceed more than 500 mobile number at a time in the
                      file.
                    </ul>
                  </p>
                  <p style={{ color: 'green' }}>
                    <strong>Suggestion</strong>: The best way to upload the file
                    is -
                    <ol>
                      <a href="/Bulk number upload sample.xlsx" download>
                        1.Download the sample file.
                      </a>
                      <br></br>
                      2. Put all the data in sample file.
                      <br></br>
                      3. Then upload sample file.
                    </ol>
                  </p>
                </div>
              </Grid>
              <Grid item xs={12}>
                <Upload ExcelRender={this.ExcelRender} />
                {error.mobile && (
                  <div className="alert alert-danger">
                    <strong>No Valid Mobile Numbers!</strong>
                    &nbsp;The operation cannot proceed without valid mobile
                    numbers.
                  </div>
                )}
                {error.mobileList.length > 0 && (
                  <div className="alert alert-warning">
                    <strong>
                      The numbers below are invalid Mobile Numbers:
                    </strong>
                    <br />
                    {error.mobileList.map(mobile => (
                      <p>
                        {mobile.number} in row {mobile.row}
                      </p>
                    ))}
                  </div>
                )}
              </Grid>
            </Grid>
            <Grid
              style={{
                marginTop: ' -13px',
                float: 'right',
              }}
              item
              xs={12}
            >
              <Button variant="contained" onClick={this.submit} color="primary">
                Submit
              </Button>
            </Grid>
          </Paper>
        </Container>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    allTasks: state.taskManagement.allTasks.data,
    allTaskStatus: state.taskManagement.allTaskStatus.data,
    state: state.user.state,
    bulkUpload: state.bulkUpload,
  };
}

export default connect(mapStateToProps, {
  getAllTaskStatus,
  getAllTasksType,
  bulkUploadAction,
})(SearchFilter);
