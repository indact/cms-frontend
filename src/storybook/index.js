import React from 'react';
import { configure, storiesOf } from '@storybook/react';

const req = require.context('../components', true, /\.stories\.js$/);
const authreq = require.context('../auth', true, /\.stories\.js$/);

function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);

function authLoadStories() {
  authreq.keys().forEach(filename => authreq(filename));
}

configure(loadStories, module);
configure(authLoadStories, module);
