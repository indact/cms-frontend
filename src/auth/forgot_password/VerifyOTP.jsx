import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import {
  Button,
  Col,
  Container,
  Input,
  InputGroup,
  InputGroupAddon,
  Row,
  FormFeedback,
} from 'reactstrap';
import LoadingOverlay from 'react-loading-overlay';
import {
  ErrorWithoutProps,
  ErrorWithProps,
  Loader,
} from '../../components/index';
import { VerifyOtp, ResendOtp, Refresh } from '../../store/action/AuthAction';

class ForgotPassword extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      username: '',
      CompoToggel: false,
      error: false,
      email: '',
      mobile: '',
      OTP: '',
      loader: false,
      errorOTP: false,
      resetpassword: false,
    };
  }

  handleChange = (event) => {
    const oldState = { ...this.state };
    oldState[event.target.id] = event.target.value.trim();
    this.setState({ ...oldState });
  };

  VerifyOTP = () => {
    const { OTP } = this.state;
    const { mobile } = this.props;
    const VerifyOtpFun = this.props;

    // eslint-disable-next-line no-restricted-globals
    if (!isNaN(OTP) && OTP.toString().length === 4) {
      const data = {
        mobile,
        otp: OTP,
      };

      const oldState = { ...this.state };
      oldState.errorOTP = false;
      oldState.loader = true;
      this.setState({ ...oldState });
      VerifyOtpFun.VerifyOtp(data);
    } else {
      const oldState = { ...this.state };
      oldState.errorOTP = true;
      this.setState({ ...oldState });
    }
  };

  ResendOTP = () => {
    const { mobile } = this.props;
    const ResendOTPFun = this.props;
    const data = {
      mobile,
      method: 'text',
    };
    const oldState = { ...this.state };
    oldState.loader = true;
    this.setState({ ...oldState });
    ResendOTPFun.ResendOtp(data);
  };

  componentWillUnmount = () => {
    const RefreshFun = this.props;
    RefreshFun.Refresh('forgotPassword');
  };

  render() {
    const { CompoToggel, errorOTP, loader } = this.state;
    const {
      resendOtpErrMessage,
      verifyOtpErrMessage,
      verifyOtpStatus,
      resendOtpStatus,
      mobile,
    } = this.props;
    if ((verifyOtpStatus !== 0 || resendOtpStatus !== 0) && loader) {
      const oldState = { ...this.state };
      oldState.loader = false;
      this.setState({ ...oldState });
    }

    if (CompoToggel === true) {
      return <Redirect true from="/verifyOTP" to="/ForgotPassword" />;
    }
    if (verifyOtpStatus === 200) {
      return <Redirect true push from="/verifyOTP" to="/resetPassword" />;
    }
    return (
      <LoadingOverlay
        active={loader}
        spinner={<Loader />}
        text="Loading your content..."
      >
        <div className="app flex-row align-items-center">
          <Container>
            <Row className="justify-content-center">
              <Col md="6">
                <div className="clearfix">
                  <h4 className="pt-3">Verify OTP</h4>
                  <p className="text-muted float-left">
                    Please enter OTP here for mobile verification.
                  </p>
                </div>
                {verifyOtpStatus !== 200 && verifyOtpStatus !== 0 && (
                  <ErrorWithProps props={{ errMessage: verifyOtpErrMessage }} />
                )}
                {resendOtpStatus !== 200 && resendOtpStatus !== 0 && (
                  <ErrorWithProps props={{ errMessage: resendOtpErrMessage }} />
                )}
                {mobile === 0 && (
                  <ErrorWithoutProps message=" You cannot verify OTP directly. Please go back and follow each step" />
                )}
                <InputGroup className="input-prepend">
                  <InputGroupAddon addonType="append" />
                  <Input
                    // size="4"
                    type="number"
                    invalid={errorOTP}
                    id="OTP"
                    onChange={this.handleChange}
                    placeholder="Enter OTP"
                  />
                  <InputGroupAddon addonType="append">
                    <Button color="primary" onClick={this.VerifyOTP}>
                      Verify
                    </Button>
                  </InputGroupAddon>
                  <FormFeedback data-test="OTP_error">
                    Please Enter correct input.
                  </FormFeedback>
                </InputGroup>
              </Col>
            </Row>
            <Row className="justify-content-center">
              <Col md="3" style={{ margin: '31px -53px 0 0' }}>
                <InputGroup className="input-prepend">
                  <InputGroupAddon addonType="append">
                    <Button
                      color="primary"
                      onClick={() => this.setState({
                        CompoToggel: !CompoToggel,
                      })
                      }
                    >
                      Back
                    </Button>
                  </InputGroupAddon>
                  <div />
                  <InputGroupAddon addonType="append">
                    <Button color="primary" onClick={this.ResendOTP}>
                      Re-Send
                    </Button>
                  </InputGroupAddon>
                </InputGroup>
              </Col>
            </Row>
          </Container>
        </div>
      </LoadingOverlay>
    );
  }
}

const mapStateToProps = state => ({
  resendOtpErrMessage: state.auth.resendOtpErrMessage,
  verifyOtpErrMessage: state.auth.verifyOtpErrMessage,

  resendOtpStatus: state.auth.resendOtpStatus,
  verifyOtpStatus: state.auth.verifyOtpStatus,
  mobile: state.auth.mobile,
});
export default connect(
  mapStateToProps,
  { VerifyOtp, ResendOtp, Refresh },
)(ForgotPassword);
