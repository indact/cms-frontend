import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import {
  Button,
  Col,
  Container,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  FormFeedback,
} from 'reactstrap';
import LoadingOverlay from 'react-loading-overlay';
import {
  ErrorWithoutProps,
  ErrorWithProps,
  Loader,
} from '../../components/index';
import { Forgotpassword, Refresh } from '../../store/action/AuthAction';

class ForgotPassword extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      username: '',
      loader: false,
      error: false,
      email: '',
      mobile: '',
    };
  }

  handleChange = (event) => {
    const oldState = { ...this.state };
    oldState[event.target.id] = event.target.value.trim();
    this.setState({ ...oldState });
  };

  search = (e) => {
    e.preventDefault();
    const ForgotpasswordFun = this.props;
    const { username } = this.state;
    // eslint-disable-next-line no-useless-escape
    const emailRex = /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    const mobileRex = /^[1-9]\d{9}$/;

    if (emailRex.test(username)) {
      // use only mobile recovery email recovery temprary closed

      // let data = { email: username };
      const oldState = { ...this.state };
      oldState.error = true;
      this.setState({ ...oldState });

      // ForgotpasswordFun.Forgotpassword(data);
    } else if (mobileRex.test(username)) {
      const data = { mobile: username };
      ForgotpasswordFun.Forgotpassword(data);
      const oldState = { ...this.state };
      oldState.loader = true;
      this.setState({ ...oldState });
    } else {
      const oldState = { ...this.state };
      oldState.error = true;
      this.setState({ ...oldState });
    }
  };
  //  for reset all responce

  componentWillUnmount = () => {
    const RefreshFun = this.props;
    RefreshFun.Refresh('forgotPassword');
  };

  render() {
    const { error, loader } = this.state;
    const { sendOtpStatus, sendOtpErrMessage } = this.props;
    if (sendOtpStatus !== 0 && loader) {
      const oldState = { ...this.state };
      oldState.loader = false;
      this.setState({ ...oldState });
    }
    if (sendOtpStatus === 200) {
      return <Redirect true push from="/ForgotPassword" to="/verifyOTP" />;
    }
    return (
      <LoadingOverlay
        active={loader}
        spinner={<Loader />}
        text="Loading your content..."
      >
        <div className="app flex-row align-items-center">
          <Container>
            <Row className="justify-content-center">
              <Col md="7">
                <div className="clearfix">
                  <h1 className="float-left display-3 mr-4">? </h1>
                  <h4 className="pt-3">Forgot Password</h4>
                  <p className="text-muted float-left">
                    Please enter the registered mobile number. OTP will be sent
                    for verification.
                  </p>
                </div>
                {// eslint-disable-next-line no-nested-ternary
                sendOtpStatus === 404
                || sendOtpStatus === 401
                || sendOtpStatus === 400
                || sendOtpStatus === 409 ? (
                  <ErrorWithProps props={{ errMessage: sendOtpErrMessage }} />
                ) : sendOtpStatus >= 300 ? (
                  <ErrorWithoutProps message=" Something went wrong! Please contact the cms administrator or try again after refresh" />
                ) : (
                  ''
                )}
                <InputGroup className="input-prepend">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="fa fa-search" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    // size="16"
                    invalid={error}
                    type="text"
                    id="username"
                    onChange={this.handleChange}
                    placeholder="Enter registerd mobile"
                  />
                  <InputGroupAddon addonType="append">
                    <Button color="primary" onClick={this.search}>
                      Search
                    </Button>
                  </InputGroupAddon>
                  <FormFeedback data-test="forgot_error">
                    Please enter correct input.
                  </FormFeedback>
                </InputGroup>
                <Link to="/">Go To Login</Link>
              </Col>
            </Row>
          </Container>
        </div>
      </LoadingOverlay>
    );
  }
}

const mapStateToProps = state => ({
  sendOtpErrMessage: state.auth.sendOtpErrMessage,
  sendOtpStatus: state.auth.sendOtpStatus,
});
export default connect(
  mapStateToProps,
  { Forgotpassword, Refresh },
)(ForgotPassword);
