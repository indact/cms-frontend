import React, { Component } from 'react';

import {
  Button, Card, CardBody, Col, Container, Form, Row,
} from 'reactstrap';
import { connect } from 'react-redux';
import LoadingOverlay from 'react-loading-overlay';
import { ResetPassword, Refresh } from '../../store/action/AuthAction';
import {
  Password,
  ErrorWithoutProps,
  ErrorWithProps,
  Loader,
} from '../../components/index';

class Login extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      password: '',
      loader: false,
      repassword: '',
      error: {
        password: false,
        repassword: false,
      },
      validation: {
        password: false,
        repassword: false,
      },
    };
  }

  handleValidation = async (name, state) => {
    const { validation } = this.state;
    validation[name] = state;
    await this.setState({ validation });
  };

  handleChange = (event) => {
    const oldState = { ...this.state };
    const { error } = { ...this.state };
    if (event.target.name) {
      error[event.target.name] = false;
      this.setState({ ...error });
    }
    oldState[event.target.name] = event.target.value.trim();
    this.setState({ ...oldState });
  };

  handelError = (name) => {
    const { error } = { ...this.state };
    error[name] = true;
    this.setState({ ...error });
  };

  submit = (e) => {
    e.preventDefault();
    const {
      password,

      validation,
    } = this.state;

    let success = true;
    Object.values(validation).map((value) => {
      success = success && value;
      return success;
    });
    if (success) {
      const ResetPasswordFun = this.props;
      const { userKey } = this.props;
      const oldState = { ...this.state };
      oldState.loader = false;
      this.setState({ ...oldState });
      ResetPasswordFun.ResetPassword({ password, userKey });
    } else if (password === '') {
      this.handelError('password');
    }
  };

  goToLogin = () => {
    const { history } = this.props;

    history.push('/');
  };

  componentWillUnmount = () => {
    const RefreshFun = this.props;
    RefreshFun.Refresh('resetpassword');
  };

  render() {
    const { error, loader } = this.state;
    const { ResetStatus, resetPasswordError, mobile } = this.props;
    if (ResetStatus !== 0 && loader) {
      const oldState = { ...this.state };
      oldState.loader = false;
      this.setState({ ...oldState });
    }
    return (
      <LoadingOverlay
        active={loader}
        spinner={<Loader />}
        text="Loading your content..."
      >
        <div className="app flex-row align-items-center">
          <Container>
            <Row className="justify-content-center">
              <Col md="8">
                <Card className="col-12 p-12">
                  <CardBody>
                    <Form>
                      <h1>Reset Password</h1>
                      <p className="text-muted">Reset your Password here!</p>
                      {ResetStatus !== 200 && ResetStatus !== 0 && (
                        <ErrorWithProps
                          props={{ errMessage: resetPasswordError }}
                        />
                      )}
                      {mobile === 0 && (
                        <ErrorWithoutProps message="You cannot reset your password directly. Please go back and follow each step" />
                      )}

                      {ResetStatus === 200 ? (
                        <div className="resetsucmsg">
                          Password has been reset successfully. Please login
                          here.
                          <Button
                            color="link"
                            onClick={this.goToLogin}
                            className="px-0"
                          >
                            Login
                          </Button>{' '}
                        </div>
                      ) : (
                        <div>
                          <Password
                            onChange={this.handleChange}
                            onValidation={this.handleValidation}
                            submit={error.password}
                          />
                          {/* <div
                            style={{
                              fontSize: ' x-small',
                              margin: ' -6px 0 5px 0',
                            }}
                          >
                            Make sure it&apos;s at least 6 characters including
                            a number, symbols and a lowercase-uppercase letter.{' '}
                          </div> */}

                          <Row>
                            <Col xs="6">
                              <Button
                                color="primary"
                                onClick={this.submit}
                                className="px-4"
                              >
                                Submit
                              </Button>
                            </Col>
                            <Col xs="6" className="text-right">
                              {' '}
                              <Button
                                color="link"
                                className="px-10"
                                onClick={() => {
                                  const { history } = this.props;
                                  history.push('/ForgotPassword');
                                }}
                              >
                                Back
                              </Button>
                              <Button
                                color="link"
                                className="px-0"
                                onClick={() => {
                                  const { history } = this.props;
                                  history.push('/');
                                }}
                              >
                                Login
                              </Button>
                            </Col>
                          </Row>
                        </div>
                      )}
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </Container>
        </div>
      </LoadingOverlay>
    );
  }
}

const mapStateToProps = state => ({
  ResetStatus: state.auth.resetPasswordStatus,
  userKey: state.auth.userKey,
  resetPasswordError: state.auth.resetPasswordError,
  mobile: state.auth.mobile,
});
export default connect(
  mapStateToProps,
  { ResetPassword, Refresh },
)(Login);
