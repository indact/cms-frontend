import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Cookies from 'universal-cookie';
// import { Card, CardImg, CardText, CardBody, CardLink,
//   CardTitle, CardSubtitle } from 'reactstrap';
import LoadingOverlay from 'react-loading-overlay';

import {
  Button,
  // Card,
  // CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Row,
  Card,
  CardBody,
} from 'reactstrap';
import { login, Refresh } from '../../store/action/AuthAction';
import userActions from '../../store/action/UserAction';
import {
  Username,
  UserPassword,
  ErrorWithoutProps,
  ErrorWithProps,
  Loader,
  ErrorBoundary,
} from '../../components/index';

const { getStatesData } = userActions;

const baseUrl = `${process.env.REACT_APP_BASE_URL}:${process.env.REACT_APP_CMS_PORT}`;
const cookies = new Cookies();

class Login extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      username: '',
      password: '',
      loader: false,
      cookieStatus: false,
      error: {
        username: false,
        password: false,
      },

      validation: {
        username: false,
        password: false,
      },
    };
  }

  // static getDerivedStateFromProps = (nextProps, prevProps) => {
  //   if (prevProps.token !== nextProps.token) {
  //     nextProps.getStatesData(nextProps.token);
  //     return { token: nextProps.token };
  //   }
  //   return null;
  // };

  handleValidation = async (name, state) => {
    const { validation } = this.state;
    validation[name] = state;
    await this.setState({ validation });
  };

  handleChange = (event) => {
    const oldState = { ...this.state };
    const { error } = { ...this.state };
    if (event.target.name) {
      error[event.target.name] = false;
      this.setState({ ...error });
    }
    oldState[event.target.name] = event.target.value.trim();
    this.setState({ ...oldState });
  };

  handelError = (name) => {
    const { error } = { ...this.state };
    error[name] = true;
    this.setState({ ...error });
  };

  componentWillUnmount = () => {
    const RefreshFun = this.props;
    RefreshFun.Refresh('Login');
  };

  submit = (e) => {
    e.preventDefault();

    const { password, username, validation } = this.state;
    const loginFunc = this.props;

    let success = true;
    Object.values(validation).map((value) => {
      success = success && value;
      return success;
    });

    if (success) {
      // eslint-disable-next-line no-useless-escape
      const emailRex = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
      const mobileRex = /^[1-9]\d{9}$/;
      let data;
      // eslint-disable-next-line no-restricted-globals
      if (emailRex.test(username.trim())) {
        data = { password, email: username };
        loginFunc.login(data);
        const oldState = { ...this.state };
        oldState.loader = true;
        this.setState({ ...oldState });
      } else if (mobileRex.test(username.trim())) {
        data = { password, mobile: username };
        loginFunc.login(data);
        const oldState = { ...this.state };
        oldState.loader = true;
        this.setState({ ...oldState });
      } else {
        this.handelError('username');
      }
    } else {
      if (username === '') {
        this.handelError('username');
      }
      if (password === '') {
        this.handelError('password');
      }
    }
  };

  render() {
    const { error, loader, cookieStatus } = this.state;
    const { status, token: freshToken } = this.props;
    if (status !== 0 && loader) {
      const oldState = { ...this.state };
      oldState.loader = false;
      this.setState({ ...oldState });
    }

    const token = freshToken || cookies.get('token');
    if (token) {
      const destination = cookies.get('prevPath') || '/admin';
      // Check if token is valid
      fetch(`${baseUrl}/user/states`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json; charset=urf-8',
          Accept: 'application/json, text/plain, */*',
          authorization_token: token,
        },
      })
        .then((response) => {
          if (response.status === 200) {
            this.setState({ cookieStatus: true });
          } else {
            cookies.remove('token');
            cookies.remove('stateList');
            cookies.remove('state');
            cookies.remove('userBasicDetail');
            cookies.remove('features');
          }
        })
        .catch((e) => {
          console.log(e, 'hello');
        });
      if (cookieStatus) {
        return <Redirect true push from="*" to={destination} />;
      }
    }

    return (
      <ErrorBoundary>
        <LoadingOverlay
          active={loader}
          spinner={<Loader />}
          text="Loading your content..."
        >
          <div
            className="app flex-row align-items-center"
            data-test="LoginTest"
          >
            <Container>
              <Row className="justify-content-center">
                <CardGroup className="row">
                  <Card className="col col-md-6 col-sm-12 p-4">
                    <CardBody>
                      <Form>
                        <h1>Login</h1>
                        <p className="text-muted">
                          Sign in to your account with email or mobile.
                        </p>
                        {// eslint-disable-next-line no-nested-ternary
                        status === 404
                        || status === 401
                        || status === 400
                        || status === 409 ? (
                          <ErrorWithProps props={this.props} />
                        ) : status >= 300 ? (
                          <ErrorWithoutProps message=" Something went wrong! Please contact the cms administrator or try again after refresh " />
                        ) : (
                          ''
                        )}
                        <Username
                          name="username"
                          placeholder="Mobile or Email"
                          required
                          onChange={this.handleChange}
                          onValidation={this.handleValidation}
                          submit={error.username}
                        />
                        <UserPassword
                          name="password"
                          placeholder="Password"
                          required
                          onChange={this.handleChange}
                          onValidation={this.handleValidation}
                          submit={error.password}
                        />
                        <Row>
                          <Col xs="6">
                            <Button
                              color="primary"
                              type="submit"
                              onClick={this.submit}
                              className="px-4"
                            >
                              Login
                            </Button>
                          </Col>
                          <Col xs="6" className="text-right">
                            <Link to="/ForgotPassword">Forgot password?</Link>
                          </Col>
                        </Row>
                      </Form>
                    </CardBody>
                  </Card>
                  <Card className="col col-md-6 col-sm-12 text-white bg-primary ">
                    <CardBody className="text-center">
                      <div>
                        <h2>Sign up</h2>
                        <p>
                          Indus Action’s low-cost, high-stake policy
                          implementation campaigns nurture existing leadership
                          within the community and Partner Entrepreneurs. This
                          network organizes existing resources within
                          governments and civil society to achieve our mission
                          across India.
                        </p>{' '}
                        <Link to="/register">
                          <Button
                            color="primary"
                            className="mt-3"
                            active
                            tabIndex={-1}
                          >
                            Register Now!
                          </Button>
                        </Link>
                      </div>
                    </CardBody>
                  </Card>
                </CardGroup>
              </Row>
            </Container>
          </div>
        </LoadingOverlay>
      </ErrorBoundary>
    );
  }
}

const mapStateToProps = state => ({
  token: state.auth.token,
  status: state.auth.loginStatus,
  errMessage: state.auth.loginErrMessage,
  userId: state.auth.userId,
});
export default connect(
  mapStateToProps,
  { login, Refresh, getStatesData },
)(Login);
