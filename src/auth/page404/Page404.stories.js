import React from 'react';
import { storiesOf } from '@storybook/react';

import Page404 from './Page404';

storiesOf('Auth|error pages', module).add('Page404', () => <Page404 />);
