import React from 'react';
import { storiesOf } from '@storybook/react';

import Page500 from './Page500';

storiesOf('Auth|error pages', module).add('Page500', () => <Page500 />);
