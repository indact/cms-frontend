import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Cookies from 'universal-cookie';
import LoadingOverlay from 'react-loading-overlay';
import {
  Button, Card, CardBody, Col, Container, Form, Row,
} from 'reactstrap';
import { signup, Refresh } from '../../store/action/AuthAction';
import userActions from '../../store/action/UserAction';
import {
  Email,
  Mobile,
  Password,
  Name,
  ErrorWithoutProps,
  ErrorWithProps,
  Loader,
} from '../../components/index';

const baseUrl = `${process.env.REACT_APP_BASE_URL}:${process.env.REACT_APP_CMS_PORT}`;
const { getStatesData } = userActions;
const cookies = new Cookies();

class Register extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      name: '',
      mobile: '',
      email: '',
      loader: false,
      password: '',
      repassword: '',
      cookieStatus: false,
      error: {
        name: false,
        mobile: false,
        password: false,
        repassword: false,
      },
      validation: {
        mobile: false,
        name: false,
        password: false,
        repassword: false,
      },
    };
  }

  // static getDerivedStateFromProps = (nextProps, prevProps) => {
  //   if (prevProps.token !== nextProps.token) {
  //     nextProps.getStatesData(nextProps.token);
  //     return { token: nextProps.token };
  //   }
  //   return null;
  // };

  componentDidMount() {
    cookies.remove('token');
    cookies.remove('stateList');
    cookies.remove('state');
    cookies.remove('userBasicDetail');
    cookies.remove('features');
  }

  componentWillUnmount = () => {
    // const RefreshFun = this.props;
    // RefreshFun.Refresh('Register');
  };

  handleValidation = async (name, state) => {
    const { validation } = this.state;
    validation[name] = state;
    await this.setState({ validation });
  };

  handleChange = (event) => {
    const oldState = { ...this.state };
    const { error } = { ...this.state };
    if (event.target.name) {
      error[event.target.name] = false;
      this.setState({ ...error });
    }
    oldState[event.target.name] = event.target.value.trim();
    this.setState({ ...oldState });
  };

  handelError = (name) => {
    const { error } = { ...this.state };
    error[name] = true;
    this.setState({ ...error });
  };

  submit = (e) => {
    e.preventDefault();
    const {
      name,
      password,
      mobile,
      email,
      validation,
      repassword,
    } = this.state;
    let success = true;
    Object.values(validation).map((value) => {
      success = success && value;
      return success;
    });
    if (success) {
      const { ...action } = this.props;
      const send = {
        name,
        password,
        mobile,
      };
      if (email !== '') {
        send.email = email;
      }
      action.signup(send);
      const oldState = { ...this.state };
      oldState.loader = true;
      this.setState({ ...oldState });
    } else {
      if (password === '') {
        this.handelError('password');
      }
      if (repassword === '') {
        this.handelError('repassword');
      }
      // if (email === '') {
      //   this.handelError('email');
      // }
      if (mobile === '') {
        this.handelError('mobile');
      }
      if (name === '') {
        this.handelError('name');
      }
    }
  };

  render() {
    const { error, loader, cookieStatus } = this.state;
    const { status, token: freshToken } = this.props;
    if (status !== 0 && loader) {
      const oldState = { ...this.state };
      oldState.loader = false;
      this.setState({ ...oldState });
    }
    const token = freshToken || cookies.get('token');
    if (token) {
      const destination = cookies.get('prevPath') || '/admin';
      // Check if token is valid
      fetch(`${baseUrl}/user/states`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json; charset=urf-8',
          Accept: 'application/json, text/plain, */*',
          authorization_token: token,
        },
      })
        .then((response) => {
          if (response.status === 200) {
            this.setState({ cookieStatus: true });
          } else {
            cookies.remove('token');
            cookies.remove('stateList');
            cookies.remove('state');
            cookies.remove('userBasicDetail');
            cookies.remove('features');
          }
        })
        .catch((e) => {
          console.log(e, 'hello');
        });
      if (cookieStatus) {
        return <Redirect true push from="*" to={destination} />;
      }
    }

    return (
      <LoadingOverlay
        active={loader}
        spinner={<Loader />}
        text="Loading your content..."
      >
        <div className="app flex-row align-items-center">
          <Container>
            <Row className="justify-content-center">
              <Col md="9" lg="7" xl="6">
                <Card className="mx-4">
                  <CardBody className="p-4">
                    <Form>
                      <h1>Register</h1>
                      <p className="text-muted">Create your account</p>
                      {// eslint-disable-next-line no-nested-ternary
                      status === 404
                      || status === 401
                      || status === 400
                      || status === 409 ? (
                        <ErrorWithProps props={this.props} />
                      ) : status >= 300 ? (
                        <ErrorWithoutProps message="Something went wrong! Please contact the cms administrator or try again after refresh" />
                      ) : (
                        ''
                      )}

                      {/* {status === 400 ? (
                      <div className="error">
                        You Are Already Exist Please <Link to="/">Login!</Link>
                        <Button color="link" className="btn-red-cross">
                          X
                        </Button>
                      </div>
                    ) : (
                      ''
                    )} */}
                      <Name
                        name="name"
                        placeholder="Name"
                        required
                        onChange={this.handleChange}
                        onValidation={this.handleValidation}
                        submit={error.name}
                      />

                      <Mobile
                        name="mobile"
                        placeholder="Mobile"
                        required
                        onChange={this.handleChange}
                        onValidation={this.handleValidation}
                        submit={error.mobile}
                      />
                      <Email
                        name="email"
                        placeholder="Email"
                        // required
                        onChange={this.handleChange}
                        onValidation={this.handleValidation}
                        submit={error.email}
                      />
                      <Password
                        onChange={this.handleChange}
                        onValidation={this.handleValidation}
                        submit={error.password || error.repassword}
                      />
                      <div
                        style={{
                          fontSize: ' x-small',
                          margin: ' -6px 0 5px 0',
                          color: 'red',
                        }}
                      >
                        {/* <li>Password must be at least 6 characters long.</li> */}
                        {/* <li>
                          Must contain at least one lower case letter,one upper
                          case letter,one digit.
                        </li>
                        <li>
                          {' '}
                          and one of these special characters ~!@#$%^&*()_+
                        </li> */}
                      </div>

                      <Button
                        color="success"
                        type="submit"
                        onClick={this.submit}
                        block
                      >
                        Create Account
                      </Button>
                      <Link to="/">Go To Login!</Link>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </Container>
        </div>
      </LoadingOverlay>
    );
  }
}

const mapStateToProps = state => ({
  token: state.auth.token,
  status: state.auth.signupStatus,
  errMessage: state.auth.signupErrMessage,
});

export default connect(
  mapStateToProps,
  { signup, Refresh, getStatesData },
)(Register);
