# cms-front-end

#start react-dev server
npm start

#start story book server
npm run storybook

#start json-server
npm run server

- port 3005 is fix for the json server

json-server --watch ./json-server/json/db1.json --port 3006
json-server --watch ./json-server/json/taskmanage.json --port 3006

#duild for prod

npm run build

npm i -D eslint eslint-config-airbnb eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-react eslint-plugin-promise
npm i -D husky lint-staged pretty-quick
npm i -D enzyme enzyme-adapter-reat-16 jest-enzyme

coding flow
step 1: design
step 2: code
step 3: validate all field (PropType, Manual validation)
step 4: test this Component as unit test
step 5: add in story boot
step 6: review code
step 7: commit code

git rm -r --cached .
git add .
git commit -m "fixed untracked files"

#spinner or loader
https://www.npmjs.com/package/react-loading-overlay

https://www.npmjs.com/package/react-spinners

##cypress
npm install cypress --save-dev

npx cypress open

if not resolve use

cypress install

## instructions for feature permissions

json-server --watch ./json-server/json/usermanage.json --port 3006
