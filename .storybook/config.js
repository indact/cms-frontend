import { configure } from '@storybook/react';
import '../src/assets/scss/App.scss';

function loadStories() {
  require('../src/storybook/index.js');
}

configure(loadStories, module);
