describe('username validations', () => {
  it('should load webpage', () => {
    cy.visit('/');
    cy.url().should('include', 'localhost:3000');
  });
  it('empty username ', () => {
    cy.get(
      '#root > div > div > div > div > div > div.col.col-md-6.col-sm-12.p-4.card > div > form > div.row > div.text-right.col-6 > a',
    ).click();
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[0].empty_input).each((array) => {
        cy.get('#username')
          .clear()
          .type(array.Username);
        cy.get(
          '#root > div > div > div > div > div > div.input-prepend.input-group > div.input-group-append > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=forgot_error]').should(
              'contain',
              'Please enter correct input.',
            );
          });
      });
    });
  });

  it('less than 10 characters username ', () => {
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[1].less_input).each((array) => {
        cy.get('#username')
          .clear()
          .type(array.Username);
        cy.get(
          '#root > div > div > div > div > div > div.input-prepend.input-group > div.input-group-append > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=forgot_error]').should(
              'contain',
              'Please enter correct input.',
            );
          });
      });
    });
  });

  it('more than 10 characters username ', () => {
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[2].more_input).each((array) => {
        cy.get('#username')
          .clear()
          .type(array.Username);
        cy.get(
          '#root > div > div > div > div > div > div.input-prepend.input-group > div.input-group-append > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=forgot_error]').should(
              'contain',
              'Please enter correct input.',
            );
          });
      });
    });
  });

  it('alphabets username ', () => {
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[3].alphabets_input).each((array) => {
        cy.get('#username')
          .clear()
          .type(array.Username);
        cy.get(
          '#root > div > div > div > div > div > div.input-prepend.input-group > div.input-group-append > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=forgot_error]').should(
              'contain',
              'Please enter correct input.',
            );
          });
      });
    });
  });

  it('alphanumberic username ', () => {
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[4].alphanumberic_input).each((array) => {
        cy.get('#username')
          .clear()
          .type(array.Username);
        cy.get(
          '#root > div > div > div > div > div > div.input-prepend.input-group > div.input-group-append > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=forgot_error]').should(
              'contain',
              'Please enter correct input.',
            );
          });
      });
    });
  });

  it('special characters username ', () => {
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[5].specialchr_input).each((array) => {
        cy.get('#username')
          .clear()
          .type(array.Username);
        cy.get(
          '#root > div > div > div > div > div > div.input-prepend.input-group > div.input-group-append > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=forgot_error]').should(
              'contain',
              'Please enter correct input.',
            );
          });
      });
    });
  });

  it('invalid username ', () => {
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[6].invalid_input).each((array) => {
        cy.get('#username')
          .clear()
          .type(array.Username);
        cy.get(
          '#root > div > div > div > div > div > div.input-prepend.input-group > div.input-group-append > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=forgot_error]').should(
              'contain',
              'Please enter correct input.',
            );
          });
      });
    });
  });

  it('correct username ', () => {
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[7].correct_input).each((array) => {
        cy.get('#username')
          .clear()
          .type(array.Username);
        cy.get(
          '#root > div > div > div > div > div > div.input-prepend.input-group > div.input-group-append > button',
        )
          .click()
          .then(() => {
            cy.url().should('include', 'localhost:3000/verifyOTP');
          });
      });
    });
  });

  it('empty OTP ', () => {
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[8].empty_OTP).each((array) => {
        cy.get('#OTP')
          .clear()
          .type(array.OTP);
        cy.get(
          '#root > div > div > div > div:nth-child(1) > div > div.input-prepend.input-group > div:nth-child(3) > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=OTP_error]').should(
              'contain',
              'Please Enter correct input.',
            );
          });
      });
    });
  });

  it('less than 4 characters OTP ', () => {
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[9].lesser_OTP).each((array) => {
        cy.get('#OTP')
          .clear()
          .type(array.OTP);
        cy.get(
          '#root > div > div > div > div:nth-child(1) > div > div.input-prepend.input-group > div:nth-child(3) > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=OTP_error]').should(
              'contain',
              'Please Enter correct input.',
            );
          });
      });
    });
  });

  it('more than 4 characters OTP ', () => {
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[10].more_OTP).each((array) => {
        cy.get('#OTP')
          .clear()
          .type(array.OTP);
        cy.get(
          '#root > div > div > div > div:nth-child(1) > div > div.input-prepend.input-group > div:nth-child(3) > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=OTP_error]').should(
              'contain',
              'Please Enter correct input.',
            );
          });
      });
    });
  });

  it('wrong 4 characters OTP ', () => {
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[11].wrong_OTP).each((array) => {
        cy.get('#OTP')
          .clear()
          .type(array.OTP);
        cy.get(
          '#root > div > div > div > div:nth-child(1) > div > div.input-prepend.input-group > div:nth-child(3) > button',
        )
          .click()
          .then(() => {
            cy.get(
              '#root > div > div > div > div:nth-child(1) > div > div.error',
            ).should('contain', 'Could not verify OTP ');
          });
      });
    });
  });

  it('alphabets OTP ', () => {
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[12].alphabets_OTP).each((array) => {
        cy.get('#OTP')
          .clear()
          .type(array.OTP);
        cy.get(
          '#root > div > div > div > div:nth-child(1) > div > div.input-prepend.input-group > div:nth-child(3) > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=OTP_error]').should(
              'contain',
              'Please Enter correct input.',
            );
          });
      });
    });
  });

  it('alphanumberic OTP ', () => {
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[13].alphanumberic_OTP).each((array) => {
        cy.get('#OTP')
          .clear()
          .type(array.OTP);
        cy.get(
          '#root > div > div > div > div:nth-child(1) > div > div.input-prepend.input-group > div:nth-child(3) > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=OTP_error]').should(
              'contain',
              'Please Enter correct input.',
            );
          });
      });
    });
  });

  it('special characters OTP ', () => {
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[14].specialchr_OTP).each((array) => {
        cy.get('#OTP')
          .clear()
          .type(array.OTP);
        cy.get(
          '#root > div > div > div > div:nth-child(1) > div > div.input-prepend.input-group > div:nth-child(3) > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=OTP_error]').should(
              'contain',
              'Please Enter correct input.',
            );
          });
      });
    });
  });

  it('correct OTP ', () => {
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[15].correct_OTP).each((array) => {
        cy.get('#OTP')
          .clear()
          .type(array.OTP);
        cy.get(
          '#root > div > div > div > div:nth-child(1) > div > div.input-prepend.input-group > div:nth-child(3) > button',
        )
          .click()
          .then(() => {
            cy.url().should('include', 'localhost:3000/ResetPassword');
          });
      });
    });
  });

  it('reset empty password ', () => {
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[16].reset_password_empty).each((array) => {
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('input[Name=repassword]')
          .clear()
          .type(array.repassword);
        cy.get(
          '#root > div > div > div > div > div > div > div > form > div:nth-child(4) > div.row > div:nth-child(1) > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=password_error]').should(
              'contain',
              'Password cannot be empty',
            );
          });
      });
    });
  });

  it('reset password less than 6 characters ', () => {
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[17].reset_password_lesser).each((array) => {
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('input[Name=repassword]')
          .clear()
          .type(array.repassword);
        cy.get(
          '#root > div > div > div > div > div > div > div > form > div:nth-child(4) > div.row > div:nth-child(1) > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=repassword_error]').should(
              'contain',
              'Password must be at least 6 characters.',
            );
          });
      });
    });
  });

  it('alphabets reset password ', () => {
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[18].alphabets_password).each((array) => {
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('input[Name=repassword]')
          .clear()
          .type(array.repassword);
        cy.get(
          '#root > div > div > div > div > div > div > div > form > div:nth-child(4) > div.row > div:nth-child(1) > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=repassword_error]').should(
              'contain',
              'Password must be at least 6 characters.',
            );
          });
      });
    });
  });
  it('alphanumberic reset password ', () => {
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[19].aphanumeric_password).each((array) => {
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('input[Name=repassword]')
          .clear()
          .type(array.repassword);
        cy.get(
          '#root > div > div > div > div > div > div > div > form > div:nth-child(4) > div.row > div:nth-child(1) > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=repassword_error]').should(
              'contain',
              'Password must be at least 6 characters.',
            );
          });
      });
    });
  });

  it('reset empty repassword ', () => {
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[20].reset_repassword_empty).each((array) => {
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('input[Name=repassword]')
          .clear()
          .type(array.repassword);
        cy.get(
          '#root > div > div > div > div > div > div > div > form > div:nth-child(4) > div.row > div:nth-child(1) > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=repassword_error]').should(
              'contain',
              'Confirmed Password does not match.',
            );
          });
      });
    });
  });

  it('reset wrong repassword ', () => {
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[21].reset_wrong_password).each((array) => {
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('input[Name=repassword]')
          .clear()
          .type(array.repassword);
        cy.get(
          '#root > div > div > div > div > div > div > div > form > div:nth-child(4) > div.row > div:nth-child(1) > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=repassword_error]').should(
              'contain',
              'Confirmed Password does not match.',
            );
          });
      });
    });
  });

  it('reset correct details ', () => {
    cy.readFile('cypress/fixtures/forgot.json').then((str) => {
      cy.wrap(str[22].reset_correct_details).each((array) => {
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('input[Name=repassword]')
          .clear()
          .type(array.repassword);
        cy.get(
          '#root > div > div > div > div > div > div > div > form > div:nth-child(4) > div.row > div:nth-child(1) > button',
        )
          .click()
          .then(() => {
            cy.get(
              '#root > div > div > div > div > div > div > form > div',
            ).should(
              'include',
              'Your Password Reset Succsesfully Please Login',
            );
          });
      });
    });
  });
});
