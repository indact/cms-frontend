describe('userManager permissions', () => {
  it('should load webpage', () => {
    cy.visit('/');
    cy.url().should('include', 'localhost:3000');
  });

  it('login with no state selected ', () => {
    cy.readFile('cypress/fixtures/callermgt.json').then((str) => {
      cy.wrap(str[0].login_correct_details).each((array) => {
        cy.get('input[Name=username]')
          .clear()
          .type(array.Username);
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('.px-4')
          .click()
          .then(() => {
            cy.url().should('include', '/admin/default');
          });
      });
    });

    // heading should be No State Selected
    cy.get('[data-test=no_state]').should('contain', 'No State Selected');

    // navbar should be empty
    cy.get('.nav').should('be.empty');

    // State Selected Statement
    cy.get('.default-state').should(
      'contain',
      'Please select login state then you can see your features and permissions',
    );

    // State Selected dropdown
    cy.get('[data-test=drop_test]').click();
    cy.get('[data-test=drop_item]')
      .click()
      .then(() => {
        cy.url().should('include', '/admin/dashboard');
      });

    // Coming Soon
    cy.get('[data-test=dash_heading]').should('contain', 'Coming Soon...');

    // User Mgt
    cy.get('.sidebar')
      .contains('User Management')
      .click()
      .then(() => {
        cy.url().should('include', '/admin/userlist');
      });

    // caller profile
    cy.get('[data-test=caller_profile]')
      .eq(0)
      .click()
      .then(() => {
        cy.get('.modal-content').should('be.visible');
      });
    cy.get('[data-test=user_cancel]').click();

    // Search by name
    cy.get('[data-test=Searching]')
      .clear()
      .type('admin');
    cy.get('[data-test=Search_test]').click();
    cy.get('[data-test=srchmenu_test]')
      .contains('Name')
      .click()
      .then(() => {
        cy.get('[data-test=table_test]').should('be.visible');
      });

    // Fail Search by name/Phone
    cy.get('[data-test=Searching]')
      .clear()
      .type('admin');
    cy.get('[data-test=Search_test]').click();
    cy.get('[data-test=srchmenu_test]')
      .contains('Phone')
      .click()
      .then(() => {
        cy.get('[data-test=table_test]').should('be.visible');
      });

    // Fail Search by name/email
    cy.get('[data-test=Searching]')
      .clear()
      .type('admin');
    cy.get('[data-test=Search_test]').click();
    cy.get('[data-test=srchmenu_test]')
      .contains('Email')
      .click()
      .then(() => {
        cy.get('[data-test=table_test]').should('be.visible');
      });

    // Search by Phone
    cy.get('[data-test=Searching]')
      .clear()
      .type('9999999922');
    cy.get('[data-test=Search_test]').click();
    cy.get('[data-test=srchmenu_test]')
      .contains('Phone')
      .click()
      .then(() => {
        cy.get('[data-test=table_test]').should('be.visible');
      });

    // Fail Search by name/Phone
    cy.get('[data-test=Searching]')
      .clear()
      .type('9999999922');
    cy.get('[data-test=Search_test]').click();
    cy.get('[data-test=srchmenu_test]')
      .contains('Name')
      .click()
      .then(() => {
        cy.get('[data-test=table_test]').should('be.visible');
      });

    // Fail Search by email/phone
    cy.get('[data-test=Searching]')
      .clear()
      .type('9999999922');
    cy.get('[data-test=Search_test]').click();
    cy.get('[data-test=srchmenu_test]')
      .contains('Email')
      .click()
      .then(() => {
        cy.get('[data-test=table_test]').should('be.visible');
      });

    // Fail Search by name/email
    cy.get('[data-test=Searching]')
      .clear()
      .type('test14@indusaction.org');
    cy.get('[data-test=Search_test]').click();
    cy.get('[data-test=srchmenu_test]')
      .contains('Name')
      .click()
      .then(() => {
        cy.get('[data-test=table_test]').should('be.visible');
      });

    // Fail Search by phone/email
    cy.get('[data-test=Searching]')
      .clear()
      .type('test14@indusaction.org');
    cy.get('[data-test=Search_test]').click();
    cy.get('[data-test=srchmenu_test]')
      .contains('Phone')
      .click()
      .then(() => {
        cy.get('[data-test=table_test]').should('be.visible');
      });

    // Search by Email
    cy.get('[data-test=Searching]')
      .clear()
      .type('test14@indusaction.org');
    cy.get('[data-test=Search_test]').click();
    cy.get('[data-test=srchmenu_test]')
      .contains('Email')
      .click()
      .then(() => {
        cy.get('[data-test=table_test]').should('be.visible');
      });

    // Search by Disabled User
    cy.get('[data-test=Searching]').clear();
    cy.get('[data-test=Search_test]').click();
    cy.get('[data-test=srchmenu_test]')
      .contains('Disabled User')
      .click()
      .then(() => {
        cy.get('[data-test=table_test]').should('be.visible');
      });

    // Search by Enabled User
    cy.get('[data-test=Searching]').clear();
    cy.get('[data-test=Search_test]').click();
    cy.get('[data-test=srchmenu_test]')
      .contains('Enabled User')
      .click()
      .then(() => {
        cy.get('[data-test=table_test]').should('be.visible');
      });

    cy.get('[data-test=Searching]').clear();
    cy.get('[data-test=Search_test]').click();
    cy.get('[data-test=srchmenu_test]')
      .contains('Name')
      .click();
    cy.wait(5000);

    // checkAll
    cy.get('input[Name=checkAll]').click();
    cy.wait(5000);
    cy.get('[data-test=enable_test]')
      .click()
      .then(() => {
        cy.get('[data-test=table_test]').should('be.visible');
      });

    // one enable
    //  cy.get('[name=admin]').click().then(() => {
    //   cy.get('[data-test=enable_user]' ).should('be.enabled');
    // });

    // one enable
    cy.get('.switch-slider')
      .eq('0')
      .click()
      .then(() => {
        cy.get('[data-test=enable_user]').should('be.enabled');
      });

    // one disable
    cy.get('.switch-slider')
      .eq('1')
      .click()
      .then(() => {
        cy.get('[data-test=disable_user]').should('be.enabled');
      });

    // enable checkAll
    cy.get('[data-test=enable_test]')
      .click()
      .then(() => {
        cy.get('[data-test=enable_user]').should('be.enabled');
      });

    // disable checkAll
    cy.get('[data-test=disable_test]')
      .click()
      .then(() => {
        cy.get('[data-test=disable_user]').should('be.enabled');
      });

    // enable 3 user
    cy.get('.table tr td')
      .eq('1')
      .click();
    cy.get('.table tr td')
      .eq('3')
      .click();
    cy.get('.table tr td')
      .eq('5')
      .click();
    cy.get('[data-test=enable_test]')
      .click()
      .then(() => {
        cy.get('[data-test=enable_user]').should('be.enabled');
      });

    // disable 3 user
    cy.get('.table tr td')
      .eq('2')
      .click();
    cy.get('.table tr td')
      .eq('3')
      .click();
    cy.get('.table tr td')
      .eq('4')
      .click();
    cy.get('[data-test=disable_test]')
      .click()
      .then(() => {
        cy.get('[data-test=disable_user]').should('be.enabled');
      });
  });
});
