describe('userManager permissions', () => {
  it('should load webpage', () => {
    cy.visit('/');
    cy.url().should('include', 'localhost:3000');
  });

  it('should have correct username & password ', () => {
    cy.readFile('cypress/fixtures/featuremgt.json').then((str) => {
      cy.wrap(str[0].login_correct_details).each((array) => {
        cy.get('input[Name=username]')
          .clear()
          .type(array.Username);
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('.px-4')
          .click()
          .then(() => {
            cy.wait(5000);
            cy.url().should('include', '/admin/default');
          });
      });
    });

    // heading should be No State Selected
    cy.get('[data-test=no_state]').should('contain', 'No State Selected');

    // navbar should be empty
    cy.get('.nav').should('be.empty');

    // State Selected Statement
    cy.get('.default-state').should(
      'contain',
      'Please select login state then you can see your features and permissions',
    );

    // State Selected dropdown
    cy.get('[data-test=drop_test]').click();
    cy.get('[data-test=drop_item]')
      .click()
      .then(() => {
        cy.url().should('include', '/admin/dashboard');
      });

    // User Mgt
    cy.get('.sidebar')
      .contains('Feature Management')
      .click()
      .then(() => {
        cy.url().should('include', '/admin/userManager/permissionManager');
      });

    // no select
    cy.get('.mb-3 button')
      .click()
      .then(() => {
        cy.get('#collapseExample').should('not.be.visible');
      });

    // enter wrong alphabets in feature
    cy.get('#features').type('j');
    cy.get('.mb-3 button')
      .click()
      .then(() => {
        cy.get('#collapseExample').should('not.be.visible');
      });

    // enter wrong alphabets in permission
    cy.get('#permission').type('k');
    cy.get('.mb-3 button')
      .click()
      .then(() => {
        cy.get('#collapseExample').should('not.be.visible');
      });

    // select User Mgt features
    cy.get('#features')
      .clear()
      .type('u');
    cy.get('.suggestions li')
      .eq('0')
      .click();
    cy.get('.mb-3 button')
      .click()
      .then(() => {
        cy.wait(5000);
        cy.get('#collapseExample').should('be.visible');
      });

    // Remove
    cy.get('[data-test=remove_test]')
      .eq('0')
      .click()
      .then(() => {
        cy.get('[username=test3]').should('not.be.visible');
      });

    // select Caller Mgt features
    cy.get('#features')
      .clear()
      .type('c');
    cy.get('.suggestions li')
      .eq('0')
      .click();
    cy.get('.mb-3 button')
      .click()
      .then(() => {
        cy.get('#collapseExample').should('be.visible');
      });

    // select user
    cy.get('#features').clear();
    cy.get('.arrow').click();
    cy.get('.options.show li')
      .eq(4)
      .click();
    cy.get('.options.show li')
      .eq(0)
      .click();
    cy.get('.options.show li')
      .eq(3)
      .click();
    cy.get('.mb-3 button')
      .click()
      .then(() => {
        cy.get('#collapseExample').should('be.visible');
      });

    // feature user
    cy.get('#features')
      .clear()
      .type('c');
    cy.get('.suggestions li')
      .eq('0')
      .click();
    cy.get('#permission')
      .clear()
      .type('a');
    cy.get('.suggestions li')
      .eq('0')
      .and('eq', '1')
      .click()
      .then(() => {
        cy.wait(1000);
        cy.get('#collapseExample').should('not.be.visible');
      });

    // feature user
    cy.get('#features')
      .clear()
      .type('a');
    cy.get('.suggestions li')
      .eq('4')
      .click();
    cy.get('#permission')
      .clear()
      .type('a');
    cy.get('.suggestions li')
      .eq('2')
      .click()
      .then(() => {
        cy.get('#collapseExample').should('be.visible');
      });
  });
});
