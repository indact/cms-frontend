describe('userManager permissions', () => {
  it('should load webpage', () => {
    cy.visit('/');
    cy.url().should('include', 'localhost:3000');
  });

  it('login with no state selected ', () => {
    cy.readFile('cypress/fixtures/dashboard.json').then((str) => {
      cy.wrap(str[0].login_correct_details).each((array) => {
        cy.get('input[Name=username]')
          .clear()
          .type(array.Username);
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('.px-4')
          .click()
          .then(() => {
            cy.url().should('include', '/admin/default');
          });
      });
    });
  });

  it('heading should be No State Selected ', () => {
    cy.get('[data-test=no_state]').should('contain', 'No State Selected');
  });

  it('navbar should be empty ', () => {
    cy.get('.nav').should('be.empty');
  });

  it('navbar should be No State Selected ', () => {
    cy.get('.default-no-state').should(
      'contain',
      'Please contact CMS admin for assignment of states and permissions',
    );
  });

  it('logout ', () => {
    cy.get('[data-test=logout_test]')
      .click()
      .then(() => {
        cy.url().should('include', 'localhost:3000');
      });
  });
});
