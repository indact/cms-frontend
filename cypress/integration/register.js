describe('My first cypress test', () => {
  it('should load webpage', () => {
    cy.visit('/');
    cy.url().should('include', 'localhost:3000');
  });

  it('empty name ', () => {
    cy.get('.mt-3').click();
    cy.readFile('cypress/fixtures/register.json').then((str) => {
      cy.wrap(str[0].name_empty).each((array) => {
        cy.get('input[Name=name]').type(array.name);
        cy.get('input[Name=mobile]')
          .clear()
          .type(array.mobile);
        cy.get('input[Name=email]')
          .clear()
          .type(array.email);
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('input[Name=repassword]')
          .clear()
          .type(array.repassword);
        cy.get(
          '#root > div > div > div > div > div > div > div > form > button',
        )
          .click()
          .then(() => {
            cy.get(
              '#root > div > div > div > div > div > div > div > form > div.error',
            ).should('contain', 'Validation Error');
          });
      });
    });
  });

  it('empty mobile number ', () => {
    cy.readFile('cypress/fixtures/register.json').then((str) => {
      cy.wrap(str[1].mobile_empty).each((array) => {
        cy.get('input[Name=name]')
          .clear()
          .type(array.name);
        cy.get('input[Name=mobile]')
          .clear()
          .type(array.mobile);
        cy.get('input[Name=email]')
          .clear()
          .type(array.email);
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('input[Name=repassword]')
          .clear()
          .type(array.repassword);
        cy.get(
          '#root > div > div > div > div > div > div > div > form > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=mobile_error]').should(
              'contain',
              'Mobile number cannot be empty',
            );
          });
      });
    });
  });

  it('less digits in mobile number ', () => {
    cy.readFile('cypress/fixtures/register.json').then((str) => {
      cy.wrap(str[2].register_wrong_mobile).each((array) => {
        cy.get('input[Name=name]')
          .clear()
          .type(array.name);
        cy.get('input[Name=mobile]')
          .clear()
          .type(array.mobile);
        cy.get('input[Name=email]')
          .clear()
          .type(array.email);
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('input[Name=repassword]')
          .clear()
          .type(array.repassword);
        cy.get(
          '#root > div > div > div > div > div > div > div > form > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=mobile_error]').should(
              'contain',
              'Incorrect number entered. Number cannot start with 0.Cannot contain letters.Must contain 10 digits',
            );
          });
      });
    });
  });

  it('alphabets or any other special characters in mobile number ', () => {
    cy.readFile('cypress/fixtures/register.json').then((str) => {
      cy.wrap(str[3].alphabets_mobile).each((array) => {
        cy.get('input[Name=name]')
          .clear()
          .type(array.name);
        cy.get('input[Name=mobile]')
          .clear()
          .type(array.mobile);
        cy.get('input[Name=email]')
          .clear()
          .type(array.email);
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('input[Name=repassword]')
          .clear()
          .type(array.repassword);
        cy.get(
          '#root > div > div > div > div > div > div > div > form > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=mobile_error]').should(
              'contain',
              'Incorrect number entered. Number cannot start with 0.Cannot contain letters.Must contain 10 digits',
            );
          });
      });
    });
  });

  it('empty email ', () => {
    cy.readFile('cypress/fixtures/register.json').then((str) => {
      cy.wrap(str[4].email_empty).each((array) => {
        cy.get('input[Name=name]')
          .clear()
          .type(array.name);
        cy.get('input[Name=mobile]')
          .clear()
          .type(array.mobile);
        cy.get('input[Name=email]')
          .clear()
          .type(array.email);
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('input[Name=repassword]')
          .clear()
          .type(array.repassword);
        cy.get(
          '#root > div > div > div > div > div > div > div > form > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=email_error]').should(
              'contain',
              'Email cannot be empty',
            );
          });
      });
    });
  });

  it('invalid email ', () => {
    cy.readFile('cypress/fixtures/register.json').then((str) => {
      cy.wrap(str[5].register_wrong_email).each((array) => {
        cy.get('input[Name=name]')
          .clear()
          .type(array.name);
        cy.get('input[Name=mobile]')
          .clear()
          .type(array.mobile);
        cy.get('input[Name=email]')
          .clear()
          .type(array.email);
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('input[Name=repassword]')
          .clear()
          .type(array.repassword);
        cy.get(
          '#root > div > div > div > div > div > div > div > form > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=email_error]').should(
              'contain',
              'Email format is incorrect. Correct format: someone@example.com',
            );
          });
      });
    });
  });

  it('empty password ', () => {
    cy.readFile('cypress/fixtures/register.json').then((str) => {
      cy.wrap(str[6].password_empty).each((array) => {
        cy.get('input[Name=name]')
          .clear()
          .type(array.name);
        cy.get('input[Name=mobile]')
          .clear()
          .type(array.mobile);
        cy.get('input[Name=email]')
          .clear()
          .type(array.email);
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('input[Name=repassword]')
          .clear()
          .type(array.repassword);
        cy.get(
          '#root > div > div > div > div > div > div > div > form > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=password_error]').should(
              'contain',
              'Password cannot be empty',
            );
          });
      });
    });
  });

  it('password less than 6 characters ', () => {
    cy.readFile('cypress/fixtures/register.json').then((str) => {
      cy.wrap(str[7].register_wrong_password).each((array) => {
        cy.get('input[Name=name]')
          .clear()
          .type(array.name);
        cy.get('input[Name=mobile]')
          .clear()
          .type(array.mobile);
        cy.get('input[Name=email]')
          .clear()
          .type(array.email);
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('input[Name=repassword]')
          .clear()
          .type(array.repassword);
        cy.get(
          '#root > div > div > div > div > div > div > div > form > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=password_error]').should(
              'contain',
              'Password must be at least 6 characters.',
            );
          });
      });
    });
  });

  it('empty repassword ', () => {
    cy.readFile('cypress/fixtures/register.json').then((str) => {
      cy.wrap(str[8].repassword_empty).each((array) => {
        cy.get('input[Name=name]')
          .clear()
          .type(array.name);
        cy.get('input[Name=mobile]')
          .clear()
          .type(array.mobile);
        cy.get('input[Name=email]')
          .clear()
          .type(array.email);
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('input[Name=repassword]')
          .clear()
          .type(array.repassword);
        cy.get(
          '#root > div > div > div > div > div > div > div > form > button',
        )
          .click()
          .then(() => {
            cy.get('[data-test=repassword_error]').should(
              'contain',
              'Confirm Password cannot be empty.',
            );
          });
      });
    });
  });

  it('should have wrong repassword error ', () => {
    cy.readFile('cypress/fixtures/register.json').then((str) => {
      cy.wrap(str[9].wrong_repassword).each((array) => {
        cy.get('input[Name=name]')
          .clear()
          .type(array.name);
        cy.get('input[Name=mobile]')
          .clear()
          .type(array.mobile);
        cy.get('input[Name=email]')
          .clear()
          .type(array.email);
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('input[Name=repassword]')
          .clear()
          .type(array.repassword);
        cy.get(
          '#root > div > div > div > div > div > div > div > form > button',
        )
          .click()
          .then(() => {
            cy.wait(500);
            cy.get('[data-test=repassword_error]').should(
              'contain',
              'Your Password and Confirmed Password does not match',
            );
          });
      });
    });
  });

  it('same mobile number and email  ', () => {
    cy.readFile('cypress/fixtures/register.json').then((str) => {
      cy.wrap(str[10].same_mobile).each((array) => {
        cy.get('input[Name=name]')
          .clear()
          .type(array.name);
        cy.get('input[Name=mobile]')
          .clear()
          .type(array.mobile);
        cy.get('input[Name=email]')
          .clear()
          .type(array.email);
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('input[Name=repassword]')
          .clear()
          .type(array.repassword);
        cy.get(
          '#root > div > div > div > div > div > div > div > form > button',
        )
          .click()
          .then(() => {
            cy.get(
              '#root > div > div > div > div > div > div > div > form > div.error',
            ).should('contain', 'User already exists');
          });
      });
    });
  });

  it('same email ', () => {
    cy.readFile('cypress/fixtures/register.json').then((str) => {
      cy.wrap(str[11].register_same_email).each((array) => {
        cy.get('input[Name=name]')
          .clear()
          .type(array.name);
        cy.get('input[Name=mobile]')
          .clear()
          .type(array.mobile);
        cy.get('input[Name=email]')
          .clear()
          .type(array.email);
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('input[Name=repassword]')
          .clear()
          .type(array.repassword);
        cy.get(
          '#root > div > div > div > div > div > div > div > form > button',
        )
          .click()
          .then(() => {
            cy.get(
              '#root > div > div > div > div > div > div > div > form > div.error',
            ).should('contain', 'User already exists');
          });
      });
    });
  });

  it('should have all correct details ', () => {
    cy.readFile('cypress/fixtures/register.json').then((str) => {
      cy.wrap(str[12].register_correct).each((array) => {
        cy.get('input[Name=name]')
          .clear()
          .type(array.name);
        cy.get('input[Name=mobile]')
          .clear()
          .type(array.mobile);
        cy.get('input[Name=email]')
          .clear()
          .type(array.email);
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('input[Name=repassword]')
          .clear()
          .type(array.repassword);
        cy.get(
          '#root > div > div > div > div > div > div > div > form > button',
        )
          .click()
          .then(() => {
            cy.url().should('include', '/admin/dashboard');
          });
      });
    });
  });
});
