describe('username validations', () => {
  it('should load webpage', () => {
    cy.visit('/');
    cy.url().should('include', 'localhost:3000');
  });
  it('empty username ', () => {
    cy.readFile('cypress/fixtures/login.json').then((str) => {
      cy.wrap(str[0].login_empty_username).each((array) => {
        cy.get('input[Name=username]')
          .clear()
          .type(array.Username);
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('.px-4')
          .click()
          .then(() => {
            cy.get('[data-test=username_error]').should(
              'contain',
              'Enter a valid 10 digit Mobile number.',
            );
          });
      });
    });
  });

  it('empty password ', () => {
    cy.readFile('cypress/fixtures/login.json').then((str) => {
      cy.wrap(str[1].login_empty_password).each((array) => {
        cy.get('input[Name=username]')
          .clear()
          .type(array.Username);
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('.px-4')
          .click()
          .then(() => {
            cy.get('[data-test=password_error]').should(
              'contain',
              'Password cannot be empty',
            );
          });
      });
    });
  });

  it('Wrong username but correct password ', () => {
    cy.readFile('cypress/fixtures/login.json').then((str) => {
      cy.wrap(str[2].login_wrong_username).each((array) => {
        cy.get('input[Name=username]')
          .clear()
          .type(array.Username);
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('.px-4')
          .click()
          .then(() => {
            cy.wait(5000);
            cy.get(
              '#root > div > div > div > div > div > div.col.col-md-6.col-sm-12.p-4.card > div > form > div.error',
            ).should('contain', 'User does not exists');
          });
      });
    });
  });

  it('Wrong password but correct username ', () => {
    cy.readFile('cypress/fixtures/login.json').then((str) => {
      cy.wrap(str[3].login_wrong_password).each((array) => {
        cy.get('input[Name=username]')
          .clear()
          .type(array.Username);
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('.px-4')
          .click()
          .then(() => {
            cy.get(
              '#root > div > div > div > div > div > div.col.col-md-6.col-sm-12.p-4.card > div > form > div.error',
            ).should('contain', 'Entered Password is incorrect');
          });
      });
    });
  });

  it('Wrong username & password ', () => {
    cy.readFile('cypress/fixtures/login.json').then((str) => {
      cy.wrap(str[4].login_incorrect_details).each((array) => {
        cy.get('input[Name=username]')
          .clear()
          .type(array.Username);
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('.px-4')
          .click()
          .then(() => {
            cy.get(
              '#root > div > div > div > div > div > div.col.col-md-6.col-sm-12.p-4.card > div > form > div.error',
            ).should('contain', 'User does not exists');
          });
      });
    });
  });

  it('should have correct username & password then logout ', () => {
    cy.readFile('cypress/fixtures/login.json').then((str) => {
      cy.wrap(str[5].login_correct_details).each((array) => {
        cy.get('input[Name=username]')
          .clear()
          .type(array.Username);
        cy.get('input[Name=password]')
          .clear()
          .type(array.password);
        cy.get('.px-4')
          .click()
          .then(() => {
            cy.url().should('include', '/admin/default');
          });
      });
    });
  });
});
