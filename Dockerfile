
FROM node:alpine as build-deps

WORKDIR /var/www/cms/fronend

COPY ./package.json ./
RUN npm install
RUN npm install babel-loader@8.1.0
RUN npm install webpack@4.42.0

COPY ./ ./
RUN npm run build

FROM nginx:1.12-alpine
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build-deps /var/www/cms/fronend/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
